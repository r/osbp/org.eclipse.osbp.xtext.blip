/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 *
 */
 
package org.eclipse.osbp.xtext.blip.ui.labeling

import com.google.inject.Inject
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider
import org.eclipse.osbp.xtext.basic.ui.labeling.BasicDSLLabelProvider
import org.eclipse.osbp.xtext.blip.Blip
import org.eclipse.osbp.xtext.blip.BlipDto
import org.eclipse.osbp.xtext.blip.BlipFilter
import org.eclipse.osbp.xtext.blip.BlipModel
import org.eclipse.osbp.xtext.blip.BlipWorkload

/**
 * Provides labels for a EObjects.
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#labelProvider
 */
class BlipDSLLabelProvider extends BasicDSLLabelProvider {

	@Inject
	new(AdapterFactoryLabelProvider delegate) {
		super(delegate);
	}

	/* ********************************************************************************************** */
	/**			Provides outline text for Blip-DSL.
	 *
	 *			Uses common (default) text for package and for import element.
	 *
	 *	@see	BasicDSLLabelProvider.text(Object)
	 * 
	 *	@param	o	Current outline element
	 *
	 *	@return	String instance.
	 *
	 *	@since	@D 150925, gu
	 *	@date	@D yymmdd, name
	 */ /* ******************************************************************************************* */
	 override text ( Object o ) {
		switch (o) {
			Blip					:	generateText( o, 'blip'          , (o as Blip      ).name  )
			BlipWorkload			:	generateText( o, 'blip workload' )
			BlipDto					:	generateText( o, 'blip dto'      )
			BlipFilter				:	generateText( o, 'blip filter'   , (o as BlipFilter).filterName )
			default					:	super.text( o )
		}
	}

	/* ********************************************************************************************** */
	/**			Provides outline images for Blip-DSL.
	 *
	 *			Uses common (default) image for package and for import element.
	 *
	 *	@see	BasicDSLLabelProvider.image(Object)
	 * 
	 *	@param	o	Current outline element
	 *
	 *	@return	Image instance if requested image file was found, return value of overridden superclass
	 *			method otherwise.
	 *
	 *	@since	@D 150925, gu
	 *	@date	@D yymmdd, name
	 */ /* ******************************************************************************************* */
	override image ( Object o ) {
		switch (o) {
			BlipModel					:	getInternalImage( 'model.png', super.class)
			Blip						:	getInternalImage( 'dsl_blip.png', class)
			BlipWorkload				:	getInternalImage( 'blip_workload.png', class)
			BlipDto						:	getInternalImage( 'dsl_dto.png', class)
			BlipFilter					:	getInternalImage( 'dsl_filter.png', class)
			default						:	super.image( o )
		}
	}

}
