/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.xtext.blip.ui;

import javax.inject.Inject;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.bpmn2.ecore.ui.BPMnEObjectHoverDocumentationProvider;
import org.eclipse.osbp.dsl.semantic.dto.LDto;
import org.eclipse.osbp.dsl.semantic.dto.LDtoAbstractReference;
import org.eclipse.osbp.dsl.semantic.dto.LDtoInheritedReference;
import org.eclipse.osbp.dsl.semantic.entity.LEntity;
import org.eclipse.osbp.xtext.basic.ui.BasicDSLDocumentationTranslator;
import org.eclipse.osbp.xtext.basic.ui.BasicDSLEObjectHoverDocumentationProvider;
import org.eclipse.osbp.xtext.blip.BlipItem;
import org.eclipse.osbp.xtext.blip.common.BlipHelper;
import org.eclipse.osbp.xtext.blip.impl.BlipCallActivityImpl;
import org.eclipse.osbp.xtext.blip.impl.BlipDtoPathImpl;
import org.eclipse.osbp.xtext.blip.impl.BlipEndEventImpl;
import org.eclipse.osbp.xtext.blip.impl.BlipExclusiveSplitGatewayImpl;
import org.eclipse.osbp.xtext.blip.impl.BlipImpl;
import org.eclipse.osbp.xtext.blip.impl.BlipOutGoingImpl;
import org.eclipse.osbp.xtext.blip.impl.BlipScriptImpl;
import org.eclipse.osbp.xtext.blip.impl.BlipStartEventImpl;
import org.eclipse.osbp.xtext.blip.impl.BlipUserTaskImpl;

public class BlipDSLEObjectHoverDocumentationProvider extends BasicDSLEObjectHoverDocumentationProvider {
	
	@Inject protected BPMnEObjectHoverDocumentationProvider bpmnHoverDocumentationProvider;
	
    private static BlipDSLEObjectHoverDocumentationProvider INSTANCE;

    public static BlipDSLEObjectHoverDocumentationProvider instance() {
        return INSTANCE;
    }

    public BlipDSLEObjectHoverDocumentationProvider() {
        super();
        INSTANCE = this;
    }
    
    @Override
    protected BasicDSLDocumentationTranslator getTranslator() {
        return BlipDSLUiDocumentationTranslator.instance();
    }
	
	@Override
	public String getDocumentation(EObject object) {
		String documentation = "";
		if	((object instanceof BlipImpl) && (((BlipImpl)object).getProcess() != null)) {
			documentation += bpmnHoverDocumentationProvider.getDocumentation(((BlipImpl) object).getProcess());
		}
		else if	((object instanceof BlipStartEventImpl) && (((BlipStartEventImpl)object).getEvent() != null)) {
			documentation += bpmnHoverDocumentationProvider.getDocumentation(((BlipStartEventImpl) object).getEvent());
		}
		else if	((object instanceof BlipEndEventImpl) && (((BlipEndEventImpl)object).getEvent() != null)) {
			documentation += bpmnHoverDocumentationProvider.getDocumentation(((BlipEndEventImpl) object).getEvent());
		}
		else if	((object instanceof BlipUserTaskImpl) && (((BlipUserTaskImpl)object).getTask() != null)) {
			documentation += bpmnHoverDocumentationProvider.getDocumentation(((BlipUserTaskImpl) object).getTask());
		}
		else if	((object instanceof BlipCallActivityImpl) && (((BlipCallActivityImpl)object).getCallActivity() != null)) {
			documentation += bpmnHoverDocumentationProvider.getDocumentation(((BlipCallActivityImpl) object).getCallActivity());
		}
		else if	((object instanceof BlipScriptImpl) && (((BlipScriptImpl)object).getTask() != null)) {
			documentation += bpmnHoverDocumentationProvider.getDocumentation(((BlipScriptImpl) object).getTask());
		}
		else if	((object instanceof BlipExclusiveSplitGatewayImpl) && (((BlipExclusiveSplitGatewayImpl)object).getGateway() != null)) {
			documentation += bpmnHoverDocumentationProvider.getDocumentation(((BlipExclusiveSplitGatewayImpl) object).getGateway());
		}
		else if	((object instanceof BlipOutGoingImpl) && (((BlipOutGoingImpl)object).getSequenceFlow() != null)) {
			documentation += bpmnHoverDocumentationProvider.getDocumentation(((BlipOutGoingImpl) object).getSequenceFlow());
		}
		else if	(object instanceof BlipDtoPathImpl) {
			LDtoAbstractReference feature = BlipHelper.getDtoFeature((BlipItem)object.eContainer());
			if	(feature instanceof LDtoInheritedReference) {
				documentation += super.getDocumentation(((LDtoInheritedReference) feature).getInheritedFeature());
			}
			else if	(feature != null) {
				documentation += super.getDocumentation(feature);
			}
			LDto dto = BlipHelper.getOperativeLDto((BlipItem)object.eContainer());
			if	(dto != null) {
				documentation += super.getDocumentation(dto);
			}
			LEntity entity = BlipHelper.getOperativeEntity((BlipItem)object.eContainer());
			if	(entity != null) {
				documentation += super.getDocumentation(entity);
			}
		}
		String superDocumentation = super.getDocumentation(object);
		if	(superDocumentation != null) {
			documentation += superDocumentation;
		}
		return documentation;
	}
}
