/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 *
 */
package org.eclipse.osbp.xtext.blip.ui.quickfix

import org.eclipse.osbp.xtext.blip.validation.BlipDSLValidator
import org.eclipse.osbp.xtext.oxtype.ui.quickfix.OXtypeQuickfixProvider
import org.eclipse.xtext.ui.editor.quickfix.Fix
import org.eclipse.xtext.ui.editor.quickfix.IssueResolutionAcceptor
import org.eclipse.xtext.validation.Issue

/**
 * Custom quickfixes.
 *
 * see http://www.eclipse.org/Xtext/documentation.html#quickfixes
 */
class BlipDSLQuickfixProvider extends OXtypeQuickfixProvider {

	@Fix(BlipDSLValidator::RECOMMENDED_BLIP_NAME)
	def recommendedBlipName(Issue issue, IssueResolutionAcceptor acceptor) {
		val recommendedBlipName = issue.data.get(0)
		acceptor.accept(issue, '''Use BPMs id '«recommendedBlipName»' as process name''',
			'''Use BPMs id '«recommendedBlipName»' as process name.''', null) [ context |
			val xtextDocument = context.xtextDocument
			xtextDocument.replace(issue.offset, issue.length, recommendedBlipName)
		]
	}

	@Fix(BlipDSLValidator::RECOMMENDED_BLIP_ITEM_NAME)
	def recommendedBlipItemName(Issue issue, IssueResolutionAcceptor acceptor) {
		val recommendedBlipItemName = issue.data.get(0)
		acceptor.accept(issue, '''Use BPMs item id '«recommendedBlipItemName»' as item name''',
			'''Use BPMs item id '«recommendedBlipItemName»' as item name.''', null) [ context |
			val xtextDocument = context.xtextDocument
			xtextDocument.replace(issue.offset, issue.length, recommendedBlipItemName)
		]
	}
}
