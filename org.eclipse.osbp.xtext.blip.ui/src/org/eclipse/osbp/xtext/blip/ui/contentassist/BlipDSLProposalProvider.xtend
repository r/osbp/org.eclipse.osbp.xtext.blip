/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 *
 */
 
package org.eclipse.osbp.xtext.blip.ui.contentassist

import com.google.inject.Inject
import org.eclipse.emf.ecore.EObject
import org.eclipse.jface.viewers.StyledString
import org.eclipse.osbp.xtext.basic.ui.contentassist.BasicDSLProposalProviderHelper
import org.eclipse.osbp.xtext.blip.BlipItem
import org.eclipse.osbp.xtext.blip.common.BlipHelper
import org.eclipse.swt.graphics.Image
import org.eclipse.swt.layout.FillLayout
import org.eclipse.swt.widgets.Shell
import org.eclipse.xtext.Assignment
import org.eclipse.xtext.RuleCall
import org.eclipse.xtext.common.ui.contentassist.TerminalsProposalProvider
import org.eclipse.xtext.ui.editor.contentassist.ConfigurableCompletionProposal
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor
import org.eclipse.xtext.ui.editor.contentassist.ReplacementTextApplier
import org.mihalis.opal.imageSelector.ImageSelectorDialog

class ImageFileNameTextApplier extends ReplacementTextApplier {
	var ContentAssistContext context
	var String[] extensions

	def setContext(ContentAssistContext context) {
		this.context = context
	}

	def setExtensions(String[] fileExtensions) {
		extensions = fileExtensions
	}

	// this will inject a file dialog when selecting the file picker proposal 
	override getActualReplacementString(ConfigurableCompletionProposal proposal) {
		var display = context.getViewer().getTextWidget().getDisplay();
		var shell = new Shell(display);
		shell.setLayout(new FillLayout());
		var imageSelectorDialog = new ImageSelectorDialog(shell, 16);
		imageSelectorDialog.setFilterExtensions(extensions)
		var imageFileName = imageSelectorDialog.open();
//		fileName = fileName.replace("\\", "/")
		return "\"".concat(imageFileName).concat("\"");
	}
}

//class Bpmn2FileNameTextApplier extends ReplacementTextApplier {
//	var ContentAssistContext context
//	var String[] extensions
//
//	def setContext(ContentAssistContext context) {
//		this.context = context
//	}
//
//	def setExtensions(String[] fileExtensions) {
//		extensions = fileExtensions
//	}
//
//	// this will inject a file dialog when selecting the file picker proposal 
//	override getActualReplacementString(ConfigurableCompletionProposal proposal) {
//		var display = context.getViewer().getTextWidget().getDisplay();
//		var shell = new Shell(display);
//		var dialog = new FileDialog(shell, SWT.OPEN);
//		var bundleDirectory = ""
//   		dialog.filterExtensions = extensions;
//   		try {
//   			val tokens = context.resource.URI.toString.split("/")
//   			var base = null as String
//   			for	(var i = 0; i < 3; i++) {
//   				if	(base == null) {
//   					base = "";
//   				}
//   				else {
//   					base += "/"
//   				}
//   				base += tokens.get(i)
//   			}
//	   		val url = new URL(base)
//   			bundleDirectory = FileLocator.toFileURL(url).toString.replace("file:", "")
//   			var searchDirectory = bundleDirectory
//   			if	((new File(searchDirectory+"/models")).isDirectory) {
//   				searchDirectory = searchDirectory+"/models"
//   			}
//   			if	((new File(searchDirectory+"/bpmn")).isDirectory) {
//   				searchDirectory = searchDirectory+"/bpmn"
//   			}
//   			if	((new File(searchDirectory+"/bpmn2")).isDirectory) {
//   				searchDirectory = searchDirectory+"/bpmn2"
//   			}
//   			dialog.filterPath = searchDirectory
//   		}
//   		catch (Exception e) {}
//   		var result = dialog.open();
//   		if	(result != null) {
//   			result = '''"«result.replace("\\", "/").replace(bundleDirectory+"/", "")»"'''
//   		}
//   		return result
//	}
//}


/**
 * see http://www.eclipse.org/Xtext/documentation.html#contentAssist on how to customize content assistant
 */
class BlipDSLProposalProvider extends AbstractBlipDSLProposalProvider {
	
	@Inject TerminalsProposalProvider provider
	@Inject BasicDSLProposalProviderHelper providerHelper
	
//	override completeCCBlip_ResourceName(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
//		bpmnFilePickerProposal(model, assignment, context, acceptor, "*.bpmn2")
//	}

	override completeBlipDtoPath_DtoPath(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		//super.completeBlipDtoPath_DtoPath(model, assignment, context, acceptor)
        val document = context.viewer.document.get.replace("\\\"", "''")
        // --- find the whole attribute-value ---
        var nextquote = context.replaceRegion.offset
        var character = document.charAt(nextquote)
        while ((nextquote < document.length) && !Character.isWhitespace(character)) {
        	nextquote += 1
        	character = document.charAt(nextquote)
        }
        // --- select the whole attribute-value ---
        context.viewer.setSelectedRange(context.replaceRegion.offset, nextquote - context.replaceRegion.offset)
        for	(proposal : BlipHelper.getAvailableDtoPaths(model as BlipItem)) {
			acceptor.accept(doCreateInStringProposal(proposal, new StyledString(proposal), null,
	    		getPriorityHelper().getDefaultPriority(), context));
        }
    }
    
    def private ConfigurableCompletionProposal doCreateInStringProposal(String proposal, StyledString displayString, Image image,
            int priority, ContentAssistContext context) {
        val selectedRange = context.viewer.getSelectedRange()
        val replacementOffset = selectedRange.x; // context.getReplaceRegion().getOffset();
        val replacementLength = selectedRange.y; // context.getReplaceRegion().getLength();
        val result = doCreateProposal(proposal, displayString, image, replacementOffset, replacementLength);
        result.setPriority(priority);
        result.setMatcher(context.getMatcher());
        result.setReplaceContextLength(context.getReplaceContextLength());
        return result;
    }

//	def bpmnFilePickerProposal(EObject model, Assignment assignment, ContentAssistContext context,
//		ICompletionProposalAcceptor acceptor, String fileExtensions) {
//		var fileName = createCompletionProposal("Select BPMN2 file...", context) as ConfigurableCompletionProposal
//		if (fileName != null) {
//			var applier = new Bpmn2FileNameTextApplier()
//			applier.setExtensions(fileExtensions.split(","))
//			applier.setContext(context)
//			fileName.setTextApplier = applier
//		}
//		acceptor.accept(fileName)
//	}
	
	override completeBlip_Image(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		imageFilePickerProposal(model, assignment, context, acceptor, ".jpg,.jpeg,.png")
	}
	
	def imageFilePickerProposal(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor, String fileExtensions) {
		var fileName = createCompletionProposal("Select image file...", context) as ConfigurableCompletionProposal
		if (fileName != null) {
			var applier = new ImageFileNameTextApplier()
			applier.setExtensions(fileExtensions.split(","))
			applier.setContext(context)
			fileName.setTextApplier = applier
		}
		acceptor.accept(fileName)
	}
	
	override public void complete_QualifiedName(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		providerHelper.complete_PackageName(model, ruleCall, context, acceptor, this)
	}
	
	override public void complete_QualifiedNameWithWildcard(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		providerHelper.complete_PackageName(model, ruleCall, context, acceptor, this)
	}
	
	// ------------------------ delegates to TerminalsProposalProvider -----------------
	override public void complete_TRANSLATABLESTRING(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		provider.complete_STRING(model, ruleCall, context, acceptor)
	}

	override public void complete_TRANSLATABLEID(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		provider.complete_ID(model, ruleCall, context, acceptor)
	}
	
}
