/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Contributors:
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.blip.ui.contentassist;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.osbp.xtext.basic.ui.contentassist.BasicDSLProposalProviderHelper;
import org.eclipse.osbp.xtext.blip.BlipItem;
import org.eclipse.osbp.xtext.blip.common.BlipHelper;
import org.eclipse.osbp.xtext.blip.ui.contentassist.AbstractBlipDSLProposalProvider;
import org.eclipse.osbp.xtext.blip.ui.contentassist.ImageFileNameTextApplier;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.xtext.Assignment;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.common.ui.contentassist.TerminalsProposalProvider;
import org.eclipse.xtext.ui.editor.contentassist.ConfigurableCompletionProposal;
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext;
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor;

/**
 * see http://www.eclipse.org/Xtext/documentation.html#contentAssist on how to customize content assistant
 */
@SuppressWarnings("all")
public class BlipDSLProposalProvider extends AbstractBlipDSLProposalProvider {
  @Inject
  private TerminalsProposalProvider provider;
  
  @Inject
  private BasicDSLProposalProviderHelper providerHelper;
  
  @Override
  public void completeBlipDtoPath_DtoPath(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    final String document = context.getViewer().getDocument().get().replace("\\\"", "\'\'");
    int nextquote = context.getReplaceRegion().getOffset();
    char character = document.charAt(nextquote);
    while (((nextquote < document.length()) && (!Character.isWhitespace(character)))) {
      {
        int _nextquote = nextquote;
        nextquote = (_nextquote + 1);
        character = document.charAt(nextquote);
      }
    }
    ITextViewer _viewer = context.getViewer();
    int _offset = context.getReplaceRegion().getOffset();
    int _offset_1 = context.getReplaceRegion().getOffset();
    int _minus = (nextquote - _offset_1);
    _viewer.setSelectedRange(_offset, _minus);
    List<String> _availableDtoPaths = BlipHelper.getAvailableDtoPaths(((BlipItem) model));
    for (final String proposal : _availableDtoPaths) {
      StyledString _styledString = new StyledString(proposal);
      acceptor.accept(this.doCreateInStringProposal(proposal, _styledString, null, 
        this.getPriorityHelper().getDefaultPriority(), context));
    }
  }
  
  private ConfigurableCompletionProposal doCreateInStringProposal(final String proposal, final StyledString displayString, final Image image, final int priority, final ContentAssistContext context) {
    final Point selectedRange = context.getViewer().getSelectedRange();
    final int replacementOffset = selectedRange.x;
    final int replacementLength = selectedRange.y;
    final ConfigurableCompletionProposal result = this.doCreateProposal(proposal, displayString, image, replacementOffset, replacementLength);
    result.setPriority(priority);
    result.setMatcher(context.getMatcher());
    result.setReplaceContextLength(context.getReplaceContextLength());
    return result;
  }
  
  @Override
  public void completeBlip_Image(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    this.imageFilePickerProposal(model, assignment, context, acceptor, ".jpg,.jpeg,.png");
  }
  
  public void imageFilePickerProposal(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor, final String fileExtensions) {
    ICompletionProposal _createCompletionProposal = this.createCompletionProposal("Select image file...", context);
    ConfigurableCompletionProposal fileName = ((ConfigurableCompletionProposal) _createCompletionProposal);
    boolean _notEquals = (!Objects.equal(fileName, null));
    if (_notEquals) {
      ImageFileNameTextApplier applier = new ImageFileNameTextApplier();
      applier.setExtensions(fileExtensions.split(","));
      applier.setContext(context);
      fileName.setTextApplier(applier);
    }
    acceptor.accept(fileName);
  }
  
  @Override
  public void complete_QualifiedName(final EObject model, final RuleCall ruleCall, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    this.providerHelper.complete_PackageName(model, ruleCall, context, acceptor, this);
  }
  
  @Override
  public void complete_QualifiedNameWithWildcard(final EObject model, final RuleCall ruleCall, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    this.providerHelper.complete_PackageName(model, ruleCall, context, acceptor, this);
  }
  
  @Override
  public void complete_TRANSLATABLESTRING(final EObject model, final RuleCall ruleCall, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    this.provider.complete_STRING(model, ruleCall, context, acceptor);
  }
  
  @Override
  public void complete_TRANSLATABLEID(final EObject model, final RuleCall ruleCall, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    this.provider.complete_ID(model, ruleCall, context, acceptor);
  }
}
