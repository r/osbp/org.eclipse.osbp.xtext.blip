/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Contributors:
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.blip.ui.quickfix;

import org.eclipse.osbp.xtext.blip.validation.BlipDSLValidator;
import org.eclipse.osbp.xtext.oxtype.ui.quickfix.OXtypeQuickfixProvider;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.ui.editor.model.IXtextDocument;
import org.eclipse.xtext.ui.editor.model.edit.IModification;
import org.eclipse.xtext.ui.editor.model.edit.IModificationContext;
import org.eclipse.xtext.ui.editor.quickfix.Fix;
import org.eclipse.xtext.ui.editor.quickfix.IssueResolutionAcceptor;
import org.eclipse.xtext.validation.Issue;

/**
 * Custom quickfixes.
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#quickfixes
 */
@SuppressWarnings("all")
public class BlipDSLQuickfixProvider extends OXtypeQuickfixProvider {
  @Fix(BlipDSLValidator.RECOMMENDED_BLIP_NAME)
  public void recommendedBlipName(final Issue issue, final IssueResolutionAcceptor acceptor) {
    final String recommendedBlipName = issue.getData()[0];
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Use BPMs id \'");
    _builder.append(recommendedBlipName);
    _builder.append("\' as process name");
    StringConcatenation _builder_1 = new StringConcatenation();
    _builder_1.append("Use BPMs id \'");
    _builder_1.append(recommendedBlipName);
    _builder_1.append("\' as process name.");
    final IModification _function = (IModificationContext context) -> {
      final IXtextDocument xtextDocument = context.getXtextDocument();
      xtextDocument.replace((issue.getOffset()).intValue(), (issue.getLength()).intValue(), recommendedBlipName);
    };
    acceptor.accept(issue, _builder.toString(), _builder_1.toString(), null, _function);
  }
  
  @Fix(BlipDSLValidator.RECOMMENDED_BLIP_ITEM_NAME)
  public void recommendedBlipItemName(final Issue issue, final IssueResolutionAcceptor acceptor) {
    final String recommendedBlipItemName = issue.getData()[0];
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Use BPMs item id \'");
    _builder.append(recommendedBlipItemName);
    _builder.append("\' as item name");
    StringConcatenation _builder_1 = new StringConcatenation();
    _builder_1.append("Use BPMs item id \'");
    _builder_1.append(recommendedBlipItemName);
    _builder_1.append("\' as item name.");
    final IModification _function = (IModificationContext context) -> {
      final IXtextDocument xtextDocument = context.getXtextDocument();
      xtextDocument.replace((issue.getOffset()).intValue(), (issue.getLength()).intValue(), recommendedBlipItemName);
    };
    acceptor.accept(issue, _builder.toString(), _builder_1.toString(), null, _function);
  }
}
