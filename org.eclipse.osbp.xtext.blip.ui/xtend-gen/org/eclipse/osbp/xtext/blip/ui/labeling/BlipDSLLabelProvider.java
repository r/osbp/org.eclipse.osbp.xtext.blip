/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Contributors:
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.blip.ui.labeling;

import com.google.inject.Inject;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.osbp.xtext.basic.ui.labeling.BasicDSLLabelProvider;
import org.eclipse.osbp.xtext.blip.Blip;
import org.eclipse.osbp.xtext.blip.BlipDto;
import org.eclipse.osbp.xtext.blip.BlipFilter;
import org.eclipse.osbp.xtext.blip.BlipModel;
import org.eclipse.osbp.xtext.blip.BlipWorkload;

/**
 * Provides labels for a EObjects.
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#labelProvider
 */
@SuppressWarnings("all")
public class BlipDSLLabelProvider extends BasicDSLLabelProvider {
  @Inject
  public BlipDSLLabelProvider(final AdapterFactoryLabelProvider delegate) {
    super(delegate);
  }
  
  @Override
  public Object text(final Object o) {
    Object _switchResult = null;
    boolean _matched = false;
    if (o instanceof Blip) {
      _matched=true;
      _switchResult = this.generateText(o, "blip", ((Blip) o).getName());
    }
    if (!_matched) {
      if (o instanceof BlipWorkload) {
        _matched=true;
        _switchResult = this.generateText(o, "blip workload");
      }
    }
    if (!_matched) {
      if (o instanceof BlipDto) {
        _matched=true;
        _switchResult = this.generateText(o, "blip dto");
      }
    }
    if (!_matched) {
      if (o instanceof BlipFilter) {
        _matched=true;
        _switchResult = this.generateText(o, "blip filter", ((BlipFilter) o).getFilterName());
      }
    }
    if (!_matched) {
      _switchResult = super.text(o);
    }
    return _switchResult;
  }
  
  @Override
  public Object image(final Object o) {
    Object _switchResult = null;
    boolean _matched = false;
    if (o instanceof BlipModel) {
      _matched=true;
      _switchResult = this.getInternalImage("model.png", super.getClass());
    }
    if (!_matched) {
      if (o instanceof Blip) {
        _matched=true;
        _switchResult = this.getInternalImage("dsl_blip.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof BlipWorkload) {
        _matched=true;
        _switchResult = this.getInternalImage("blip_workload.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof BlipDto) {
        _matched=true;
        _switchResult = this.getInternalImage("dsl_dto.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof BlipFilter) {
        _matched=true;
        _switchResult = this.getInternalImage("dsl_filter.png", this.getClass());
      }
    }
    if (!_matched) {
      _switchResult = super.image(o);
    }
    return _switchResult;
  }
}
