/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.xtext.blip;

import org.eclipse.osbp.bpmn2.ecore.BPMnSupportStandaloneSetup;
import org.eclipse.osbp.xtext.builder.xbase.setups.XbaseBundleSpaceStandaloneSetup;

import com.google.inject.Guice;
import com.google.inject.Injector;

@SuppressWarnings("restriction")
public class BlipDSLBundleSpaceStandaloneSetup extends
		BlipDSLStandaloneSetup {

	public static void doSetup() {
		new BlipDSLBundleSpaceStandaloneSetup().createInjectorAndDoEMFRegistration();
		BPMnSupportStandaloneSetup.setup();
	}
	
	@Override
	public Injector createInjectorAndDoEMFRegistration() {
		XbaseBundleSpaceStandaloneSetup.doSetup();

		Injector injector = createInjector();
		register(injector);
		return injector;
	}

	@Override
	public Injector createInjector() {
		return Guice.createInjector(new BlipDSLBundleSpaceRuntimeModule());
	}

}
