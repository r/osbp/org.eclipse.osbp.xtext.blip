/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 *
 */
 
package org.eclipse.osbp.xtext.blip.validation

import com.google.inject.Inject
import org.eclipse.bpmn2.Task
import org.eclipse.bpmn2.impl.TaskImpl
import org.eclipse.bpmn2.impl.UserTaskImpl
import org.eclipse.osbp.bpmn2.ecore.BPMnHelper
import org.eclipse.osbp.xtext.basic.validation.IBasicValidatorDelegate
import org.eclipse.osbp.xtext.blip.Blip
import org.eclipse.osbp.xtext.blip.BlipCallActivity
import org.eclipse.osbp.xtext.blip.BlipDSLPackage
import org.eclipse.osbp.xtext.blip.BlipExclusiveSplitGateway
import org.eclipse.osbp.xtext.blip.BlipItem
import org.eclipse.osbp.xtext.blip.BlipPersistTask
import org.eclipse.osbp.xtext.blip.BlipScriptTask
import org.eclipse.osbp.xtext.blip.BlipUserTask
import org.eclipse.osbp.xtext.blip.common.BlipHelper
import org.eclipse.xtext.validation.Check

/**
 * Custom validation rules. 
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#validation
 */
class BlipDSLValidator extends AbstractBlipDSLValidator {
	@Inject(optional=true) IBasicValidatorDelegate delegate

	@Check
	def checkBPMlicensed(Blip blip) {
		if ((blip.process != null)) {
			if(delegate != null && !delegate.validateCommercial("blip", "net.osbee.bpm")) {
				info('''BPM is needed and not yet licensed. License BPM at www.osbee.net''', blip,
				BlipDSLPackage.Literals.BLIP__PROCESS)
			}
		}
	}

	@Check
	def checkStartsWithCapital(Blip blip) {
		if ((blip.process != null) && !Character.isUpperCase(blip.process.id.charAt(0))) {
			info(
				'''It's recommended, that BPMs id '«blip.process.id»' should start with a capital! You have to edit the BPM definition first to solve this!''',
				blip, BlipDSLPackage.Literals.BLIP__PROCESS)
		}
	}

	public static val RECOMMENDED_BLIP_ITEM_NAME = 'recommendedBlipItemName'

	@Check
	def checkRecommendedItemName(BlipItem blipItem) {
		var bpmItemName = BlipHelper.getBpmItemRecommendedName(blipItem)
		if (!bpmItemName.isEmpty && !blipItem.name.equals(bpmItemName)) {
			warning(
				'''If a BPM is defined, it's recommended to use the BPMs item id '«bpmItemName»' as the item name too!''',
				blipItem, BlipDSLPackage.Literals.BLIP_ITEM__NAME, RECOMMENDED_BLIP_ITEM_NAME, bpmItemName)
		}
	}

	public static val RECOMMENDED_BLIP_NAME = 'recommendedBlipName'

	@Check
	def checkRecommendedProcessName(Blip blip) {
		if ((blip.process != null) && !blip.name.equals(blip.process.id)) {
			warning(
				'''If a BPM is defined, it's recommended to use the BPMs id '«blip.process.id»' as the process name too!''',
				blip, BlipDSLPackage.Literals.BLIP_BASE__NAME, RECOMMENDED_BLIP_NAME, blip.process.id)
		}
	}

	@Check
	def checkSubDtoPathOnBlipItem(BlipItem blipItem) {
		if (BlipHelper.isOperativeDtoDefined(blipItem)) {
			if (!BlipHelper.getAvailableDtoPaths(blipItem).contains(blipItem.dtoPath.dtoPath)) {
				error(
					'Operative Dto is set, but doesn\'t exist inside workload Dto', blipItem,
					BlipDSLPackage.Literals.BLIP_ITEM__DTO_PATH)
			}
		}
	}

	@Check
	def checkRecommendedTaskProperties(TaskImpl task) {
		System.err.println("are there any missing checks for recommended task properties?")
	}

	@Check
	def checkRecommendedUserTaskProperties(UserTaskImpl task) {
		System.err.println("are there any missing checks for recommended user task properties?")
	}

	@Check
	def checkRecommendedUserTaskProperties(BlipUserTask task) {
		if	(task.task instanceof Task) {
			val blip = task.eContainer as Blip
			val bpmTask = task.task
			val taskName = BPMnHelper.getBpmTaskDataInputProperty(bpmTask, "TaskName")
			if	(!bpmTask.name.equals(taskName)) {
				error(
					'''It's required, that the General - Name '«bpmTask.name»' and the User Task - Task Name '«taskName»' of the user task '«bpmTask.name»' in the BPM '«blip.process.id»' have to be identical! You have to edit the BPM definition to solve this!''',
					task, BlipDSLPackage.Literals.BLIP_USER_TASK__TASK)
			}
		}
	}

	@Check
	def checkRecommendedCallActivityProperties(BlipCallActivity blipCallActivity) {
//		if	(blipCallActivity.callActivity instanceof CallActivity) {
//			val blip = blipCallActivity.eContainer as Blip
//			val bpmCallActivity = blipCallActivity.callActivity
//			//val callActivityName = BPMnHelper.getBpmCallActivityDataInputProperty(bpmCallActivity, "SubProcessName")
//			//val callActivityName = bpmCallActivity.name
//			if	((bpmCallActivity.name != null) && (blipCallActivity.name != null) && !bpmCallActivity.name.equals(blipCallActivity.name)) {
//				error(
//					'''It's required, that the General - Name '«bpmCallActivity.name»' and the Call Activity - Process Name '«blipCallActivity.name»' of the call activity '«bpmCallActivity.name»' in the BPM '«blip.process.id»' have to be identical! You have to edit the BPM definition to solve this!''',
//					blipCallActivity, BlipDSLPackage.Literals.BLIP_CALL_ACTIVITY__CALL_ACTIVITY)
//			}
//		}
	}

	@Check
	def checkRecommendedScriptTaskProperties(BlipScriptTask task) {
		System.err.println("are there any missing checks for recommended script task properties?")
	}

	@Check
	def checkRecommendedPersistTaskProperties(BlipPersistTask task) {
		System.err.println("are there any missing checks for recommended persist task properties?")
	}

	@Check
	def checkRecommendedExclusiveSplitGatewayProperties(BlipExclusiveSplitGateway gateway) {
		System.err.println("are there any missing checks for recommended exclusive split gateway properties?")
	}

//	@Check
//	def checkRecommendedInclusiveSplitGatewayProperties(BlipInclusiveSplitGateway gateway) {
//		System.err.println("are there any missing checks for recommended inclusive split gateway properties?")
//	}
}
