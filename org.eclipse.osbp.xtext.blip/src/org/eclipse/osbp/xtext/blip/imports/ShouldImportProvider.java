package org.eclipse.osbp.xtext.blip.imports;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.osbp.dsl.semantic.dto.LDto;
import org.eclipse.osbp.dsl.semantic.dto.OSBPDtoPackage;
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryBlipGroup;
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryDSLPackage;
import org.eclipse.osbp.xtext.oxtype.imports.DefaultShouldImportProvider;

public class ShouldImportProvider extends DefaultShouldImportProvider {

	protected boolean doShouldImport(EObject toImport, EReference eRef, EObject context) {
		return toImport instanceof LDto || toImport instanceof FunctionLibraryBlipGroup;
	}

	protected boolean doShouldProposeAllElements(EObject object, EReference reference) {
		EClass type = reference.getEReferenceType();
		return FunctionLibraryDSLPackage.Literals.FUNCTION_LIBRARY_BLIP_GROUP.isSuperTypeOf(type)
				|| OSBPDtoPackage.Literals.LDTO.isSuperTypeOf(type);
	}
}
