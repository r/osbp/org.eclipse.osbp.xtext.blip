/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 *
 */
 
package org.eclipse.osbp.xtext.blip.common

import java.util.LinkedList
import java.util.List
import org.eclipse.osbp.bpm.NamingConvention
import org.eclipse.osbp.dsl.semantic.common.types.LMultiplicity
import org.eclipse.osbp.dsl.semantic.dto.LAutoInheritDto
import org.eclipse.osbp.dsl.semantic.dto.LDto
import org.eclipse.osbp.dsl.semantic.dto.LDtoAbstractReference
import org.eclipse.osbp.dsl.semantic.dto.impl.LDtoInheritedReferenceImpl
import org.eclipse.osbp.dsl.semantic.dto.impl.LDtoReferenceImpl
import org.eclipse.osbp.dsl.semantic.entity.LEntity
import org.eclipse.osbp.xtext.blip.Blip
import org.eclipse.osbp.xtext.blip.BlipCallActivity
import org.eclipse.osbp.xtext.blip.BlipEvent
import org.eclipse.osbp.xtext.blip.BlipItem
import org.eclipse.osbp.xtext.blip.BlipOutGoing
import org.eclipse.osbp.xtext.blip.BlipScript
import org.eclipse.osbp.xtext.blip.BlipServiceTask
import org.eclipse.osbp.xtext.blip.BlipSplitGateway
import org.eclipse.osbp.xtext.blip.BlipUserTask

class BlipHelper {

	static public def String getBpmItemRecommendedName(BlipItem blipItem) {
		var bpmItemName = ""
		if	(blipItem instanceof BlipEvent) {
			bpmItemName = blipItem.event.name
		}
		else if	(blipItem instanceof BlipSplitGateway) {
			bpmItemName = blipItem.gateway.name
		}
		else if	(blipItem instanceof BlipUserTask) {
			bpmItemName = blipItem.task.name
		}
		else if	(blipItem instanceof BlipCallActivity) {
			bpmItemName = blipItem.callActivity.name
		}
		else if	(blipItem instanceof BlipScript) {
			bpmItemName = blipItem.task.name
		}
		else if	(blipItem instanceof BlipServiceTask) {
			bpmItemName = blipItem.task.name
		}
		else if	(blipItem instanceof BlipOutGoing) {
			bpmItemName = blipItem.sequenceFlow.name
		}
		return NamingConvention.getBpmItemRecommendedName(bpmItemName)
	}

	static public def boolean isOperativeDtoDefined(BlipItem blipItem) {
		return ((blipItem?.dtoPath != null) && !blipItem?.dtoPath.dtoPath.isEmpty)
	}
	
	static public def LDtoAbstractReference getDtoFeature(LDto operativeDto, String dtoChildName) {
		if	(operativeDto != null) {
	        for	(feature : operativeDto.features) {
	        	var attributeName = null as String
	        	if	(feature instanceof LDtoReferenceImpl) {
	        		attributeName = feature.getName
	        	}
	        	if	(feature instanceof LDtoInheritedReferenceImpl) {
	        		attributeName = feature.getInheritedFeature.getName
	        	}
	        	if	(attributeName != null) {
	        		// --- supress recursive references ---
	        		if	(attributeName.equals(dtoChildName) && (feature instanceof LDtoAbstractReference)) {
	        			return feature as LDtoAbstractReference
	        		}
	        	}
	        }
		}
        return null
	}
	
	static public def LDto getPureOperativeLDto(BlipItem blipItem) {
		if	(blipItem.operativeDtoDefined) {
			var operativeDto = (blipItem?.eContainer as Blip)?.workload?.source?.dtoRef
			if	(operativeDto != null && operativeDto.name != null && !operativeDto.name.equals(blipItem?.dtoPath)) {
				// --- only if more than the workload dto is set ---
				var dtoPath = blipItem?.dtoPath.dtoPath
				if	(!dtoPath.startsWith(operativeDto.name+".")) {
					return null
				}
				else {
					dtoPath = dtoPath.substring((operativeDto.name+".").length)
					val dtoChildNames = dtoPath.split('\\.')
					for (dtoChildName : dtoChildNames) {
						operativeDto = getReferencedLDto(operativeDto, dtoChildName)
					}
				}
			}
			return operativeDto
		}
		return null
	}

	static public def LDtoAbstractReference getDtoFeature(BlipItem blipItem) {
		var dtoFeature = null as LDtoAbstractReference
		var operativeDto = null as LDto
		if	(BlipHelper.isOperativeDtoDefined(blipItem)) {
			operativeDto = (blipItem?.eContainer as Blip)?.workload?.source?.dtoRef
			if	(operativeDto != null && operativeDto.name != null && !operativeDto.name.equals(blipItem?.dtoPath)) {
				// --- only if more than the workload dto is set ---
				val dtoChildNames = blipItem?.dtoPath.dtoPath.replace(operativeDto.name+".", "").split('\\.')
				for (dtoChildName : dtoChildNames) {
					dtoFeature = BlipHelper.getDtoFeature(operativeDto, dtoChildName)
					operativeDto = BlipHelper.getReferencedLDto(operativeDto, dtoChildName)
				}
			}
		}
		return dtoFeature
	}

	static public def LEntity getOperativeEntity(BlipItem blipItem) {
		var dto = blipItem.operativeLDto
		if	(dto instanceof LAutoInheritDto) {
			if	(dto.wrappedType instanceof LEntity) {
				return dto.wrappedType as LEntity
			}
		}
		return null
	}
	
	static public def LDto getOperativeLDto(BlipItem blipItem) {
		val operativeDto = blipItem.pureOperativeLDto
		if	(operativeDto == null) {
			return (blipItem?.eContainer as Blip)?.workload?.source?.dtoRef
		}
		else {
			return operativeDto
		}
	}
	
	static public def LDto getReferencedLDto(LDto operativeDto, String dtoChildName) {
		var dtoFeature = getDtoFeature(operativeDto, dtoChildName)
		if	(dtoFeature != null) {
    		return dtoFeature.getType
		}
        return null
	}
	
	static public def List<String> getAvailableDtoPaths(BlipItem blipItem) {
        return (blipItem.eContainer as Blip).availableDtoPaths
    }

	static public def List<String> getAvailableDtoPaths(Blip blip) {
		var results = newArrayList()
        val baseDto = blip?.workload?.source?.dtoRef
        if	(baseDto != null) {
        	val dtoStack = newLinkedList(baseDto)
        	recursiveCompleteBlipItem_DtoPath(results, baseDto.name, dtoStack)
        }
        return results
	}

	/**
	 * TODO: find a more meaningful solution to find unnecessary recursive dto paths
	 */
	static private def recursiveCompleteBlipItem_DtoPath(List<String> results, String parentName, LinkedList<LDto> dtoStack) {
		val proposal = parentName
		results.add(proposal)
		val actualDto = dtoStack.last
		var previousDto = null as LDto
		if	(dtoStack.size > 1) {
			previousDto = dtoStack.get(dtoStack.size-2)
		} 
        for	(feature : actualDto.features) {
        	var referencedDto = null as LDto
        	var multiplicity = null as LMultiplicity
        	var attributeName = ""
        	if	(feature instanceof LDtoReferenceImpl) {
        		referencedDto = feature.getType
        		attributeName = feature.getName()
        		multiplicity = feature.getMultiplicity
        	}
        	if	(feature instanceof LDtoInheritedReferenceImpl) {
        		referencedDto = feature.getType
        		attributeName = feature.getInheritedFeature().name
        		multiplicity = feature.getInheritedFeature().multiplicity
        	}
        	if	(referencedDto != null) {
        		// --- supress recursive references ---
        		var lastDtoNames = '''«actualDto.name».«referencedDto.name»'''
        		var parentNames = parentName.split("\\.")
        		var lastAttributeNames = '''«parentNames.get(parentNames.size-1)».«attributeName»'''
        		if	((previousDto == null) || !previousDto.name.equals(referencedDto.name)) {
        			if	((multiplicity != null) && !parentName.contains(lastAttributeNames) && !dtoStack.dtoStackNames.contains(lastDtoNames)) {
	        			dtoStack.addLast(referencedDto)
//	        			System.err.println('''---''')
//	        			System.err.println('''«parentName».«attributeName»''')
//	        			System.err.println(dtoStack.dtoStackNames)
	 					// TODO: find a more meaningful solution to find unnecessary recursive dto paths ...
						if	(dtoStack.size < 32) {
	 					// TODO: ... find a more meaningful solution to find unnecessary recursive dto paths
		    				recursiveCompleteBlipItem_DtoPath(results, '''«parentName».«attributeName»''', dtoStack)
						}
						else {
							System.err.println("BREAKING recursive inspection of dto references due to meaningful depth!")
						}
	    				dtoStack.removeLast 
        			}
        		}
        	}
        }
	}
	
	static def private String dtoStackNames(LinkedList<LDto> dtoStack) {
		var names = ""
		val iterator = dtoStack.iterator
		while (iterator.hasNext) {
			names = '''«names».«iterator.next.name»'''
		}
		return names
	}
}
