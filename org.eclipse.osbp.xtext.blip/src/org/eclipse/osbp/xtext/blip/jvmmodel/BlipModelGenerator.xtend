/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 *
 */
 
package org.eclipse.osbp.xtext.blip.jvmmodel

import java.util.ArrayList
import java.util.Arrays
import java.util.Collection
import java.util.Collections
import java.util.HashMap
import java.util.List
import javax.inject.Inject
import org.eclipse.e4.core.contexts.ContextInjectionFactory
import org.eclipse.e4.ui.model.application.MApplication
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.osbp.bpm.AbstractBPMServiceTask
import org.eclipse.osbp.bpm.AbstractBlipBPMItem
import org.eclipse.osbp.bpm.AbstractBlipBPMUserTask
import org.eclipse.osbp.bpm.BPMCallActivity
import org.eclipse.osbp.bpm.BPMEndEvent
import org.eclipse.osbp.bpm.BPMScriptTask
import org.eclipse.osbp.bpm.BPMSplitGateway
import org.eclipse.osbp.bpm.BPMSplitGateway.GatewayMode
import org.eclipse.osbp.bpm.BlipBPMOutgoing
import org.eclipse.osbp.bpm.BlipBPMStartInfo
import org.eclipse.osbp.bpm.api.IBPMEngine
import org.eclipse.osbp.bpm.api.IBlipBPMFunctionProvider
import org.eclipse.osbp.bpm.api.IBlipBPMWorkloadModifiableItem
import org.eclipse.osbp.bpm.api.ServiceExecutionMode
import org.eclipse.osbp.bpm.api.ServiceImplementation
import org.eclipse.osbp.dsl.common.datatypes.IDto
import org.eclipse.osbp.ui.api.useraccess.IBlipProcessPermissions
import org.eclipse.osbp.xtext.basic.generator.BasicDslGeneratorUtils
import org.eclipse.osbp.xtext.blip.BlipPackage
import org.eclipse.osbp.xtext.i18n.I18NModelGenerator
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.xbase.compiler.GeneratorConfig
import org.eclipse.xtext.xbase.compiler.ImportManager
import org.eclipse.xtext.xbase.jvmmodel.IJvmDeclaredTypeAcceptor
import org.osgi.framework.FrameworkUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.eclipse.osbp.preferences.ProductConfiguration

class BlipModelGenerator extends I18NModelGenerator {
	@Inject extension BasicDslGeneratorUtils
	
	var public static String pckgName = null
	
	def void generatePckgName(BlipPackage pckg, IJvmDeclaredTypeAcceptor acceptor) {
		pckgName = pckg.getName
	}

	override doGenerate(Resource input, IFileSystemAccess fsa) {
		super.doGenerate(input, fsa)
	}
	
	override createAppendable(EObject context, ImportManager importManager, GeneratorConfig config) {
		// required to initialize the needed builder to avoid deprecated methods
		builder = context.eResource
		// ---------
		addImportFor(importManager, _typeReferenceBuilder
			, Arrays
			, Collections
			, HashMap
			, BlipBPMStartInfo
			, AbstractBlipBPMItem
			, AbstractBlipBPMUserTask
			, AbstractBPMServiceTask
			, ServiceExecutionMode
			, ServiceImplementation
			, BPMEndEvent
			, BPMScriptTask
			, BPMSplitGateway
			, BPMCallActivity
			, GatewayMode
			, BlipBPMOutgoing
			, IBlipBPMWorkloadModifiableItem
			, IDto
			, IBlipBPMFunctionProvider
			, MApplication
			, ContextInjectionFactory
			, IBlipProcessPermissions	
			, IBPMEngine
			, Collection
			, List
			, ArrayList
			, Logger
			, LoggerFactory
			, FrameworkUtil
			, ProductConfiguration)
		super.createAppendable(context, importManager, config)
	}
	
}
