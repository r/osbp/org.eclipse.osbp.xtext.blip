/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 *
 */

package org.eclipse.osbp.xtext.blip.jvmmodel

import java.util.HashSet
import java.util.List
import java.util.TreeSet
import java.util.regex.Pattern
import javax.inject.Inject
import org.apache.commons.lang3.StringUtils
import org.eclipse.bpmn2.DocumentRoot
import org.eclipse.core.runtime.CoreException
import org.eclipse.core.runtime.IStatus
import org.eclipse.core.runtime.MultiStatus
import org.eclipse.emf.ecore.EObject
import org.eclipse.osbp.bpm.AbstractBPMServiceTask
import org.eclipse.osbp.bpm.AbstractBlipBPMFunctionProvider
import org.eclipse.osbp.bpm.AbstractBlipBPMUserTask
import org.eclipse.osbp.bpm.BPMCallActivity
import org.eclipse.osbp.bpm.BPMEndEvent
import org.eclipse.osbp.bpm.BPMPersistTask
import org.eclipse.osbp.bpm.BPMScriptTask
import org.eclipse.osbp.bpm.BPMSplitGateway
import org.eclipse.osbp.bpm.BPMStartEvent
import org.eclipse.osbp.bpm.BlipBPMStartInfo
import org.eclipse.osbp.bpm.api.IBPMEngine
import org.eclipse.osbp.bpm.api.IBlipBPMFunctionProvider
import org.eclipse.osbp.bpm.api.IBlipBPMWorkloadModifiableItem
import org.eclipse.osbp.bpm.api.ServiceExecutionMode
import org.eclipse.osbp.dsl.common.datatypes.IDto
import org.eclipse.osbp.dsl.common.xtext.extensions.AnnotationExtension
import org.eclipse.osbp.dsl.semantic.dto.LDto
import org.eclipse.osbp.dsl.semantic.dto.LDtoInheritedReference
import org.eclipse.osbp.dsl.semantic.dto.LDtoReference
import org.eclipse.osbp.xtext.blip.Blip
import org.eclipse.osbp.xtext.blip.BlipCallActivity
import org.eclipse.osbp.xtext.blip.BlipEndEvent
import org.eclipse.osbp.xtext.blip.BlipExclusiveSplitGateway
import org.eclipse.osbp.xtext.blip.BlipInclusiveSplitGateway
import org.eclipse.osbp.xtext.blip.BlipItem
import org.eclipse.osbp.xtext.blip.BlipPackage
import org.eclipse.osbp.xtext.blip.BlipPersistTask
import org.eclipse.osbp.xtext.blip.BlipScriptTask
import org.eclipse.osbp.xtext.blip.BlipServiceTask
import org.eclipse.osbp.xtext.blip.BlipSplitGateway
import org.eclipse.osbp.xtext.blip.BlipStartEvent
import org.eclipse.osbp.xtext.blip.BlipUserTask
import org.eclipse.osbp.xtext.blip.EndEventHandlingEnum
import org.eclipse.osbp.xtext.blip.ServiceExecutionModeEnum
import org.eclipse.osbp.xtext.blip.common.BlipHelper
import org.eclipse.xtext.common.types.JvmDeclaredType
import org.eclipse.xtext.common.types.JvmField
import org.eclipse.xtext.common.types.JvmFormalParameter
import org.eclipse.xtext.common.types.JvmGenericType
import org.eclipse.xtext.common.types.JvmTypeReference
import org.eclipse.xtext.common.types.JvmVisibility
import org.eclipse.xtext.naming.IQualifiedNameProvider
import org.eclipse.xtext.xbase.jvmmodel.AbstractModelInferrer
import org.eclipse.xtext.xbase.jvmmodel.IJvmDeclaredTypeAcceptor
import org.eclipse.xtext.xbase.jvmmodel.JvmTypesBuilder
import org.osgi.service.component.ComponentContext
import org.osgi.service.component.annotations.Activate
import org.osgi.service.component.annotations.Component
import org.osgi.service.component.annotations.Deactivate
import org.osgi.service.component.annotations.Reference
import org.osgi.service.component.annotations.ReferenceCardinality
import org.osgi.service.component.annotations.ReferencePolicy
import org.slf4j.Logger

/**
 * <p>
 * Business Logic Implementation Platform
 * This inferrer infers models of extension blip and generates code to be injected as context function to e4 application 
 * and handles the communication with the bpm service.
 * </p>
 * 
 * @author Joerg Riegel
 */

class BlipDSLJvmModelInferrer extends AbstractModelInferrer {

	@Inject extension JvmTypesBuilder
	@Inject extension IQualifiedNameProvider
	@Inject extension BlipModelGenerator
	@Inject extension AnnotationExtension

   	def dispatch void infer(BlipPackage blipPkg, IJvmDeclaredTypeAcceptor acceptor, boolean isPreIndexingPhase) {
   		blipPkg.generatePckgName(acceptor)
   		blipPkg.createAbstractBpmItemClasses(acceptor)
   		val clsName = blipPkg.className.toFirstUpper
   		val cls = blipPkg.toClass(clsName);
        cls.superTypes.add(_typeReferenceBuilder.typeRef(AbstractBlipBPMFunctionProvider))
        cls.simpleName = cls.simpleName.toFirstUpper
		acceptor.accept(cls,
			[
			var annotationRef = _annotationTypesBuilder.annotationRef(typeof(Component))
			annotationRef.addAnnAttr(blipPkg, "service", _typeReferenceBuilder.typeRef(IBlipBPMFunctionProvider) as JvmTypeReference)
			annotations += annotationRef
			it.fileHeader = blipPkg.documentation
			it.toFields(blipPkg)
			it.toOperations(blipPkg)
			packageName = blipPkg.fullyQualifiedName.toString
		])
   	}

	def blipItemAbstractClassName(BlipItem blipItem) {
		if	(blipItem.dtoPathTokensCount == 2) {
			val blip = blipItem.eContainer as Blip
			'''AbstractBPMItem«blip.name.toFirstUpper»«blipItem.name.toFirstUpper»'''
		}
		else {
			blipItem.blipItemAbstractClass.simpleName
		}
	}

	def blipItemAbstractClass(BlipItem blipItem) {
		if	(blipItem instanceof BlipUserTask) {
			AbstractBlipBPMUserTask
		}
		else if	(blipItem instanceof BlipCallActivity) {
			BPMCallActivity
		}
		else if	(blipItem instanceof BlipScriptTask) {
			BPMScriptTask
		}
		else if	(blipItem instanceof BlipPersistTask) {
			BPMPersistTask
		}
		else if	(blipItem instanceof BlipServiceTask) {
			AbstractBPMServiceTask
		}
		else if	(blipItem instanceof BlipSplitGateway) {
			BPMSplitGateway
		}
		else if	(blipItem instanceof BlipStartEvent) {
			BPMStartEvent
		}
		else if	(blipItem instanceof BlipEndEvent) {
			BPMEndEvent
		}
	}

	def getDtoPathTokensCount(BlipItem blipItem) {
		var path = blipItem.dtoPath?.dtoPath
		if(path === null) {
			return 0
		}
		if(!path.contains(".")) {
			return 0
		}
		return path.split(Pattern.quote('.')).size
	}

	def createAbstractBpmItemClasses(BlipPackage blipPkg, IJvmDeclaredTypeAcceptor acceptor) {
		// create special abstract bpm item classes
		for (blip : blipPkg.blips) {
			for	(blipItem : blip.items) {
				if	(blipItem.dtoPathTokensCount == 2) {
					val cls = blipPkg.toClass('''«blipPkg.name».«blipItem.blipItemAbstractClassName.toString»''')
					val operativeDtoClassName = blipItem.operativeDtoFqn
					val dtoChildNames = blipItem?.dtoPath.dtoPath.split('\\.')
					val dtoFeatureName = StringUtils.capitalize(dtoChildNames.last)
					cls.abstract = true
			        cls.superTypes.add(_typeReferenceBuilder.typeRef(blipItem.blipItemAbstractClass))
			        cls.superTypes.add(_typeReferenceBuilder.typeRef(IBlipBPMWorkloadModifiableItem))
					acceptor.accept(cls, [
						cls.members += blipPkg.toConstructor[
							visibility = JvmVisibility.PROTECTED
							parameters += blipPkg.toParameter("blipId", _typeReferenceBuilder.typeRef(String))
							parameters += blipPkg.toParameter("bpmId", _typeReferenceBuilder.typeRef(String))
							parameters += blipPkg.toParameter("onEntryFunction", _typeReferenceBuilder.typeRef(String))
							parameters += blipPkg.toParameter("onExitFunction", _typeReferenceBuilder.typeRef(String))
							body = [
								append(
									'''
									super(blipId, bpmId, onEntryFunction, onExitFunction);''')
								]
							]
						cls.members += blipItem.createMethod(cls, "addOperativeDto", 
							blipPkg.toParameter("operativeDto", _typeReferenceBuilder.typeRef(IDto)),
							dtoFeatureName,
							'''((«blip.workloadDtoFqn»)workloadDto).addTo«dtoFeatureName»((«operativeDtoClassName») operativeDto);'''
						)
						cls.members += blipItem.createMethod(cls, "addOperativeDtos", 
							blipPkg.toParameter("operativeDtos", _typeReferenceBuilder.typeRef(List, _typeReferenceBuilder.typeRef(IDto))),
							dtoFeatureName,
							'''
							for	(IDto operativeDto : operativeDtos) {
								((«blip.workloadDtoFqn»)workloadDto).addTo«dtoFeatureName»((«operativeDtoClassName») operativeDto);
							}
							'''
						)
						cls.members += blipItem.createMethod(cls, "removeAllOperativeDtos", 
							null,
							dtoFeatureName,
							'''((«blip.workloadDtoFqn»)workloadDto).set«dtoFeatureName»(null);'''
						)
						cls.members += blipItem.createMethod(cls, "removeOperativeDto", 
							blipPkg.toParameter("operativeDto", _typeReferenceBuilder.typeRef(IDto)),
							dtoFeatureName,
							'''((«blip.workloadDtoFqn»)workloadDto).removeFrom«dtoFeatureName»((«operativeDtoClassName») operativeDto);'''
						)
						cls.members += blipItem.createMethod(cls, "removeOperativeDtos", 
							blipPkg.toParameter("operativeDtos", _typeReferenceBuilder.typeRef(List, _typeReferenceBuilder.typeRef(IDto))),
							dtoFeatureName,
							'''
							for	(IDto operativeDto : operativeDtos) {
								((«blip.workloadDtoFqn»)workloadDto).removeFrom«dtoFeatureName»((«operativeDtoClassName») operativeDto);
							}
							'''
						)
					])
				}
			}
		}
	}
	
	def createMethod(BlipItem blipItem, JvmGenericType cls, String methodName, JvmFormalParameter parameter2, String getterSetterPostfix, String listModifier) {
		val blip = blipItem.eContainer as Blip
		val blipPckg = blip.eContainer as BlipPackage
		return blipPckg.toMethod(methodName, _typeReferenceBuilder.typeRef(Void::TYPE), [
			annotations += _annotationTypesBuilder.annotationRef(Override)
			visibility = JvmVisibility.PUBLIC
			parameters += blipPckg.toParameter("workloadDto", _typeReferenceBuilder.typeRef(IDto))
			if	(parameter2 !== null) {
				parameters += parameter2
			}
			body = [
				append(
					'''
					«listModifier»
					''')
			]
		])
	}
	
	def String className(BlipPackage pkg) {
		var String clsName = pkg.name.toString.toFirstUpper+"Controller"
		var parts = clsName.split(Pattern.quote("."))
		var len = parts.length
		if (len > 1) {
			return parts.get(len-1).toFirstUpper
		}
		return clsName
	}

	def void toFields(JvmGenericType type, BlipPackage pkg) {
   		var JvmField field = null
   		// create logger
		field = pkg.toField("log", _typeReferenceBuilder.typeRef(Logger))[setInitializer([append('''org.slf4j.LoggerFactory.getLogger("blip")''')])]
   		field.static = true
   		field.visibility = JvmVisibility::PRIVATE
		type.members += field
	}

	def createBPMNCalls(BlipPackage pkg) {
		val CODE = IStatus.ERROR
		val PLUGINID = this.class.canonicalName
		val PREFIX_MESSAGE = "BPMN2 file"
		val POSTFIX_MESSAGE = "could not be found in this bundle!"
		var errors = new HashSet<IStatus>
		var resources = new TreeSet
		var text = ""
		val tokens = pkg.eResource.URI.toString.split("/")
		var base = null as String
		for	(var i = 0; i < 3; i++) {
			if	(base === null) {
				base = tokens.get(i)
			}
			else {
				base = '''«base»/«tokens.get(i)»'''
			}
		}
   		for (blip : pkg.blips) {
			text = 
				'''
				«text»
				startInfos.put("«blip.name»",new «BlipBPMStartInfo.simpleName»("«blip.name»","«blip.name»"
					, "«blip.resourceName»"
					, «IF blip.hasImage»"«blip.image»"«ELSE»null«ENDIF»,«IF blip.hasIcon»"«blip.icon.literal»"«ELSE»null«ENDIF»,«IF blip.hasLogging»«blip.hasLogging.booleanValue»«ELSE»false«ENDIF»
					, «blip.functionGroupClassName»
					, «blip.workloadDtoClassName»
					«blip.createBlipItems»
					));
				'''
		}
		if	(!errors.empty) {
			throw new CoreException(new MultiStatus(PLUGINID, CODE, errors, '''«PREFIX_MESSAGE» «resources.join(",\n")» «POSTFIX_MESSAGE»''', null))
		}
		return text
	}

	def getWorkloadDtoClass(Blip blip) {
		if	(blip.workload === null) {
			return null
		}
		else {
			return blip.workload.source.dtoRef.fullyQualifiedName
		}
	}

	def getFunctionGroupClass(Blip blip) {
		if	(blip.functionGroup === null) {
			return "null"
		}
		return '''«blip.functionGroup.fullyQualifiedName»'''
	}

	def getFunctionGroupClassName(Blip blip) {
		if	(blip.functionGroup === null) {
			return "null"
		}
		return '''«blip.functionGroup.fullyQualifiedName».class'''
	}

	def getWorkloadDtoClassName(Blip blip) {
		if	(blip.workloadDtoFqn.equals("null")) {
			return "null"
		}
		return '''«blip.workloadDtoFqn».class'''
	}

	def getWorkloadDtoFqn(Blip blip) {
		if	(blip.workloadDtoClass === null) {
			return "null"
		}
		else {
			return '''«blip.workloadDtoClass»'''
		}
	}

	def getOperativeDtoClassName(BlipItem blipItem) {
		if	(blipItem.operativeDtoFqn.equals("null")) {
			return "null"
		}
		return '''«blipItem.operativeDtoFqn».class'''
	}

	def getOperativeDtoFqn(BlipItem blipItem) {
		var blip = blipItem.eContainer as Blip
		if	(blip.workloadDtoClass === null) {
			"null"
		}
		else {
			'''«blipItem.operativeLDto.fullyQualifiedName»'''
		}
	}

	def asString(ServiceExecutionModeEnum mode) {
		switch (mode) {
			case ONEWAY:
				ServiceExecutionMode.ONEWAY.toString
			case SYNC:
				ServiceExecutionMode.SYNC.toString
		}
	}

	def createBlipItems(Blip blip) {
		var text = ""
		for	(blipItem : blip.items) {
			val itemClass = blipItem.blipItemAbstractClassName
			switch blipItem {
				BlipUserTask: {
					var onEntry = "null"
					if	(blipItem.onEntry !== null) {
						onEntry = '''"«blipItem.onEntry.name»"'''
					}
					var onExit = "null"
					if	(blipItem.onExit !== null) {
						onExit = '''"«blipItem.onExit.name»"'''
					}
					text = 
						'''
						«text», new «itemClass»("«blipItem.name»", "«blipItem.task.id»", «onEntry», «onExit») {
							@Override
							public Class<?> getOperativeDtoClass() {
								return «blipItem.operativeDtoClassName»;
							}
							@Override
							public List<IDto> getOperativeDtos(IDto item0) {
								«blipItem.operativeDtosBody»
							}
							«blipItem.operativeCastedDtosMethod»
						}
						'''
				}
				BlipCallActivity: {
					var onEntry = "null"
					if	(blipItem.onEntry !== null) {
						onEntry = '''"«blipItem.onEntry.name»"'''
					}
					var onExit = "null"
					if	(blipItem.onExit !== null) {
						onExit = '''"«blipItem.onExit.name»"'''
					}
					text = 
						'''
						«text», new «itemClass»("«blipItem.name»", "«blipItem.callActivity.id»", «onEntry», «onExit»)
						'''
				}
				BlipScriptTask: {
					text = 
						'''
						«text», new «itemClass»("«blipItem.name»", "«blipItem.task.id»", "«blipItem.function.name»")
						'''
				}
				BlipPersistTask: {
					text = 
						'''
						«text», new «itemClass»("«blipItem.name»", "«blipItem.task.id»", "«blipItem.function.name»")
						'''
				}
				BlipServiceTask: {
					var onEntry = "null"
					if	(blipItem.onEntry !== null) {
						onEntry = '''"«blipItem.onEntry.name»"'''
					}
					var onExit = "null"
					if	(blipItem.onExit !== null) {
						onExit = '''"«blipItem.onExit.name»"'''
					}
					var executionMode = ""
					if	(blipItem.timeoutInSecs > 0) {
						executionMode = '''ServiceExecutionMode.ASYNCHRON, «blipItem.timeoutInSecs»'''
					}
					else {
						executionMode = '''ServiceExecutionMode.«blipItem.executionMode.asString», 0'''
					}
					var implementation = ""
					if	(blipItem.function !== null) {
						implementation = '''ServiceImplementation.FUNCTION_LIBRARY, "«blip.functionGroupClass»", "«blipItem.function.name»"'''
					}
					else {
						implementation = '''ServiceImplementation.WEB_SERVICE, "«blipItem.webServiceInterface»", "«blipItem.webServiceOperation»"'''
					}
					text = 
						'''
						«text», new «itemClass»("«blipItem.name»", "«blipItem.task.id»", «onEntry», «onExit», «executionMode», «implementation») {
							@Override
							public Class<?> getOperativeDtoClass() {
								return «blip.workloadDtoClassName»;
							}
							@Override
							public List<IDto> getOperativeDtos(IDto item0) {
								return Collections.emptyList();
							}
						}
						'''
				}
				BlipSplitGateway: {
					var mode = ""
					if	(blipItem instanceof BlipExclusiveSplitGateway) {
						mode = "GatewayMode.EXCLUSIVE"
					}
					else if	(blipItem instanceof BlipInclusiveSplitGateway) {
						mode = "GatewayMode.INCLUSIVE"
					} 
					var outgoings = ""
					var priority = 0
					for (outgoing : blipItem.outgoings) {
						priority = priority + 1
						if	(outgoing.isIsDefault) {
							outgoings = '''
								«outgoings»,
										new BlipBPMOutgoing("«outgoing.name»", "«outgoing.sequenceFlow.id»", «priority»)'''
						}
						else {
							outgoings = '''
								«outgoings»,
										new BlipBPMOutgoing("«outgoing.name»", "«outgoing.sequenceFlow.id»", «priority», "«outgoing.constraint.name»")'''
						}
					}
					text = 
						'''
						«text», new «itemClass»("«blipItem.name»", "«blipItem.gateway.id»", «mode»«outgoings»)
						'''
				}
				BlipEndEvent: {
					text = 
						'''
						«text», new «itemClass»("«blipItem.name»", "«blipItem.event.id»", «IF (blipItem.endHandling == EndEventHandlingEnum.TERMINATES_PROCESS)»true«ELSE»false«ENDIF»)
						'''
				}
			}
		}
		return text
	}

	def getOperativeDtosBody(BlipItem blipItem) {
		var blip = blipItem.eContainer as Blip
		if	(blip.workloadDtoClass === null) {
			'''return null;'''
		}
		else {
			'''
			List<IDto> result = new java.util.ArrayList<>();
			if	(item0 instanceof «blip.workloadDtoClass») {
				for	(«blipItem.operativeLDto.fullyQualifiedName» dto : getOperative«blipItem.operativeLDto.name»s((«blip.workloadDtoClass»)item0)) {
					result.add(dto);
				}
			}
			return result;
			'''
		}
	}

	def getOperativeCastedDtosMethod(BlipItem blipItem) {
		var blip = blipItem.eContainer as Blip
		if	(blip.workloadDtoClass === null) {
			return ""
		}
		else {
			var workloadDto = blip.workload.source.dtoRef
			var operativeLDto = blipItem.operativeLDto
			var body = ""
			if	(workloadDto.equals(operativeLDto)) {
				body =
					'''
					List<«blipItem.operativeLDto.fullyQualifiedName»> result = new java.util.ArrayList<>();
					if	(item0 != null) {
						result.add(item0);
					}
					return result;
					'''
			}
			else {
				body = '''«getOperativeDtoMethod("item0", blipItem)»''';
			}
			var text = 
				'''
				public List<«blipItem.operativeLDto.fullyQualifiedName»> getOperative«blipItem.operativeLDto.name»s(«blip.workloadDtoClass» item0) {
					«body»
				}
				'''
		return text
		}
	}
	
	private def String getOperativeDtoMethod(String rootDto, BlipItem blipItem) {
		if	(BlipHelper.isOperativeDtoDefined(blipItem)) {
			var operativeDto = null as LDto
			operativeDto = (blipItem?.eContainer as Blip)?.workload?.source?.dtoRef
			if	(operativeDto !== null && operativeDto.name !== null && !operativeDto.name.equals(blipItem?.dtoPath)) {
				// --- only if more than the workload dto is set ---
				val dtoChildNames = blipItem?.dtoPath.dtoPath.replace(operativeDto.name+".", "").split('\\.')
				var actual = 1
				var method = ''''''
				for (dtoChildName : dtoChildNames) {
					val previousclass = operativeDto.fullyQualifiedName
					val dtoFeature = BlipHelper.getDtoFeature(operativeDto, dtoChildName)
					operativeDto = BlipHelper.getReferencedLDto(operativeDto, dtoChildName)
					var multiple = false;
					var getter = ""
					val actualclass = operativeDto.fullyQualifiedName
					val previous = actual-1
					if	(dtoFeature instanceof LDtoInheritedReference) {
						multiple = dtoFeature.inheritedFeature.multiplicity !== null
						getter = '''get«StringUtils.capitalize(dtoFeature.inheritedFeature.name)»()'''
					}
					else if	(dtoFeature instanceof LDtoReference) {
						multiple = dtoFeature.multiplicity !== null
						getter = '''get«StringUtils.capitalize(dtoFeature.name)»()'''
					}
					method = 
						'''
						«method»
						List<«actualclass»> result«actual» = new java.util.ArrayList<>();
						'''
					if	(actual > 1) {
						method = 
							'''
							«method»
							for («previousclass» item«previous» : result«previous») {
							'''
					}
					if	(multiple) {
						method = 
							'''
							«method»
							result«actual».addAll(item«previous».«getter»);
							'''
					}
					else {
						method = 
							'''
							«method»
							«actualclass» item = item«previous».«getter»;
							if	(item != null) {
								result«actual».add(item);
							}
							'''
					}
					if	(actual > 1) {
						method = 
							'''
							«method»
							}
							'''
					}
					actual = actual+1
				}
				return 
					'''
					«method»
					return result«(actual-1)»;
					'''
			}
		}
		return '''return «rootDto»;'''
	}

	def static String getResourceName(Blip blip) {
		if	(blip.process !== null) {
			var parent = blip.process as EObject
			while	(parent !== null && !(parent instanceof DocumentRoot)) {
				parent = parent.eContainer
			}
			switch parent {
				DocumentRoot: {
					var uri = parent.eResource.URI
					var relPath = uri.segment(uri.segmentCount-2)+"/"+uri.segment(uri.segmentCount-1)
					return relPath.replace("%20", " ")
				}
			}
		}
		return "<no-process-found>"
	}

	def void toOperations(JvmDeclaredType type, BlipPackage blipPkg) {
   		// init blip
   		var ac = blipPkg.toMethod("activate", _typeReferenceBuilder.typeRef(Void::TYPE), [
   			annotations += _annotationTypesBuilder.annotationRef(typeof(Activate))
   			parameters += blipPkg.toParameter("context", _typeReferenceBuilder.typeRef(ComponentContext))
   			body = [ append(
   			'''
			«blipPkg.createBPMNCalls»
   			''')]
   		])
   		ac.visibility = JvmVisibility.PROTECTED
   		type.members += ac
 	}
}

