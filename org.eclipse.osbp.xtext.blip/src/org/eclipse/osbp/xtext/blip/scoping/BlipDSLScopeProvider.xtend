/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 *
 */
 
package org.eclipse.osbp.xtext.blip.scoping

import com.google.inject.Inject
import org.eclipse.bpmn2.CallActivity
import org.eclipse.bpmn2.ExclusiveGateway
import org.eclipse.bpmn2.FlowElement
import org.eclipse.bpmn2.Gateway
import org.eclipse.bpmn2.GatewayDirection
import org.eclipse.bpmn2.SequenceFlow
import org.eclipse.bpmn2.StartEvent
import org.eclipse.bpmn2.UserTask
import org.eclipse.bpmn2.impl.ScriptTaskImpl
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.osbp.xtext.blip.Blip
import org.eclipse.osbp.xtext.blip.BlipCallActivity
import org.eclipse.osbp.xtext.blip.BlipDSLPackage
import org.eclipse.osbp.xtext.blip.BlipExclusiveSplitGateway
import org.eclipse.osbp.xtext.blip.BlipItem
import org.eclipse.osbp.xtext.blip.BlipOutGoing
import org.eclipse.osbp.xtext.blip.BlipScript
import org.eclipse.osbp.xtext.blip.BlipSplitGateway
import org.eclipse.osbp.xtext.blip.BlipStartEvent
import org.eclipse.osbp.xtext.blip.BlipUserTask
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryFunction
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryTest
import org.eclipse.xtext.resource.EObjectDescription
import org.eclipse.xtext.resource.IEObjectDescription
import org.eclipse.xtext.scoping.IScope
import org.eclipse.xtext.scoping.impl.DefaultGlobalScopeProvider
import org.eclipse.xtext.scoping.impl.MapBasedScope
import org.eclipse.xtext.xbase.annotations.typesystem.XbaseWithAnnotationsBatchScopeProvider
import org.eclipse.osbp.xtext.blip.BlipEndEvent
import org.eclipse.bpmn2.EndEvent
import org.eclipse.osbp.xtext.blip.BlipInclusiveSplitGateway
import org.eclipse.bpmn2.InclusiveGateway
import org.eclipse.osbp.xtext.blip.BlipServiceTask
import org.eclipse.bpmn2.impl.ServiceTaskImpl

class BlipDSLScopeProvider extends AbstractBlipDSLScopeProvider {

	@Inject
	DefaultGlobalScopeProvider globalScopeProvider

	override getScope(EObject context, EReference reference) {
//		if (reference == BlipDSLPackage.Literals.CC_BLIP__PROCESS) {
//			return globalScopeProvider.getScope(context.eResource, reference, null)
//		}
		if (reference == BlipDSLPackage.Literals.BLIP_SCRIPT__TASK) {
			return getScope_BLIP_SCRIPT_TASK(context as BlipScript)
		}
		if (reference == BlipDSLPackage.Literals.BLIP_USER_TASK__TASK) {
			return getScope_BLIP_USER_TASK(context as BlipUserTask)
		}
		if (reference == BlipDSLPackage.Literals.BLIP_CALL_ACTIVITY__CALL_ACTIVITY) {
			return getScope_BLIP_CALL_ACTIVITY(context as BlipCallActivity)
		}
		if (reference == BlipDSLPackage.Literals.BLIP_SPLIT_GATEWAY__GATEWAY) {
			return getScope_BLIP_SPLIT_GATEWAY_GATEWAY(context as BlipSplitGateway)
		}
		if (reference == BlipDSLPackage.Literals.BLIP_OUT_GOING__SEQUENCE_FLOW) {
			return getScope_BLIP_OUTGOING_SEQUENCE(context as BlipOutGoing)
		}
		if (reference == BlipDSLPackage.Literals.BLIP_EVENT__EVENT) {
			switch context {
				BlipStartEvent: return getScope_BLIP_START_EVENT__EVENT(context)
				BlipEndEvent: return getScope_BLIP_END_EVENT__EVENT(context)
			}
		}
		if (reference == BlipDSLPackage.Literals.BLIP_USER_TASK__ON_ENTRY) {
			return getScope_BLIP_ITEM__FUNCTION(context as BlipItem)
		}
		if (reference == BlipDSLPackage.Literals.BLIP_USER_TASK__ON_EXIT) {
			return getScope_BLIP_ITEM__FUNCTION(context as BlipItem)
		}
		if (reference == BlipDSLPackage.Literals.BLIP_CALL_ACTIVITY__ON_ENTRY) {
			return getScope_BLIP_ITEM__FUNCTION(context as BlipItem)
		}
		if (reference == BlipDSLPackage.Literals.BLIP_CALL_ACTIVITY__ON_EXIT) {
			return getScope_BLIP_ITEM__FUNCTION(context as BlipItem)
		}
		if (reference == BlipDSLPackage.Literals.BLIP_SCRIPT__FUNCTION) {
			return getScope_BLIP_ITEM__FUNCTION(context as BlipItem)
		}
		if (reference == BlipDSLPackage.Literals.BLIP_SERVICE_TASK__TASK) {
			return getScope_BLIP_SERVICE_TASK(context as BlipServiceTask)
		}
		if (reference == BlipDSLPackage.Literals.BLIP_SERVICE_TASK__ON_ENTRY) {
			return getScope_BLIP_ITEM__FUNCTION(context as BlipItem)
		}
		if (reference == BlipDSLPackage.Literals.BLIP_SERVICE_TASK__ON_EXIT) {
			return getScope_BLIP_ITEM__FUNCTION(context as BlipItem)
		}
		if (reference == BlipDSLPackage.Literals.BLIP_SERVICE_TASK__FUNCTION) {
			return getScope_BLIP_ITEM__FUNCTION(context as BlipItem)
		}
		if (reference == BlipDSLPackage.Literals.BLIP_OUT_GOING__CONSTRAINT) {
			return getScope_BLIP_ITEM__TEST(context as BlipItem)
		}
//		if (reference == BlipDSLPackage.Literals.BLIP_ITEM__DTO_PATH) {
//			return getScope_BLIP_ITEM__DTO_PATH(context as BlipItem)
//		}
		super.getScope(context, reference)
	}
	
	def IScope getScope_BLIP_ITEM__FUNCTION(BlipItem blipItem) {
		var result = <IEObjectDescription>newArrayList
		var blip = blipItem as EObject
		while	(!(blip instanceof Blip)) {
			blip = blip.eContainer
		}
		if ((blip instanceof Blip) && ((blip as Blip).functionGroup != null)) {
	        for (FunctionLibraryFunction element : (blip as Blip).functionGroup.functions) {
                result.add(EObjectDescription.create(element.name, (element as FunctionLibraryFunction)))
	        }
        }
		return MapBasedScope.createScope(IScope.NULLSCOPE, result)
	}
	
	def IScope getScope_BLIP_ITEM__TEST(BlipItem blipItem) {
		var result = <IEObjectDescription>newArrayList
		var blip = blipItem as EObject
		while	(!(blip instanceof Blip)) {
			blip = blip.eContainer
		}
		if ((blip instanceof Blip) && ((blip as Blip).functionGroup != null)) {
	        for (FunctionLibraryTest element : (blip as Blip).functionGroup.tests) {
                result.add(EObjectDescription.create(element.name, (element as FunctionLibraryTest)))
	        }
        }
		return MapBasedScope.createScope(IScope.NULLSCOPE, result)
	}
	
	def IScope getScope_BLIP_SCRIPT_TASK(BlipScript scriptTask) {
		var result = <IEObjectDescription>newArrayList
		var blip = scriptTask.eContainer as Blip
		if (blip != null && blip.process != null && blip.process.flowElements != null) {
	        for (FlowElement element : blip.process.flowElements) {
	        	if	(element instanceof ScriptTaskImpl) {
	                result.add(EObjectDescription.create(element.id, (element as ScriptTaskImpl)))
	            }
	        }
        }
		return MapBasedScope.createScope(IScope.NULLSCOPE, result)
	}
	
	def IScope getScope_BLIP_SERVICE_TASK(BlipServiceTask serviceTask) {
		var result = <IEObjectDescription>newArrayList
		var blip = serviceTask.eContainer as Blip
		if (blip != null && blip.process != null && blip.process.flowElements != null) {
	        for (FlowElement element : blip.process.flowElements) {
	        	if	(element instanceof ServiceTaskImpl) {
	                result.add(EObjectDescription.create(element.id, (element as ServiceTaskImpl)))
	            }
	        }
        }
		return MapBasedScope.createScope(IScope.NULLSCOPE, result)
	}
	
	def IScope getScope_BLIP_USER_TASK(BlipUserTask humanTask) {
		var result = <IEObjectDescription>newArrayList
		var blip = humanTask.eContainer as Blip
		if (blip != null && blip.process != null && blip.process.flowElements != null) {
	        for (FlowElement element : blip.process.flowElements) {
	        	if	(element instanceof UserTask) {
	                result.add(EObjectDescription.create(element.id, (element as UserTask)))
	            }
	        }
        }
		return MapBasedScope.createScope(IScope.NULLSCOPE, result)
	}
	
	def IScope getScope_BLIP_CALL_ACTIVITY(BlipCallActivity callActivity) {
		var result = <IEObjectDescription>newArrayList
		var blip = callActivity.eContainer as Blip
		if (blip != null && blip.process != null && blip.process.flowElements != null) {
	        for (FlowElement element : blip.process.flowElements) {
	        	if	(element instanceof CallActivity) {
	                result.add(EObjectDescription.create(element.id, (element as CallActivity)))
	            }
	        }
        }
		return MapBasedScope.createScope(IScope.NULLSCOPE, result)
	}
	
	def IScope getScope_BLIP_SPLIT_GATEWAY_GATEWAY(BlipSplitGateway gateway) {
		var result = <IEObjectDescription>newArrayList
		var blip = gateway.eContainer as Blip
		if (blip != null && blip.process != null && blip.process.flowElements != null) {
	        for (FlowElement element : blip.process.flowElements) {
	        	if	(element instanceof Gateway) {
	        		if	(element.gatewayDirection == GatewayDirection.DIVERGING) {
	        			if	((gateway instanceof BlipExclusiveSplitGateway) && (element instanceof ExclusiveGateway)) {
	                		result.add(EObjectDescription.create(element.id, (element as Gateway)))
	        			}
	        			else if	((gateway instanceof BlipInclusiveSplitGateway) && (element instanceof InclusiveGateway)) {
	                		result.add(EObjectDescription.create(element.id, (element as Gateway)))
	        			}
	        		}
	            }
	        }
        }
		return MapBasedScope.createScope(IScope.NULLSCOPE, result)
	}
	
	def IScope getScope_BLIP_OUTGOING_SEQUENCE(BlipOutGoing outgoing) {
		var result = <IEObjectDescription>newArrayList
		var gateway = outgoing.eContainer as BlipSplitGateway
		if (gateway != null && gateway.gateway != null && !gateway.gateway.outgoing.isEmpty) {
	        for (FlowElement element : gateway.gateway.outgoing) {
	        	if	(element instanceof SequenceFlow) {
                	result.add(EObjectDescription.create(element.id, (element as SequenceFlow)))
	            }
	        }
        }
		return MapBasedScope.createScope(IScope.NULLSCOPE, result)
	}
	
	def IScope getScope_BLIP_START_EVENT__EVENT(BlipStartEvent blipStartEvent) {
		var result = <IEObjectDescription>newArrayList
		var blip = blipStartEvent.eContainer as Blip
		if (blip != null && blip.process != null && blip.process.flowElements != null) {
	        for (FlowElement element : blip.process.flowElements) {
	        	if	(element instanceof StartEvent) {
	                result.add(EObjectDescription.create(element.id, (element as StartEvent)))
	            }
	        }
	    }
		return MapBasedScope.createScope(IScope.NULLSCOPE, result)
	}
	
	def IScope getScope_BLIP_END_EVENT__EVENT(BlipEndEvent blipEndEvent) {
		var result = <IEObjectDescription>newArrayList
		var blip = blipEndEvent.eContainer as Blip
		if (blip != null && blip.process != null && blip.process.flowElements != null) {
	        for (FlowElement element : blip.process.flowElements) {
	        	if	(element instanceof EndEvent) {
	                result.add(EObjectDescription.create(element.id, (element as EndEvent)))
	            }
	        }
	    }
		return MapBasedScope.createScope(IScope.NULLSCOPE, result)
	}
	
//	def IScope getScope_BLIP_ITEM__DTO_PATH(BlipItem blipItem) {
//		var result = <IEObjectDescription>newArrayList
//		var blip = blipItem.eContainer as Blip
//		if (blip != null && blip.workload != null && blip.workload.source != null && (blip.workload.source.dtoRef instanceof LDto)) {
//			recursiveDto(result, "", blip.workload.source.dtoRef)
//	    }
//		return MapBasedScope.createScope(IScope.NULLSCOPE, result)
//	}

//	private def recursiveDto(ArrayList<IEObjectDescription> scopeResult, String parentName, LDto dto) {
//        scopeResult.add(EObjectDescription.create('''«parentName»«dto.name»''', dto))
//        for	(feature : dto.features) {
//        	var referencedDto = null as LDto
//        	if	(feature instanceof LDtoReferenceImpl) {
//        		referencedDto = feature.getType
//        	}
//        	if	(feature instanceof LDtoInheritedReferenceImpl) {
//        		referencedDto = feature.getType
//        	}
//        	if	(referencedDto != null) {
//        		// --- supress recursive references ---
//        		if	(!parentName.contains('''«referencedDto.name».''')) {
//    				recursiveDto(scopeResult, '''«parentName»«dto.name».''', referencedDto) 
//        		}
//        	}
//        }
//	}
}
