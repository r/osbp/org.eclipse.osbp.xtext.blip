/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.xtext.blip;

import javax.inject.Singleton;

import org.eclipse.osbp.dsl.common.xtext.scoping.ScopingInfoProvider;
import org.eclipse.osbp.xtext.blip.formatting.BlipDSLFormatter;
import org.eclipse.osbp.xtext.blip.imports.ShouldImportProvider;
import org.eclipse.osbp.xtext.blip.jvmmodel.BlipModelGenerator;
import org.eclipse.osbp.xtext.blip.scoping.BlibDSLImportSectionNamespaceScopeProvider;
import org.eclipse.osbp.xtext.blip.scoping.BlipDSLScopeProvider;
import org.eclipse.osbp.xtext.blip.valueconverter.BlipQualifiedNameProvider;
import org.eclipse.osbp.xtext.blip.valueconverter.BlipValueConverterService;
import org.eclipse.osbp.xtext.i18n.DSLOutputConfigurationProvider;
import org.eclipse.osbp.xtext.oxtype.imports.IShouldImportProvider;
import org.eclipse.osbp.xtext.oxtype.imports.OXTypeRewritableImportSection;
import org.eclipse.osbp.xtext.oxtype.linker.JvmTypeAwareLinker;
import org.eclipse.osbp.xtext.oxtype.linking.OXTypeLinkingService;
import org.eclipse.osbp.xtext.oxtype.resource.ExtendedJvmModelAssociator;
import org.eclipse.osbp.xtext.oxtype.resource.IExtendedModelAssociator;
import org.eclipse.osbp.xtext.oxtype.resource.SemanticLoadingResource;
import org.eclipse.osbp.xtext.oxtype.scoping.IScopingInfoProvider;
import org.eclipse.osbp.xtext.oxtype.serializer.JvmTypeAwareTransientValueService;
import org.eclipse.xtext.conversion.IValueConverterService;
import org.eclipse.xtext.findReferences.TargetURICollector;
import org.eclipse.xtext.generator.IGenerator;
import org.eclipse.xtext.generator.IOutputConfigurationProvider;
import org.eclipse.xtext.linking.ILinkingService;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.resource.persistence.IResourceStorageFacade;
import org.eclipse.xtext.scoping.IScopeProvider;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService;
import org.eclipse.xtext.xbase.imports.RewritableImportSection;
import org.eclipse.xtext.xbase.jvmmodel.IJvmModelAssociations;
import org.eclipse.xtext.xbase.jvmmodel.IJvmModelAssociator;
import org.eclipse.xtext.xbase.jvmmodel.JvmModelTargetURICollector;
import org.eclipse.xtext.xbase.resource.BatchLinkableResourceStorageFacade;

import com.google.inject.Binder;
import com.google.inject.name.Names;

/**
 * Use this class to register components to be used at runtime / without the
 * Equinox extension registry.
 */
public class BlipDSLRuntimeModule extends
		org.eclipse.osbp.xtext.blip.AbstractBlipDSLRuntimeModule {
	
	public Class<? extends IResourceStorageFacade> bindResourceStorageFacade() {
		return BatchLinkableResourceStorageFacade.class;
	}
	
	public Class<? extends TargetURICollector> bindTargetURICollector() {
		return JvmModelTargetURICollector.class;
	}
	
	@Override
	public Class<? extends IGenerator> bindIGenerator() {
		return BlipModelGenerator.class;
	}

	@Override
	public Class<? extends IScopeProvider> bindIScopeProvider() {
		return BlipDSLScopeProvider.class;
	}

	public Class<? extends org.eclipse.xtext.formatting.IFormatter> bindIFormatter() {
		return BlipDSLFormatter.class;
	}
	
	public Class<? extends org.eclipse.xtext.xbase.scoping.batch.XbaseBatchScopeProvider> bindXbaseBatchScopeProvider() {
		return BlipDSLScopeProvider.class;
	}

	@Override
	public Class<? extends IQualifiedNameProvider> bindIQualifiedNameProvider() {
		return BlipQualifiedNameProvider.class;
	}

	@Override
	public Class<? extends IValueConverterService> bindIValueConverterService() {
		return BlipValueConverterService.class;
	}

	@Override
	public void configure(Binder binder) {
		super.configure(binder);
		binder.bind(IOutputConfigurationProvider.class)
				.to(DSLOutputConfigurationProvider.class).in(Singleton.class);
	}

	public Class<? extends org.eclipse.xtext.resource.XtextResource> bindXtextResource() {
		return SemanticLoadingResource.class;
	}

	public void configureITransientValueService(Binder binder) {
		binder.bind(ITransientValueService.class).to(JvmTypeAwareTransientValueService.class);
	}
	
	public Class<? extends org.eclipse.xtext.linking.ILinker> bindILinker() {
		return JvmTypeAwareLinker.class;
	}

	public Class<? extends ILinkingService> bindILinkingService() {
		return OXTypeLinkingService.class;
	}

	public Class<? extends org.eclipse.xtext.resource.IDerivedStateComputer> bindIDerivedStateComputer() {
		return ExtendedJvmModelAssociator.class;
	}

	public Class<? extends IExtendedModelAssociator> bindIIndexModelAssociator() {
		return ExtendedJvmModelAssociator.class;
	}

	public Class<? extends IJvmModelAssociator> bindIJvmModelAssociator() {
		return ExtendedJvmModelAssociator.class;
	}

	public Class<? extends IJvmModelAssociations> bindIJvmModelAssociations() {
		return ExtendedJvmModelAssociator.class;
	}

	public Class<? extends RewritableImportSection.Factory> bindRewritableImportSection$Factory() {
		return OXTypeRewritableImportSection.Factory.class;
	}

	public Class<? extends IScopingInfoProvider> bindIScopingInfoProvider() {
		return ScopingInfoProvider.class;
	}

	@Override
	public void configureIScopeProviderDelegate(Binder binder) {
		binder.bind(IScopeProvider.class)
				.annotatedWith(
						Names.named("org.eclipse.xtext.scoping.impl.AbstractDeclarativeScopeProvider.delegate"))
				.to(BlibDSLImportSectionNamespaceScopeProvider.class);
	}

	public Class<? extends IShouldImportProvider> bindIShouldImportProvider() {
		return ShouldImportProvider.class;
	}

}
