/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Contributors:
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.blip.jvmmodel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import javax.inject.Inject;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.osbp.bpm.AbstractBPMServiceTask;
import org.eclipse.osbp.bpm.AbstractBlipBPMItem;
import org.eclipse.osbp.bpm.AbstractBlipBPMUserTask;
import org.eclipse.osbp.bpm.BPMCallActivity;
import org.eclipse.osbp.bpm.BPMEndEvent;
import org.eclipse.osbp.bpm.BPMScriptTask;
import org.eclipse.osbp.bpm.BPMSplitGateway;
import org.eclipse.osbp.bpm.BlipBPMOutgoing;
import org.eclipse.osbp.bpm.BlipBPMStartInfo;
import org.eclipse.osbp.bpm.api.IBPMEngine;
import org.eclipse.osbp.bpm.api.IBlipBPMFunctionProvider;
import org.eclipse.osbp.bpm.api.IBlipBPMWorkloadModifiableItem;
import org.eclipse.osbp.bpm.api.ServiceExecutionMode;
import org.eclipse.osbp.bpm.api.ServiceImplementation;
import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.preferences.ProductConfiguration;
import org.eclipse.osbp.ui.api.useraccess.IBlipProcessPermissions;
import org.eclipse.osbp.xtext.basic.generator.BasicDslGeneratorUtils;
import org.eclipse.osbp.xtext.blip.BlipPackage;
import org.eclipse.osbp.xtext.i18n.I18NModelGenerator;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.xbase.compiler.GeneratorConfig;
import org.eclipse.xtext.xbase.compiler.ImportManager;
import org.eclipse.xtext.xbase.compiler.output.TreeAppendable;
import org.eclipse.xtext.xbase.jvmmodel.IJvmDeclaredTypeAcceptor;
import org.eclipse.xtext.xbase.lib.Extension;
import org.osgi.framework.FrameworkUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("all")
public class BlipModelGenerator extends I18NModelGenerator {
  @Inject
  @Extension
  private BasicDslGeneratorUtils _basicDslGeneratorUtils;
  
  public static String pckgName = null;
  
  public void generatePckgName(final BlipPackage pckg, final IJvmDeclaredTypeAcceptor acceptor) {
    BlipModelGenerator.pckgName = pckg.getName();
  }
  
  @Override
  public void doGenerate(final Resource input, final IFileSystemAccess fsa) {
    super.doGenerate(input, fsa);
  }
  
  @Override
  public TreeAppendable createAppendable(final EObject context, final ImportManager importManager, final GeneratorConfig config) {
    TreeAppendable _xblockexpression = null;
    {
      this.setBuilder(context.eResource());
      this._basicDslGeneratorUtils.addImportFor(this, importManager, this._typeReferenceBuilder, Arrays.class, Collections.class, HashMap.class, BlipBPMStartInfo.class, AbstractBlipBPMItem.class, AbstractBlipBPMUserTask.class, AbstractBPMServiceTask.class, ServiceExecutionMode.class, ServiceImplementation.class, BPMEndEvent.class, BPMScriptTask.class, BPMSplitGateway.class, BPMCallActivity.class, BPMSplitGateway.GatewayMode.class, BlipBPMOutgoing.class, IBlipBPMWorkloadModifiableItem.class, IDto.class, IBlipBPMFunctionProvider.class, MApplication.class, ContextInjectionFactory.class, IBlipProcessPermissions.class, IBPMEngine.class, Collection.class, List.class, ArrayList.class, Logger.class, LoggerFactory.class, FrameworkUtil.class, ProductConfiguration.class);
      _xblockexpression = super.createAppendable(context, importManager, config);
    }
    return _xblockexpression;
  }
}
