/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.blip.jvmmodel;

import com.google.common.base.Objects;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.TreeSet;
import java.util.regex.Pattern;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.bpmn2.DocumentRoot;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.bpm.AbstractBPMEvent;
import org.eclipse.osbp.bpm.AbstractBPMServiceTask;
import org.eclipse.osbp.bpm.AbstractBlipBPMFunctionProvider;
import org.eclipse.osbp.bpm.AbstractBlipBPMItem;
import org.eclipse.osbp.bpm.AbstractBlipBPMUserTask;
import org.eclipse.osbp.bpm.BPMCallActivity;
import org.eclipse.osbp.bpm.BPMEndEvent;
import org.eclipse.osbp.bpm.BPMPersistTask;
import org.eclipse.osbp.bpm.BPMScriptTask;
import org.eclipse.osbp.bpm.BPMSplitGateway;
import org.eclipse.osbp.bpm.BPMStartEvent;
import org.eclipse.osbp.bpm.BlipBPMStartInfo;
import org.eclipse.osbp.bpm.api.IBlipBPMFunctionProvider;
import org.eclipse.osbp.bpm.api.IBlipBPMWorkloadModifiableItem;
import org.eclipse.osbp.bpm.api.ServiceExecutionMode;
import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.dsl.common.xtext.extensions.AnnotationExtension;
import org.eclipse.osbp.dsl.semantic.common.types.LMultiplicity;
import org.eclipse.osbp.dsl.semantic.dto.LDto;
import org.eclipse.osbp.dsl.semantic.dto.LDtoAbstractReference;
import org.eclipse.osbp.dsl.semantic.dto.LDtoInheritedReference;
import org.eclipse.osbp.dsl.semantic.dto.LDtoReference;
import org.eclipse.osbp.xtext.blip.Blip;
import org.eclipse.osbp.xtext.blip.BlipCallActivity;
import org.eclipse.osbp.xtext.blip.BlipDto;
import org.eclipse.osbp.xtext.blip.BlipDtoPath;
import org.eclipse.osbp.xtext.blip.BlipEndEvent;
import org.eclipse.osbp.xtext.blip.BlipExclusiveSplitGateway;
import org.eclipse.osbp.xtext.blip.BlipInclusiveSplitGateway;
import org.eclipse.osbp.xtext.blip.BlipItem;
import org.eclipse.osbp.xtext.blip.BlipOutGoing;
import org.eclipse.osbp.xtext.blip.BlipPackage;
import org.eclipse.osbp.xtext.blip.BlipPersistTask;
import org.eclipse.osbp.xtext.blip.BlipScriptTask;
import org.eclipse.osbp.xtext.blip.BlipServiceTask;
import org.eclipse.osbp.xtext.blip.BlipSplitGateway;
import org.eclipse.osbp.xtext.blip.BlipStartEvent;
import org.eclipse.osbp.xtext.blip.BlipUserTask;
import org.eclipse.osbp.xtext.blip.BlipWorkload;
import org.eclipse.osbp.xtext.blip.EndEventHandlingEnum;
import org.eclipse.osbp.xtext.blip.ServiceExecutionModeEnum;
import org.eclipse.osbp.xtext.blip.common.BlipHelper;
import org.eclipse.osbp.xtext.blip.jvmmodel.BlipModelGenerator;
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryBlipGroup;
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryFunction;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.common.types.JvmAnnotationReference;
import org.eclipse.xtext.common.types.JvmConstructor;
import org.eclipse.xtext.common.types.JvmDeclaredType;
import org.eclipse.xtext.common.types.JvmField;
import org.eclipse.xtext.common.types.JvmFormalParameter;
import org.eclipse.xtext.common.types.JvmGenericType;
import org.eclipse.xtext.common.types.JvmMember;
import org.eclipse.xtext.common.types.JvmOperation;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.common.types.JvmVisibility;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.xbase.compiler.output.ITreeAppendable;
import org.eclipse.xtext.xbase.jvmmodel.AbstractModelInferrer;
import org.eclipse.xtext.xbase.jvmmodel.IJvmDeclaredTypeAcceptor;
import org.eclipse.xtext.xbase.jvmmodel.JvmTypesBuilder;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.eclipse.xtext.xbase.lib.StringExtensions;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;

/**
 * <p>
 * Business Logic Implementation Platform
 * This inferrer infers models of extension blip and generates code to be injected as context function to e4 application
 * and handles the communication with the bpm service.
 * </p>
 * 
 * @author Joerg Riegel
 */
@SuppressWarnings("all")
public class BlipDSLJvmModelInferrer extends AbstractModelInferrer {
  @Inject
  @Extension
  private JvmTypesBuilder _jvmTypesBuilder;
  
  @Inject
  @Extension
  private IQualifiedNameProvider _iQualifiedNameProvider;
  
  @Inject
  @Extension
  private BlipModelGenerator _blipModelGenerator;
  
  @Inject
  @Extension
  private AnnotationExtension _annotationExtension;
  
  protected void _infer(final BlipPackage blipPkg, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPreIndexingPhase) {
    this._blipModelGenerator.generatePckgName(blipPkg, acceptor);
    this.createAbstractBpmItemClasses(blipPkg, acceptor);
    final String clsName = StringExtensions.toFirstUpper(this.className(blipPkg));
    final JvmGenericType cls = this._jvmTypesBuilder.toClass(blipPkg, clsName);
    cls.getSuperTypes().add(this._typeReferenceBuilder.typeRef(AbstractBlipBPMFunctionProvider.class));
    cls.setSimpleName(StringExtensions.toFirstUpper(cls.getSimpleName()));
    final Procedure1<JvmGenericType> _function = (JvmGenericType it) -> {
      JvmAnnotationReference annotationRef = this._annotationTypesBuilder.annotationRef(Component.class);
      JvmTypeReference _typeRef = this._typeReferenceBuilder.typeRef(IBlipBPMFunctionProvider.class);
      this._annotationExtension.addAnnAttr(annotationRef, blipPkg, "service", ((JvmTypeReference) _typeRef));
      EList<JvmAnnotationReference> _annotations = it.getAnnotations();
      this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, annotationRef);
      this._jvmTypesBuilder.setFileHeader(it, this._jvmTypesBuilder.getDocumentation(blipPkg));
      this.toFields(it, blipPkg);
      this.toOperations(it, blipPkg);
      it.setPackageName(this._iQualifiedNameProvider.getFullyQualifiedName(blipPkg).toString());
    };
    acceptor.<JvmGenericType>accept(cls, _function);
  }
  
  public CharSequence blipItemAbstractClassName(final BlipItem blipItem) {
    CharSequence _xifexpression = null;
    int _dtoPathTokensCount = this.getDtoPathTokensCount(blipItem);
    boolean _equals = (_dtoPathTokensCount == 2);
    if (_equals) {
      CharSequence _xblockexpression = null;
      {
        EObject _eContainer = blipItem.eContainer();
        final Blip blip = ((Blip) _eContainer);
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("AbstractBPMItem");
        String _firstUpper = StringExtensions.toFirstUpper(blip.getName());
        _builder.append(_firstUpper);
        String _firstUpper_1 = StringExtensions.toFirstUpper(blipItem.getName());
        _builder.append(_firstUpper_1);
        _xblockexpression = _builder;
      }
      _xifexpression = _xblockexpression;
    } else {
      _xifexpression = this.blipItemAbstractClass(blipItem).getSimpleName();
    }
    return _xifexpression;
  }
  
  public Class<? extends AbstractBlipBPMItem> blipItemAbstractClass(final BlipItem blipItem) {
    Class<? extends AbstractBlipBPMItem> _xifexpression = null;
    if ((blipItem instanceof BlipUserTask)) {
      _xifexpression = AbstractBlipBPMUserTask.class;
    } else {
      Class<? extends AbstractBlipBPMItem> _xifexpression_1 = null;
      if ((blipItem instanceof BlipCallActivity)) {
        _xifexpression_1 = BPMCallActivity.class;
      } else {
        Class<? extends AbstractBlipBPMItem> _xifexpression_2 = null;
        if ((blipItem instanceof BlipScriptTask)) {
          _xifexpression_2 = BPMScriptTask.class;
        } else {
          Class<? extends AbstractBlipBPMItem> _xifexpression_3 = null;
          if ((blipItem instanceof BlipPersistTask)) {
            _xifexpression_3 = BPMPersistTask.class;
          } else {
            Class<? extends AbstractBlipBPMItem> _xifexpression_4 = null;
            if ((blipItem instanceof BlipServiceTask)) {
              _xifexpression_4 = AbstractBPMServiceTask.class;
            } else {
              Class<? extends AbstractBPMEvent> _xifexpression_5 = null;
              if ((blipItem instanceof BlipSplitGateway)) {
                _xifexpression_5 = BPMSplitGateway.class;
              } else {
                Class<? extends AbstractBPMEvent> _xifexpression_6 = null;
                if ((blipItem instanceof BlipStartEvent)) {
                  _xifexpression_6 = BPMStartEvent.class;
                } else {
                  Class<BPMEndEvent> _xifexpression_7 = null;
                  if ((blipItem instanceof BlipEndEvent)) {
                    _xifexpression_7 = BPMEndEvent.class;
                  }
                  _xifexpression_6 = _xifexpression_7;
                }
                _xifexpression_5 = _xifexpression_6;
              }
              _xifexpression_4 = _xifexpression_5;
            }
            _xifexpression_3 = _xifexpression_4;
          }
          _xifexpression_2 = _xifexpression_3;
        }
        _xifexpression_1 = _xifexpression_2;
      }
      _xifexpression = _xifexpression_1;
    }
    return _xifexpression;
  }
  
  public int getDtoPathTokensCount(final BlipItem blipItem) {
    BlipDtoPath _dtoPath = blipItem.getDtoPath();
    String _dtoPath_1 = null;
    if (_dtoPath!=null) {
      _dtoPath_1=_dtoPath.getDtoPath();
    }
    String path = _dtoPath_1;
    if ((path == null)) {
      return 0;
    }
    boolean _contains = path.contains(".");
    boolean _not = (!_contains);
    if (_not) {
      return 0;
    }
    return ((List<String>)Conversions.doWrapArray(path.split(Pattern.quote(".")))).size();
  }
  
  public void createAbstractBpmItemClasses(final BlipPackage blipPkg, final IJvmDeclaredTypeAcceptor acceptor) {
    EList<Blip> _blips = blipPkg.getBlips();
    for (final Blip blip : _blips) {
      EList<BlipItem> _items = blip.getItems();
      for (final BlipItem blipItem : _items) {
        int _dtoPathTokensCount = this.getDtoPathTokensCount(blipItem);
        boolean _equals = (_dtoPathTokensCount == 2);
        if (_equals) {
          StringConcatenation _builder = new StringConcatenation();
          String _name = blipPkg.getName();
          _builder.append(_name);
          _builder.append(".");
          String _string = this.blipItemAbstractClassName(blipItem).toString();
          _builder.append(_string);
          final JvmGenericType cls = this._jvmTypesBuilder.toClass(blipPkg, _builder.toString());
          final CharSequence operativeDtoClassName = this.getOperativeDtoFqn(blipItem);
          BlipDtoPath _dtoPath = null;
          if (blipItem!=null) {
            _dtoPath=blipItem.getDtoPath();
          }
          final String[] dtoChildNames = _dtoPath.getDtoPath().split("\\.");
          final String dtoFeatureName = StringUtils.capitalize(IterableExtensions.<String>last(((Iterable<String>)Conversions.doWrapArray(dtoChildNames))));
          cls.setAbstract(true);
          cls.getSuperTypes().add(this._typeReferenceBuilder.typeRef(this.blipItemAbstractClass(blipItem)));
          cls.getSuperTypes().add(this._typeReferenceBuilder.typeRef(IBlipBPMWorkloadModifiableItem.class));
          final Procedure1<JvmGenericType> _function = (JvmGenericType it) -> {
            EList<JvmMember> _members = cls.getMembers();
            final Procedure1<JvmConstructor> _function_1 = (JvmConstructor it_1) -> {
              it_1.setVisibility(JvmVisibility.PROTECTED);
              EList<JvmFormalParameter> _parameters = it_1.getParameters();
              JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(blipPkg, "blipId", this._typeReferenceBuilder.typeRef(String.class));
              this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
              EList<JvmFormalParameter> _parameters_1 = it_1.getParameters();
              JvmFormalParameter _parameter_1 = this._jvmTypesBuilder.toParameter(blipPkg, "bpmId", this._typeReferenceBuilder.typeRef(String.class));
              this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters_1, _parameter_1);
              EList<JvmFormalParameter> _parameters_2 = it_1.getParameters();
              JvmFormalParameter _parameter_2 = this._jvmTypesBuilder.toParameter(blipPkg, "onEntryFunction", this._typeReferenceBuilder.typeRef(String.class));
              this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters_2, _parameter_2);
              EList<JvmFormalParameter> _parameters_3 = it_1.getParameters();
              JvmFormalParameter _parameter_3 = this._jvmTypesBuilder.toParameter(blipPkg, "onExitFunction", this._typeReferenceBuilder.typeRef(String.class));
              this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters_3, _parameter_3);
              final Procedure1<ITreeAppendable> _function_2 = (ITreeAppendable it_2) -> {
                StringConcatenation _builder_1 = new StringConcatenation();
                _builder_1.append("super(blipId, bpmId, onEntryFunction, onExitFunction);");
                it_2.append(_builder_1);
              };
              this._jvmTypesBuilder.setBody(it_1, _function_2);
            };
            JvmConstructor _constructor = this._jvmTypesBuilder.toConstructor(blipPkg, _function_1);
            this._jvmTypesBuilder.<JvmConstructor>operator_add(_members, _constructor);
            EList<JvmMember> _members_1 = cls.getMembers();
            JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(blipPkg, "operativeDto", this._typeReferenceBuilder.typeRef(IDto.class));
            StringConcatenation _builder_1 = new StringConcatenation();
            _builder_1.append("((");
            String _workloadDtoFqn = this.getWorkloadDtoFqn(blip);
            _builder_1.append(_workloadDtoFqn);
            _builder_1.append(")workloadDto).addTo");
            _builder_1.append(dtoFeatureName);
            _builder_1.append("((");
            _builder_1.append(operativeDtoClassName);
            _builder_1.append(") operativeDto);");
            JvmOperation _createMethod = this.createMethod(blipItem, cls, "addOperativeDto", _parameter, dtoFeatureName, _builder_1.toString());
            this._jvmTypesBuilder.<JvmOperation>operator_add(_members_1, _createMethod);
            EList<JvmMember> _members_2 = cls.getMembers();
            JvmFormalParameter _parameter_1 = this._jvmTypesBuilder.toParameter(blipPkg, "operativeDtos", this._typeReferenceBuilder.typeRef(List.class, this._typeReferenceBuilder.typeRef(IDto.class)));
            StringConcatenation _builder_2 = new StringConcatenation();
            _builder_2.append("for\t(IDto operativeDto : operativeDtos) {");
            _builder_2.newLine();
            _builder_2.append("\t");
            _builder_2.append("((");
            String _workloadDtoFqn_1 = this.getWorkloadDtoFqn(blip);
            _builder_2.append(_workloadDtoFqn_1, "\t");
            _builder_2.append(")workloadDto).addTo");
            _builder_2.append(dtoFeatureName, "\t");
            _builder_2.append("((");
            _builder_2.append(operativeDtoClassName, "\t");
            _builder_2.append(") operativeDto);");
            _builder_2.newLineIfNotEmpty();
            _builder_2.append("}");
            _builder_2.newLine();
            JvmOperation _createMethod_1 = this.createMethod(blipItem, cls, "addOperativeDtos", _parameter_1, dtoFeatureName, _builder_2.toString());
            this._jvmTypesBuilder.<JvmOperation>operator_add(_members_2, _createMethod_1);
            EList<JvmMember> _members_3 = cls.getMembers();
            StringConcatenation _builder_3 = new StringConcatenation();
            _builder_3.append("((");
            String _workloadDtoFqn_2 = this.getWorkloadDtoFqn(blip);
            _builder_3.append(_workloadDtoFqn_2);
            _builder_3.append(")workloadDto).set");
            _builder_3.append(dtoFeatureName);
            _builder_3.append("(null);");
            JvmOperation _createMethod_2 = this.createMethod(blipItem, cls, "removeAllOperativeDtos", 
              null, dtoFeatureName, _builder_3.toString());
            this._jvmTypesBuilder.<JvmOperation>operator_add(_members_3, _createMethod_2);
            EList<JvmMember> _members_4 = cls.getMembers();
            JvmFormalParameter _parameter_2 = this._jvmTypesBuilder.toParameter(blipPkg, "operativeDto", this._typeReferenceBuilder.typeRef(IDto.class));
            StringConcatenation _builder_4 = new StringConcatenation();
            _builder_4.append("((");
            String _workloadDtoFqn_3 = this.getWorkloadDtoFqn(blip);
            _builder_4.append(_workloadDtoFqn_3);
            _builder_4.append(")workloadDto).removeFrom");
            _builder_4.append(dtoFeatureName);
            _builder_4.append("((");
            _builder_4.append(operativeDtoClassName);
            _builder_4.append(") operativeDto);");
            JvmOperation _createMethod_3 = this.createMethod(blipItem, cls, "removeOperativeDto", _parameter_2, dtoFeatureName, _builder_4.toString());
            this._jvmTypesBuilder.<JvmOperation>operator_add(_members_4, _createMethod_3);
            EList<JvmMember> _members_5 = cls.getMembers();
            JvmFormalParameter _parameter_3 = this._jvmTypesBuilder.toParameter(blipPkg, "operativeDtos", this._typeReferenceBuilder.typeRef(List.class, this._typeReferenceBuilder.typeRef(IDto.class)));
            StringConcatenation _builder_5 = new StringConcatenation();
            _builder_5.append("for\t(IDto operativeDto : operativeDtos) {");
            _builder_5.newLine();
            _builder_5.append("\t");
            _builder_5.append("((");
            String _workloadDtoFqn_4 = this.getWorkloadDtoFqn(blip);
            _builder_5.append(_workloadDtoFqn_4, "\t");
            _builder_5.append(")workloadDto).removeFrom");
            _builder_5.append(dtoFeatureName, "\t");
            _builder_5.append("((");
            _builder_5.append(operativeDtoClassName, "\t");
            _builder_5.append(") operativeDto);");
            _builder_5.newLineIfNotEmpty();
            _builder_5.append("}");
            _builder_5.newLine();
            JvmOperation _createMethod_4 = this.createMethod(blipItem, cls, "removeOperativeDtos", _parameter_3, dtoFeatureName, _builder_5.toString());
            this._jvmTypesBuilder.<JvmOperation>operator_add(_members_5, _createMethod_4);
          };
          acceptor.<JvmGenericType>accept(cls, _function);
        }
      }
    }
  }
  
  public JvmOperation createMethod(final BlipItem blipItem, final JvmGenericType cls, final String methodName, final JvmFormalParameter parameter2, final String getterSetterPostfix, final String listModifier) {
    EObject _eContainer = blipItem.eContainer();
    final Blip blip = ((Blip) _eContainer);
    EObject _eContainer_1 = blip.eContainer();
    final BlipPackage blipPckg = ((BlipPackage) _eContainer_1);
    final Procedure1<JvmOperation> _function = (JvmOperation it) -> {
      EList<JvmAnnotationReference> _annotations = it.getAnnotations();
      JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Override.class);
      this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
      it.setVisibility(JvmVisibility.PUBLIC);
      EList<JvmFormalParameter> _parameters = it.getParameters();
      JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(blipPckg, "workloadDto", this._typeReferenceBuilder.typeRef(IDto.class));
      this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
      if ((parameter2 != null)) {
        EList<JvmFormalParameter> _parameters_1 = it.getParameters();
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters_1, parameter2);
      }
      final Procedure1<ITreeAppendable> _function_1 = (ITreeAppendable it_1) -> {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append(listModifier);
        _builder.newLineIfNotEmpty();
        it_1.append(_builder);
      };
      this._jvmTypesBuilder.setBody(it, _function_1);
    };
    return this._jvmTypesBuilder.toMethod(blipPckg, methodName, this._typeReferenceBuilder.typeRef(Void.TYPE), _function);
  }
  
  public String className(final BlipPackage pkg) {
    String _firstUpper = StringExtensions.toFirstUpper(pkg.getName().toString());
    String clsName = (_firstUpper + "Controller");
    String[] parts = clsName.split(Pattern.quote("."));
    int len = parts.length;
    if ((len > 1)) {
      return StringExtensions.toFirstUpper(parts[(len - 1)]);
    }
    return clsName;
  }
  
  public void toFields(final JvmGenericType type, final BlipPackage pkg) {
    JvmField field = null;
    final Procedure1<JvmField> _function = (JvmField it) -> {
      final Procedure1<ITreeAppendable> _function_1 = (ITreeAppendable it_1) -> {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("org.slf4j.LoggerFactory.getLogger(\"blip\")");
        it_1.append(_builder);
      };
      this._jvmTypesBuilder.setInitializer(it, _function_1);
    };
    field = this._jvmTypesBuilder.toField(pkg, "log", this._typeReferenceBuilder.typeRef(Logger.class), _function);
    field.setStatic(true);
    field.setVisibility(JvmVisibility.PRIVATE);
    EList<JvmMember> _members = type.getMembers();
    this._jvmTypesBuilder.<JvmField>operator_add(_members, field);
  }
  
  public String createBPMNCalls(final BlipPackage pkg) {
    try {
      final int CODE = IStatus.ERROR;
      final String PLUGINID = this.getClass().getCanonicalName();
      final String PREFIX_MESSAGE = "BPMN2 file";
      final String POSTFIX_MESSAGE = "could not be found in this bundle!";
      HashSet<IStatus> errors = new HashSet<IStatus>();
      TreeSet<Object> resources = new TreeSet<Object>();
      String text = "";
      final String[] tokens = pkg.eResource().getURI().toString().split("/");
      String base = ((String) null);
      for (int i = 0; (i < 3); i++) {
        if ((base == null)) {
          base = tokens[i];
        } else {
          StringConcatenation _builder = new StringConcatenation();
          _builder.append(base);
          _builder.append("/");
          String _get = tokens[i];
          _builder.append(_get);
          base = _builder.toString();
        }
      }
      EList<Blip> _blips = pkg.getBlips();
      for (final Blip blip : _blips) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append(text);
        _builder.newLineIfNotEmpty();
        _builder.append("startInfos.put(\"");
        String _name = blip.getName();
        _builder.append(_name);
        _builder.append("\",new ");
        String _simpleName = BlipBPMStartInfo.class.getSimpleName();
        _builder.append(_simpleName);
        _builder.append("(\"");
        String _name_1 = blip.getName();
        _builder.append(_name_1);
        _builder.append("\",\"");
        String _name_2 = blip.getName();
        _builder.append(_name_2);
        _builder.append("\"");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append(", \"");
        String _resourceName = BlipDSLJvmModelInferrer.getResourceName(blip);
        _builder.append(_resourceName, "\t");
        _builder.append("\"");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append(", ");
        {
          boolean _isHasImage = blip.isHasImage();
          if (_isHasImage) {
            _builder.append("\"");
            String _image = blip.getImage();
            _builder.append(_image, "\t");
            _builder.append("\"");
          } else {
            _builder.append("null");
          }
        }
        _builder.append(",");
        {
          boolean _isHasIcon = blip.isHasIcon();
          if (_isHasIcon) {
            _builder.append("\"");
            String _literal = blip.getIcon().getLiteral();
            _builder.append(_literal, "\t");
            _builder.append("\"");
          } else {
            _builder.append("null");
          }
        }
        _builder.append(",");
        {
          boolean _isHasLogging = blip.isHasLogging();
          if (_isHasLogging) {
            boolean _booleanValue = Boolean.valueOf(blip.isHasLogging()).booleanValue();
            _builder.append(_booleanValue, "\t");
          } else {
            _builder.append("false");
          }
        }
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append(", ");
        String _functionGroupClassName = this.getFunctionGroupClassName(blip);
        _builder.append(_functionGroupClassName, "\t");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append(", ");
        String _workloadDtoClassName = this.getWorkloadDtoClassName(blip);
        _builder.append(_workloadDtoClassName, "\t");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        String _createBlipItems = this.createBlipItems(blip);
        _builder.append(_createBlipItems, "\t");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("));");
        _builder.newLine();
        text = _builder.toString();
      }
      boolean _isEmpty = errors.isEmpty();
      boolean _not = (!_isEmpty);
      if (_not) {
        final HashSet<IStatus> _converted_errors = (HashSet<IStatus>)errors;
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append(PREFIX_MESSAGE);
        _builder_1.append(" ");
        String _join = IterableExtensions.join(resources, ",\n");
        _builder_1.append(_join);
        _builder_1.append(" ");
        _builder_1.append(POSTFIX_MESSAGE);
        MultiStatus _multiStatus = new MultiStatus(PLUGINID, CODE, ((IStatus[])Conversions.unwrapArray(_converted_errors, IStatus.class)), _builder_1.toString(), null);
        throw new CoreException(_multiStatus);
      }
      return text;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public QualifiedName getWorkloadDtoClass(final Blip blip) {
    BlipWorkload _workload = blip.getWorkload();
    boolean _tripleEquals = (_workload == null);
    if (_tripleEquals) {
      return null;
    } else {
      return this._iQualifiedNameProvider.getFullyQualifiedName(blip.getWorkload().getSource().getDtoRef());
    }
  }
  
  public String getFunctionGroupClass(final Blip blip) {
    FunctionLibraryBlipGroup _functionGroup = blip.getFunctionGroup();
    boolean _tripleEquals = (_functionGroup == null);
    if (_tripleEquals) {
      return "null";
    }
    StringConcatenation _builder = new StringConcatenation();
    QualifiedName _fullyQualifiedName = this._iQualifiedNameProvider.getFullyQualifiedName(blip.getFunctionGroup());
    _builder.append(_fullyQualifiedName);
    return _builder.toString();
  }
  
  public String getFunctionGroupClassName(final Blip blip) {
    FunctionLibraryBlipGroup _functionGroup = blip.getFunctionGroup();
    boolean _tripleEquals = (_functionGroup == null);
    if (_tripleEquals) {
      return "null";
    }
    StringConcatenation _builder = new StringConcatenation();
    QualifiedName _fullyQualifiedName = this._iQualifiedNameProvider.getFullyQualifiedName(blip.getFunctionGroup());
    _builder.append(_fullyQualifiedName);
    _builder.append(".class");
    return _builder.toString();
  }
  
  public String getWorkloadDtoClassName(final Blip blip) {
    boolean _equals = this.getWorkloadDtoFqn(blip).equals("null");
    if (_equals) {
      return "null";
    }
    StringConcatenation _builder = new StringConcatenation();
    String _workloadDtoFqn = this.getWorkloadDtoFqn(blip);
    _builder.append(_workloadDtoFqn);
    _builder.append(".class");
    return _builder.toString();
  }
  
  public String getWorkloadDtoFqn(final Blip blip) {
    QualifiedName _workloadDtoClass = this.getWorkloadDtoClass(blip);
    boolean _tripleEquals = (_workloadDtoClass == null);
    if (_tripleEquals) {
      return "null";
    } else {
      StringConcatenation _builder = new StringConcatenation();
      QualifiedName _workloadDtoClass_1 = this.getWorkloadDtoClass(blip);
      _builder.append(_workloadDtoClass_1);
      return _builder.toString();
    }
  }
  
  public String getOperativeDtoClassName(final BlipItem blipItem) {
    boolean _equals = this.getOperativeDtoFqn(blipItem).equals("null");
    if (_equals) {
      return "null";
    }
    StringConcatenation _builder = new StringConcatenation();
    CharSequence _operativeDtoFqn = this.getOperativeDtoFqn(blipItem);
    _builder.append(_operativeDtoFqn);
    _builder.append(".class");
    return _builder.toString();
  }
  
  public CharSequence getOperativeDtoFqn(final BlipItem blipItem) {
    CharSequence _xblockexpression = null;
    {
      EObject _eContainer = blipItem.eContainer();
      Blip blip = ((Blip) _eContainer);
      CharSequence _xifexpression = null;
      QualifiedName _workloadDtoClass = this.getWorkloadDtoClass(blip);
      boolean _tripleEquals = (_workloadDtoClass == null);
      if (_tripleEquals) {
        _xifexpression = "null";
      } else {
        StringConcatenation _builder = new StringConcatenation();
        QualifiedName _fullyQualifiedName = this._iQualifiedNameProvider.getFullyQualifiedName(blipItem.getOperativeLDto());
        _builder.append(_fullyQualifiedName);
        _xifexpression = _builder;
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  public String asString(final ServiceExecutionModeEnum mode) {
    String _switchResult = null;
    if (mode != null) {
      switch (mode) {
        case ONEWAY:
          _switchResult = ServiceExecutionMode.ONEWAY.toString();
          break;
        case SYNC:
          _switchResult = ServiceExecutionMode.SYNC.toString();
          break;
        default:
          break;
      }
    }
    return _switchResult;
  }
  
  public String createBlipItems(final Blip blip) {
    String text = "";
    EList<BlipItem> _items = blip.getItems();
    for (final BlipItem blipItem : _items) {
      {
        final CharSequence itemClass = this.blipItemAbstractClassName(blipItem);
        boolean _matched = false;
        if (blipItem instanceof BlipUserTask) {
          _matched=true;
          String onEntry = "null";
          FunctionLibraryFunction _onEntry = ((BlipUserTask)blipItem).getOnEntry();
          boolean _tripleNotEquals = (_onEntry != null);
          if (_tripleNotEquals) {
            StringConcatenation _builder = new StringConcatenation();
            _builder.append("\"");
            String _name = ((BlipUserTask)blipItem).getOnEntry().getName();
            _builder.append(_name);
            _builder.append("\"");
            onEntry = _builder.toString();
          }
          String onExit = "null";
          FunctionLibraryFunction _onExit = ((BlipUserTask)blipItem).getOnExit();
          boolean _tripleNotEquals_1 = (_onExit != null);
          if (_tripleNotEquals_1) {
            StringConcatenation _builder_1 = new StringConcatenation();
            _builder_1.append("\"");
            String _name_1 = ((BlipUserTask)blipItem).getOnExit().getName();
            _builder_1.append(_name_1);
            _builder_1.append("\"");
            onExit = _builder_1.toString();
          }
          StringConcatenation _builder_2 = new StringConcatenation();
          _builder_2.append(text);
          _builder_2.append(", new ");
          _builder_2.append(itemClass);
          _builder_2.append("(\"");
          String _name_2 = ((BlipUserTask)blipItem).getName();
          _builder_2.append(_name_2);
          _builder_2.append("\", \"");
          String _id = ((BlipUserTask)blipItem).getTask().getId();
          _builder_2.append(_id);
          _builder_2.append("\", ");
          _builder_2.append(onEntry);
          _builder_2.append(", ");
          _builder_2.append(onExit);
          _builder_2.append(") {");
          _builder_2.newLineIfNotEmpty();
          _builder_2.append("\t");
          _builder_2.append("@Override");
          _builder_2.newLine();
          _builder_2.append("\t");
          _builder_2.append("public Class<?> getOperativeDtoClass() {");
          _builder_2.newLine();
          _builder_2.append("\t\t");
          _builder_2.append("return ");
          String _operativeDtoClassName = this.getOperativeDtoClassName(blipItem);
          _builder_2.append(_operativeDtoClassName, "\t\t");
          _builder_2.append(";");
          _builder_2.newLineIfNotEmpty();
          _builder_2.append("\t");
          _builder_2.append("}");
          _builder_2.newLine();
          _builder_2.append("\t");
          _builder_2.append("@Override");
          _builder_2.newLine();
          _builder_2.append("\t");
          _builder_2.append("public List<IDto> getOperativeDtos(IDto item0) {");
          _builder_2.newLine();
          _builder_2.append("\t\t");
          CharSequence _operativeDtosBody = this.getOperativeDtosBody(blipItem);
          _builder_2.append(_operativeDtosBody, "\t\t");
          _builder_2.newLineIfNotEmpty();
          _builder_2.append("\t");
          _builder_2.append("}");
          _builder_2.newLine();
          _builder_2.append("\t");
          String _operativeCastedDtosMethod = this.getOperativeCastedDtosMethod(blipItem);
          _builder_2.append(_operativeCastedDtosMethod, "\t");
          _builder_2.newLineIfNotEmpty();
          _builder_2.append("}");
          _builder_2.newLine();
          text = _builder_2.toString();
        }
        if (!_matched) {
          if (blipItem instanceof BlipCallActivity) {
            _matched=true;
            String onEntry = "null";
            FunctionLibraryFunction _onEntry = ((BlipCallActivity)blipItem).getOnEntry();
            boolean _tripleNotEquals = (_onEntry != null);
            if (_tripleNotEquals) {
              StringConcatenation _builder = new StringConcatenation();
              _builder.append("\"");
              String _name = ((BlipCallActivity)blipItem).getOnEntry().getName();
              _builder.append(_name);
              _builder.append("\"");
              onEntry = _builder.toString();
            }
            String onExit = "null";
            FunctionLibraryFunction _onExit = ((BlipCallActivity)blipItem).getOnExit();
            boolean _tripleNotEquals_1 = (_onExit != null);
            if (_tripleNotEquals_1) {
              StringConcatenation _builder_1 = new StringConcatenation();
              _builder_1.append("\"");
              String _name_1 = ((BlipCallActivity)blipItem).getOnExit().getName();
              _builder_1.append(_name_1);
              _builder_1.append("\"");
              onExit = _builder_1.toString();
            }
            StringConcatenation _builder_2 = new StringConcatenation();
            _builder_2.append(text);
            _builder_2.append(", new ");
            _builder_2.append(itemClass);
            _builder_2.append("(\"");
            String _name_2 = ((BlipCallActivity)blipItem).getName();
            _builder_2.append(_name_2);
            _builder_2.append("\", \"");
            String _id = ((BlipCallActivity)blipItem).getCallActivity().getId();
            _builder_2.append(_id);
            _builder_2.append("\", ");
            _builder_2.append(onEntry);
            _builder_2.append(", ");
            _builder_2.append(onExit);
            _builder_2.append(")");
            _builder_2.newLineIfNotEmpty();
            text = _builder_2.toString();
          }
        }
        if (!_matched) {
          if (blipItem instanceof BlipScriptTask) {
            _matched=true;
            StringConcatenation _builder = new StringConcatenation();
            _builder.append(text);
            _builder.append(", new ");
            _builder.append(itemClass);
            _builder.append("(\"");
            String _name = ((BlipScriptTask)blipItem).getName();
            _builder.append(_name);
            _builder.append("\", \"");
            String _id = ((BlipScriptTask)blipItem).getTask().getId();
            _builder.append(_id);
            _builder.append("\", \"");
            String _name_1 = ((BlipScriptTask)blipItem).getFunction().getName();
            _builder.append(_name_1);
            _builder.append("\")");
            _builder.newLineIfNotEmpty();
            text = _builder.toString();
          }
        }
        if (!_matched) {
          if (blipItem instanceof BlipPersistTask) {
            _matched=true;
            StringConcatenation _builder = new StringConcatenation();
            _builder.append(text);
            _builder.append(", new ");
            _builder.append(itemClass);
            _builder.append("(\"");
            String _name = ((BlipPersistTask)blipItem).getName();
            _builder.append(_name);
            _builder.append("\", \"");
            String _id = ((BlipPersistTask)blipItem).getTask().getId();
            _builder.append(_id);
            _builder.append("\", \"");
            String _name_1 = ((BlipPersistTask)blipItem).getFunction().getName();
            _builder.append(_name_1);
            _builder.append("\")");
            _builder.newLineIfNotEmpty();
            text = _builder.toString();
          }
        }
        if (!_matched) {
          if (blipItem instanceof BlipServiceTask) {
            _matched=true;
            String onEntry = "null";
            FunctionLibraryFunction _onEntry = ((BlipServiceTask)blipItem).getOnEntry();
            boolean _tripleNotEquals = (_onEntry != null);
            if (_tripleNotEquals) {
              StringConcatenation _builder = new StringConcatenation();
              _builder.append("\"");
              String _name = ((BlipServiceTask)blipItem).getOnEntry().getName();
              _builder.append(_name);
              _builder.append("\"");
              onEntry = _builder.toString();
            }
            String onExit = "null";
            FunctionLibraryFunction _onExit = ((BlipServiceTask)blipItem).getOnExit();
            boolean _tripleNotEquals_1 = (_onExit != null);
            if (_tripleNotEquals_1) {
              StringConcatenation _builder_1 = new StringConcatenation();
              _builder_1.append("\"");
              String _name_1 = ((BlipServiceTask)blipItem).getOnExit().getName();
              _builder_1.append(_name_1);
              _builder_1.append("\"");
              onExit = _builder_1.toString();
            }
            String executionMode = "";
            int _timeoutInSecs = ((BlipServiceTask)blipItem).getTimeoutInSecs();
            boolean _greaterThan = (_timeoutInSecs > 0);
            if (_greaterThan) {
              StringConcatenation _builder_2 = new StringConcatenation();
              _builder_2.append("ServiceExecutionMode.ASYNCHRON, ");
              int _timeoutInSecs_1 = ((BlipServiceTask)blipItem).getTimeoutInSecs();
              _builder_2.append(_timeoutInSecs_1);
              executionMode = _builder_2.toString();
            } else {
              StringConcatenation _builder_3 = new StringConcatenation();
              _builder_3.append("ServiceExecutionMode.");
              String _asString = this.asString(((BlipServiceTask)blipItem).getExecutionMode());
              _builder_3.append(_asString);
              _builder_3.append(", 0");
              executionMode = _builder_3.toString();
            }
            String implementation = "";
            FunctionLibraryFunction _function = ((BlipServiceTask)blipItem).getFunction();
            boolean _tripleNotEquals_2 = (_function != null);
            if (_tripleNotEquals_2) {
              StringConcatenation _builder_4 = new StringConcatenation();
              _builder_4.append("ServiceImplementation.FUNCTION_LIBRARY, \"");
              String _functionGroupClass = this.getFunctionGroupClass(blip);
              _builder_4.append(_functionGroupClass);
              _builder_4.append("\", \"");
              String _name_2 = ((BlipServiceTask)blipItem).getFunction().getName();
              _builder_4.append(_name_2);
              _builder_4.append("\"");
              implementation = _builder_4.toString();
            } else {
              StringConcatenation _builder_5 = new StringConcatenation();
              _builder_5.append("ServiceImplementation.WEB_SERVICE, \"");
              String _webServiceInterface = ((BlipServiceTask)blipItem).getWebServiceInterface();
              _builder_5.append(_webServiceInterface);
              _builder_5.append("\", \"");
              String _webServiceOperation = ((BlipServiceTask)blipItem).getWebServiceOperation();
              _builder_5.append(_webServiceOperation);
              _builder_5.append("\"");
              implementation = _builder_5.toString();
            }
            StringConcatenation _builder_6 = new StringConcatenation();
            _builder_6.append(text);
            _builder_6.append(", new ");
            _builder_6.append(itemClass);
            _builder_6.append("(\"");
            String _name_3 = ((BlipServiceTask)blipItem).getName();
            _builder_6.append(_name_3);
            _builder_6.append("\", \"");
            String _id = ((BlipServiceTask)blipItem).getTask().getId();
            _builder_6.append(_id);
            _builder_6.append("\", ");
            _builder_6.append(onEntry);
            _builder_6.append(", ");
            _builder_6.append(onExit);
            _builder_6.append(", ");
            _builder_6.append(executionMode);
            _builder_6.append(", ");
            _builder_6.append(implementation);
            _builder_6.append(") {");
            _builder_6.newLineIfNotEmpty();
            _builder_6.append("\t");
            _builder_6.append("@Override");
            _builder_6.newLine();
            _builder_6.append("\t");
            _builder_6.append("public Class<?> getOperativeDtoClass() {");
            _builder_6.newLine();
            _builder_6.append("\t\t");
            _builder_6.append("return ");
            String _workloadDtoClassName = this.getWorkloadDtoClassName(blip);
            _builder_6.append(_workloadDtoClassName, "\t\t");
            _builder_6.append(";");
            _builder_6.newLineIfNotEmpty();
            _builder_6.append("\t");
            _builder_6.append("}");
            _builder_6.newLine();
            _builder_6.append("\t");
            _builder_6.append("@Override");
            _builder_6.newLine();
            _builder_6.append("\t");
            _builder_6.append("public List<IDto> getOperativeDtos(IDto item0) {");
            _builder_6.newLine();
            _builder_6.append("\t\t");
            _builder_6.append("return Collections.emptyList();");
            _builder_6.newLine();
            _builder_6.append("\t");
            _builder_6.append("}");
            _builder_6.newLine();
            _builder_6.append("}");
            _builder_6.newLine();
            text = _builder_6.toString();
          }
        }
        if (!_matched) {
          if (blipItem instanceof BlipSplitGateway) {
            _matched=true;
            String mode = "";
            if ((blipItem instanceof BlipExclusiveSplitGateway)) {
              mode = "GatewayMode.EXCLUSIVE";
            } else {
              if ((blipItem instanceof BlipInclusiveSplitGateway)) {
                mode = "GatewayMode.INCLUSIVE";
              }
            }
            String outgoings = "";
            int priority = 0;
            EList<BlipOutGoing> _outgoings = ((BlipSplitGateway)blipItem).getOutgoings();
            for (final BlipOutGoing outgoing : _outgoings) {
              {
                priority = (priority + 1);
                boolean _isIsDefault = outgoing.isIsDefault();
                if (_isIsDefault) {
                  StringConcatenation _builder = new StringConcatenation();
                  _builder.append(outgoings);
                  _builder.append(",");
                  _builder.newLineIfNotEmpty();
                  _builder.append("\t\t");
                  _builder.append("new BlipBPMOutgoing(\"");
                  String _name = outgoing.getName();
                  _builder.append(_name, "\t\t");
                  _builder.append("\", \"");
                  String _id = outgoing.getSequenceFlow().getId();
                  _builder.append(_id, "\t\t");
                  _builder.append("\", ");
                  _builder.append(priority, "\t\t");
                  _builder.append(")");
                  outgoings = _builder.toString();
                } else {
                  StringConcatenation _builder_1 = new StringConcatenation();
                  _builder_1.append(outgoings);
                  _builder_1.append(",");
                  _builder_1.newLineIfNotEmpty();
                  _builder_1.append("\t\t");
                  _builder_1.append("new BlipBPMOutgoing(\"");
                  String _name_1 = outgoing.getName();
                  _builder_1.append(_name_1, "\t\t");
                  _builder_1.append("\", \"");
                  String _id_1 = outgoing.getSequenceFlow().getId();
                  _builder_1.append(_id_1, "\t\t");
                  _builder_1.append("\", ");
                  _builder_1.append(priority, "\t\t");
                  _builder_1.append(", \"");
                  String _name_2 = outgoing.getConstraint().getName();
                  _builder_1.append(_name_2, "\t\t");
                  _builder_1.append("\")");
                  outgoings = _builder_1.toString();
                }
              }
            }
            StringConcatenation _builder = new StringConcatenation();
            _builder.append(text);
            _builder.append(", new ");
            _builder.append(itemClass);
            _builder.append("(\"");
            String _name = ((BlipSplitGateway)blipItem).getName();
            _builder.append(_name);
            _builder.append("\", \"");
            String _id = ((BlipSplitGateway)blipItem).getGateway().getId();
            _builder.append(_id);
            _builder.append("\", ");
            _builder.append(mode);
            _builder.append(outgoings);
            _builder.append(")");
            _builder.newLineIfNotEmpty();
            text = _builder.toString();
          }
        }
        if (!_matched) {
          if (blipItem instanceof BlipEndEvent) {
            _matched=true;
            StringConcatenation _builder = new StringConcatenation();
            _builder.append(text);
            _builder.append(", new ");
            _builder.append(itemClass);
            _builder.append("(\"");
            String _name = ((BlipEndEvent)blipItem).getName();
            _builder.append(_name);
            _builder.append("\", \"");
            String _id = ((BlipEndEvent)blipItem).getEvent().getId();
            _builder.append(_id);
            _builder.append("\", ");
            {
              EndEventHandlingEnum _endHandling = ((BlipEndEvent)blipItem).getEndHandling();
              boolean _equals = Objects.equal(_endHandling, EndEventHandlingEnum.TERMINATES_PROCESS);
              if (_equals) {
                _builder.append("true");
              } else {
                _builder.append("false");
              }
            }
            _builder.append(")");
            _builder.newLineIfNotEmpty();
            text = _builder.toString();
          }
        }
      }
    }
    return text;
  }
  
  public CharSequence getOperativeDtosBody(final BlipItem blipItem) {
    CharSequence _xblockexpression = null;
    {
      EObject _eContainer = blipItem.eContainer();
      Blip blip = ((Blip) _eContainer);
      CharSequence _xifexpression = null;
      QualifiedName _workloadDtoClass = this.getWorkloadDtoClass(blip);
      boolean _tripleEquals = (_workloadDtoClass == null);
      if (_tripleEquals) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("return null;");
        _xifexpression = _builder;
      } else {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("List<IDto> result = new java.util.ArrayList<>();");
        _builder_1.newLine();
        _builder_1.append("if\t(item0 instanceof ");
        QualifiedName _workloadDtoClass_1 = this.getWorkloadDtoClass(blip);
        _builder_1.append(_workloadDtoClass_1);
        _builder_1.append(") {");
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("\t");
        _builder_1.append("for\t(");
        QualifiedName _fullyQualifiedName = this._iQualifiedNameProvider.getFullyQualifiedName(blipItem.getOperativeLDto());
        _builder_1.append(_fullyQualifiedName, "\t");
        _builder_1.append(" dto : getOperative");
        String _name = blipItem.getOperativeLDto().getName();
        _builder_1.append(_name, "\t");
        _builder_1.append("s((");
        QualifiedName _workloadDtoClass_2 = this.getWorkloadDtoClass(blip);
        _builder_1.append(_workloadDtoClass_2, "\t");
        _builder_1.append(")item0)) {");
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("\t\t");
        _builder_1.append("result.add(dto);");
        _builder_1.newLine();
        _builder_1.append("\t");
        _builder_1.append("}");
        _builder_1.newLine();
        _builder_1.append("}");
        _builder_1.newLine();
        _builder_1.append("return result;");
        _builder_1.newLine();
        _xifexpression = _builder_1;
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  public String getOperativeCastedDtosMethod(final BlipItem blipItem) {
    EObject _eContainer = blipItem.eContainer();
    Blip blip = ((Blip) _eContainer);
    QualifiedName _workloadDtoClass = this.getWorkloadDtoClass(blip);
    boolean _tripleEquals = (_workloadDtoClass == null);
    if (_tripleEquals) {
      return "";
    } else {
      LDto workloadDto = blip.getWorkload().getSource().getDtoRef();
      LDto operativeLDto = blipItem.getOperativeLDto();
      String body = "";
      boolean _equals = workloadDto.equals(operativeLDto);
      if (_equals) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("List<");
        QualifiedName _fullyQualifiedName = this._iQualifiedNameProvider.getFullyQualifiedName(blipItem.getOperativeLDto());
        _builder.append(_fullyQualifiedName);
        _builder.append("> result = new java.util.ArrayList<>();");
        _builder.newLineIfNotEmpty();
        _builder.append("if\t(item0 != null) {");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("result.add(item0);");
        _builder.newLine();
        _builder.append("}");
        _builder.newLine();
        _builder.append("return result;");
        _builder.newLine();
        body = _builder.toString();
      } else {
        StringConcatenation _builder_1 = new StringConcatenation();
        String _operativeDtoMethod = this.getOperativeDtoMethod("item0", blipItem);
        _builder_1.append(_operativeDtoMethod);
        body = _builder_1.toString();
      }
      StringConcatenation _builder_2 = new StringConcatenation();
      _builder_2.append("public List<");
      QualifiedName _fullyQualifiedName_1 = this._iQualifiedNameProvider.getFullyQualifiedName(blipItem.getOperativeLDto());
      _builder_2.append(_fullyQualifiedName_1);
      _builder_2.append("> getOperative");
      String _name = blipItem.getOperativeLDto().getName();
      _builder_2.append(_name);
      _builder_2.append("s(");
      QualifiedName _workloadDtoClass_1 = this.getWorkloadDtoClass(blip);
      _builder_2.append(_workloadDtoClass_1);
      _builder_2.append(" item0) {");
      _builder_2.newLineIfNotEmpty();
      _builder_2.append("\t");
      _builder_2.append(body, "\t");
      _builder_2.newLineIfNotEmpty();
      _builder_2.append("}");
      _builder_2.newLine();
      String text = _builder_2.toString();
      return text;
    }
  }
  
  private String getOperativeDtoMethod(final String rootDto, final BlipItem blipItem) {
    boolean _isOperativeDtoDefined = BlipHelper.isOperativeDtoDefined(blipItem);
    if (_isOperativeDtoDefined) {
      LDto operativeDto = ((LDto) null);
      EObject _eContainer = null;
      if (blipItem!=null) {
        _eContainer=blipItem.eContainer();
      }
      BlipWorkload _workload = null;
      if (((Blip) _eContainer)!=null) {
        _workload=((Blip) _eContainer).getWorkload();
      }
      BlipDto _source = null;
      if (_workload!=null) {
        _source=_workload.getSource();
      }
      LDto _dtoRef = null;
      if (_source!=null) {
        _dtoRef=_source.getDtoRef();
      }
      operativeDto = _dtoRef;
      boolean _and = false;
      if (!((operativeDto != null) && (operativeDto.getName() != null))) {
        _and = false;
      } else {
        String _name = operativeDto.getName();
        BlipDtoPath _dtoPath = null;
        if (blipItem!=null) {
          _dtoPath=blipItem.getDtoPath();
        }
        boolean _equals = _name.equals(_dtoPath);
        boolean _not = (!_equals);
        _and = _not;
      }
      if (_and) {
        BlipDtoPath _dtoPath_1 = null;
        if (blipItem!=null) {
          _dtoPath_1=blipItem.getDtoPath();
        }
        String _dtoPath_2 = _dtoPath_1.getDtoPath();
        String _name_1 = operativeDto.getName();
        String _plus = (_name_1 + ".");
        final String[] dtoChildNames = _dtoPath_2.replace(_plus, "").split("\\.");
        int actual = 1;
        StringConcatenation _builder = new StringConcatenation();
        String method = _builder.toString();
        for (final String dtoChildName : dtoChildNames) {
          {
            final QualifiedName previousclass = this._iQualifiedNameProvider.getFullyQualifiedName(operativeDto);
            final LDtoAbstractReference dtoFeature = BlipHelper.getDtoFeature(operativeDto, dtoChildName);
            operativeDto = BlipHelper.getReferencedLDto(operativeDto, dtoChildName);
            boolean multiple = false;
            String getter = "";
            final QualifiedName actualclass = this._iQualifiedNameProvider.getFullyQualifiedName(operativeDto);
            final int previous = (actual - 1);
            if ((dtoFeature instanceof LDtoInheritedReference)) {
              LMultiplicity _multiplicity = ((LDtoInheritedReference)dtoFeature).getInheritedFeature().getMultiplicity();
              boolean _tripleNotEquals = (_multiplicity != null);
              multiple = _tripleNotEquals;
              StringConcatenation _builder_1 = new StringConcatenation();
              _builder_1.append("get");
              String _capitalize = StringUtils.capitalize(((LDtoInheritedReference)dtoFeature).getInheritedFeature().getName());
              _builder_1.append(_capitalize);
              _builder_1.append("()");
              getter = _builder_1.toString();
            } else {
              if ((dtoFeature instanceof LDtoReference)) {
                LMultiplicity _multiplicity_1 = ((LDtoReference)dtoFeature).getMultiplicity();
                boolean _tripleNotEquals_1 = (_multiplicity_1 != null);
                multiple = _tripleNotEquals_1;
                StringConcatenation _builder_2 = new StringConcatenation();
                _builder_2.append("get");
                String _capitalize_1 = StringUtils.capitalize(((LDtoReference)dtoFeature).getName());
                _builder_2.append(_capitalize_1);
                _builder_2.append("()");
                getter = _builder_2.toString();
              }
            }
            StringConcatenation _builder_3 = new StringConcatenation();
            _builder_3.append(method);
            _builder_3.newLineIfNotEmpty();
            _builder_3.append("List<");
            _builder_3.append(actualclass);
            _builder_3.append("> result");
            _builder_3.append(actual);
            _builder_3.append(" = new java.util.ArrayList<>();");
            _builder_3.newLineIfNotEmpty();
            method = _builder_3.toString();
            if ((actual > 1)) {
              StringConcatenation _builder_4 = new StringConcatenation();
              _builder_4.append(method);
              _builder_4.newLineIfNotEmpty();
              _builder_4.append("for (");
              _builder_4.append(previousclass);
              _builder_4.append(" item");
              _builder_4.append(previous);
              _builder_4.append(" : result");
              _builder_4.append(previous);
              _builder_4.append(") {");
              _builder_4.newLineIfNotEmpty();
              method = _builder_4.toString();
            }
            if (multiple) {
              StringConcatenation _builder_5 = new StringConcatenation();
              _builder_5.append(method);
              _builder_5.newLineIfNotEmpty();
              _builder_5.append("result");
              _builder_5.append(actual);
              _builder_5.append(".addAll(item");
              _builder_5.append(previous);
              _builder_5.append(".");
              _builder_5.append(getter);
              _builder_5.append(");");
              _builder_5.newLineIfNotEmpty();
              method = _builder_5.toString();
            } else {
              StringConcatenation _builder_6 = new StringConcatenation();
              _builder_6.append(method);
              _builder_6.newLineIfNotEmpty();
              _builder_6.append(actualclass);
              _builder_6.append(" item = item");
              _builder_6.append(previous);
              _builder_6.append(".");
              _builder_6.append(getter);
              _builder_6.append(";");
              _builder_6.newLineIfNotEmpty();
              _builder_6.append("if\t(item != null) {");
              _builder_6.newLine();
              _builder_6.append("\t");
              _builder_6.append("result");
              _builder_6.append(actual, "\t");
              _builder_6.append(".add(item);");
              _builder_6.newLineIfNotEmpty();
              _builder_6.append("}");
              _builder_6.newLine();
              method = _builder_6.toString();
            }
            if ((actual > 1)) {
              StringConcatenation _builder_7 = new StringConcatenation();
              _builder_7.append(method);
              _builder_7.newLineIfNotEmpty();
              _builder_7.append("}");
              _builder_7.newLine();
              method = _builder_7.toString();
            }
            actual = (actual + 1);
          }
        }
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append(method);
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("return result");
        _builder_1.append((actual - 1));
        _builder_1.append(";");
        _builder_1.newLineIfNotEmpty();
        return _builder_1.toString();
      }
    }
    StringConcatenation _builder_2 = new StringConcatenation();
    _builder_2.append("return ");
    _builder_2.append(rootDto);
    _builder_2.append(";");
    return _builder_2.toString();
  }
  
  public static String getResourceName(final Blip blip) {
    org.eclipse.bpmn2.Process _process = blip.getProcess();
    boolean _tripleNotEquals = (_process != null);
    if (_tripleNotEquals) {
      org.eclipse.bpmn2.Process _process_1 = blip.getProcess();
      EObject parent = ((EObject) _process_1);
      while (((parent != null) && (!(parent instanceof DocumentRoot)))) {
        parent = parent.eContainer();
      }
      boolean _matched = false;
      if (parent instanceof DocumentRoot) {
        _matched=true;
        URI uri = ((DocumentRoot)parent).eResource().getURI();
        int _segmentCount = uri.segmentCount();
        int _minus = (_segmentCount - 2);
        String _segment = uri.segment(_minus);
        String _plus = (_segment + "/");
        int _segmentCount_1 = uri.segmentCount();
        int _minus_1 = (_segmentCount_1 - 1);
        String _segment_1 = uri.segment(_minus_1);
        String relPath = (_plus + _segment_1);
        return relPath.replace("%20", " ");
      }
    }
    return "<no-process-found>";
  }
  
  public void toOperations(final JvmDeclaredType type, final BlipPackage blipPkg) {
    final Procedure1<JvmOperation> _function = (JvmOperation it) -> {
      EList<JvmAnnotationReference> _annotations = it.getAnnotations();
      JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Activate.class);
      this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
      EList<JvmFormalParameter> _parameters = it.getParameters();
      JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(blipPkg, "context", this._typeReferenceBuilder.typeRef(ComponentContext.class));
      this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
      final Procedure1<ITreeAppendable> _function_1 = (ITreeAppendable it_1) -> {
        StringConcatenation _builder = new StringConcatenation();
        String _createBPMNCalls = this.createBPMNCalls(blipPkg);
        _builder.append(_createBPMNCalls);
        _builder.newLineIfNotEmpty();
        it_1.append(_builder);
      };
      this._jvmTypesBuilder.setBody(it, _function_1);
    };
    JvmOperation ac = this._jvmTypesBuilder.toMethod(blipPkg, "activate", this._typeReferenceBuilder.typeRef(Void.TYPE), _function);
    ac.setVisibility(JvmVisibility.PROTECTED);
    EList<JvmMember> _members = type.getMembers();
    this._jvmTypesBuilder.<JvmOperation>operator_add(_members, ac);
  }
  
  public void infer(final EObject blipPkg, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPreIndexingPhase) {
    if (blipPkg instanceof BlipPackage) {
      _infer((BlipPackage)blipPkg, acceptor, isPreIndexingPhase);
      return;
    } else if (blipPkg != null) {
      _infer(blipPkg, acceptor, isPreIndexingPhase);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(blipPkg, acceptor, isPreIndexingPhase).toString());
    }
  }
}
