/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Contributors:
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.blip.validation;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import org.eclipse.bpmn2.Task;
import org.eclipse.bpmn2.impl.TaskImpl;
import org.eclipse.bpmn2.impl.UserTaskImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.bpmn2.ecore.BPMnHelper;
import org.eclipse.osbp.xtext.basic.validation.IBasicValidatorDelegate;
import org.eclipse.osbp.xtext.blip.Blip;
import org.eclipse.osbp.xtext.blip.BlipCallActivity;
import org.eclipse.osbp.xtext.blip.BlipDSLPackage;
import org.eclipse.osbp.xtext.blip.BlipExclusiveSplitGateway;
import org.eclipse.osbp.xtext.blip.BlipItem;
import org.eclipse.osbp.xtext.blip.BlipPersistTask;
import org.eclipse.osbp.xtext.blip.BlipScriptTask;
import org.eclipse.osbp.xtext.blip.BlipUserTask;
import org.eclipse.osbp.xtext.blip.common.BlipHelper;
import org.eclipse.osbp.xtext.blip.validation.AbstractBlipDSLValidator;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.validation.Check;

/**
 * Custom validation rules.
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#validation
 */
@SuppressWarnings("all")
public class BlipDSLValidator extends AbstractBlipDSLValidator {
  @Inject(optional = true)
  private IBasicValidatorDelegate delegate;
  
  @Check
  public void checkBPMlicensed(final Blip blip) {
    org.eclipse.bpmn2.Process _process = blip.getProcess();
    boolean _notEquals = (!Objects.equal(_process, null));
    if (_notEquals) {
      if (((!Objects.equal(this.delegate, null)) && (!this.delegate.validateCommercial("blip", "net.osbee.bpm")))) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("BPM is needed and not yet licensed. License BPM at www.osbee.net");
        this.info(_builder.toString(), blip, 
          BlipDSLPackage.Literals.BLIP__PROCESS);
      }
    }
  }
  
  @Check
  public void checkStartsWithCapital(final Blip blip) {
    if (((!Objects.equal(blip.getProcess(), null)) && (!Character.isUpperCase(blip.getProcess().getId().charAt(0))))) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("It\'s recommended, that BPMs id \'");
      String _id = blip.getProcess().getId();
      _builder.append(_id);
      _builder.append("\' should start with a capital! You have to edit the BPM definition first to solve this!");
      this.info(_builder.toString(), blip, BlipDSLPackage.Literals.BLIP__PROCESS);
    }
  }
  
  public final static String RECOMMENDED_BLIP_ITEM_NAME = "recommendedBlipItemName";
  
  @Check
  public void checkRecommendedItemName(final BlipItem blipItem) {
    String bpmItemName = BlipHelper.getBpmItemRecommendedName(blipItem);
    if (((!bpmItemName.isEmpty()) && (!blipItem.getName().equals(bpmItemName)))) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("If a BPM is defined, it\'s recommended to use the BPMs item id \'");
      _builder.append(bpmItemName);
      _builder.append("\' as the item name too!");
      this.warning(_builder.toString(), blipItem, BlipDSLPackage.Literals.BLIP_ITEM__NAME, BlipDSLValidator.RECOMMENDED_BLIP_ITEM_NAME, bpmItemName);
    }
  }
  
  public final static String RECOMMENDED_BLIP_NAME = "recommendedBlipName";
  
  @Check
  public void checkRecommendedProcessName(final Blip blip) {
    if (((!Objects.equal(blip.getProcess(), null)) && (!blip.getName().equals(blip.getProcess().getId())))) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("If a BPM is defined, it\'s recommended to use the BPMs id \'");
      String _id = blip.getProcess().getId();
      _builder.append(_id);
      _builder.append("\' as the process name too!");
      this.warning(_builder.toString(), blip, BlipDSLPackage.Literals.BLIP_BASE__NAME, BlipDSLValidator.RECOMMENDED_BLIP_NAME, blip.getProcess().getId());
    }
  }
  
  @Check
  public void checkSubDtoPathOnBlipItem(final BlipItem blipItem) {
    boolean _isOperativeDtoDefined = BlipHelper.isOperativeDtoDefined(blipItem);
    if (_isOperativeDtoDefined) {
      boolean _contains = BlipHelper.getAvailableDtoPaths(blipItem).contains(blipItem.getDtoPath().getDtoPath());
      boolean _not = (!_contains);
      if (_not) {
        this.error(
          "Operative Dto is set, but doesn\'t exist inside workload Dto", blipItem, 
          BlipDSLPackage.Literals.BLIP_ITEM__DTO_PATH);
      }
    }
  }
  
  @Check
  public void checkRecommendedTaskProperties(final TaskImpl task) {
    System.err.println("are there any missing checks for recommended task properties?");
  }
  
  @Check
  public void checkRecommendedUserTaskProperties(final UserTaskImpl task) {
    System.err.println("are there any missing checks for recommended user task properties?");
  }
  
  @Check
  public void checkRecommendedUserTaskProperties(final BlipUserTask task) {
    Task _task = task.getTask();
    if ((_task instanceof Task)) {
      EObject _eContainer = task.eContainer();
      final Blip blip = ((Blip) _eContainer);
      final Task bpmTask = task.getTask();
      final String taskName = BPMnHelper.getBpmTaskDataInputProperty(bpmTask, "TaskName");
      boolean _equals = bpmTask.getName().equals(taskName);
      boolean _not = (!_equals);
      if (_not) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("It\'s required, that the General - Name \'");
        String _name = bpmTask.getName();
        _builder.append(_name);
        _builder.append("\' and the User Task - Task Name \'");
        _builder.append(taskName);
        _builder.append("\' of the user task \'");
        String _name_1 = bpmTask.getName();
        _builder.append(_name_1);
        _builder.append("\' in the BPM \'");
        String _id = blip.getProcess().getId();
        _builder.append(_id);
        _builder.append("\' have to be identical! You have to edit the BPM definition to solve this!");
        this.error(_builder.toString(), task, BlipDSLPackage.Literals.BLIP_USER_TASK__TASK);
      }
    }
  }
  
  @Check
  public Object checkRecommendedCallActivityProperties(final BlipCallActivity blipCallActivity) {
    return null;
  }
  
  @Check
  public void checkRecommendedScriptTaskProperties(final BlipScriptTask task) {
    System.err.println("are there any missing checks for recommended script task properties?");
  }
  
  @Check
  public void checkRecommendedPersistTaskProperties(final BlipPersistTask task) {
    System.err.println("are there any missing checks for recommended persist task properties?");
  }
  
  @Check
  public void checkRecommendedExclusiveSplitGatewayProperties(final BlipExclusiveSplitGateway gateway) {
    System.err.println("are there any missing checks for recommended exclusive split gateway properties?");
  }
}
