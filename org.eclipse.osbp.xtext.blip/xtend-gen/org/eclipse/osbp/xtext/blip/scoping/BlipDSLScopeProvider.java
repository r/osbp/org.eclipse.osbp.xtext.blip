/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Contributors:
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.blip.scoping;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.bpmn2.CallActivity;
import org.eclipse.bpmn2.EndEvent;
import org.eclipse.bpmn2.ExclusiveGateway;
import org.eclipse.bpmn2.FlowElement;
import org.eclipse.bpmn2.Gateway;
import org.eclipse.bpmn2.GatewayDirection;
import org.eclipse.bpmn2.InclusiveGateway;
import org.eclipse.bpmn2.SequenceFlow;
import org.eclipse.bpmn2.StartEvent;
import org.eclipse.bpmn2.UserTask;
import org.eclipse.bpmn2.impl.ScriptTaskImpl;
import org.eclipse.bpmn2.impl.ServiceTaskImpl;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.osbp.xtext.blip.Blip;
import org.eclipse.osbp.xtext.blip.BlipCallActivity;
import org.eclipse.osbp.xtext.blip.BlipDSLPackage;
import org.eclipse.osbp.xtext.blip.BlipEndEvent;
import org.eclipse.osbp.xtext.blip.BlipExclusiveSplitGateway;
import org.eclipse.osbp.xtext.blip.BlipInclusiveSplitGateway;
import org.eclipse.osbp.xtext.blip.BlipItem;
import org.eclipse.osbp.xtext.blip.BlipOutGoing;
import org.eclipse.osbp.xtext.blip.BlipScript;
import org.eclipse.osbp.xtext.blip.BlipServiceTask;
import org.eclipse.osbp.xtext.blip.BlipSplitGateway;
import org.eclipse.osbp.xtext.blip.BlipStartEvent;
import org.eclipse.osbp.xtext.blip.BlipUserTask;
import org.eclipse.osbp.xtext.blip.scoping.AbstractBlipDSLScopeProvider;
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryFunction;
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryTest;
import org.eclipse.xtext.resource.EObjectDescription;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.impl.DefaultGlobalScopeProvider;
import org.eclipse.xtext.scoping.impl.MapBasedScope;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

@SuppressWarnings("all")
public class BlipDSLScopeProvider extends AbstractBlipDSLScopeProvider {
  @Inject
  private DefaultGlobalScopeProvider globalScopeProvider;
  
  @Override
  public IScope getScope(final EObject context, final EReference reference) {
    IScope _xblockexpression = null;
    {
      boolean _equals = Objects.equal(reference, BlipDSLPackage.Literals.BLIP_SCRIPT__TASK);
      if (_equals) {
        return this.getScope_BLIP_SCRIPT_TASK(((BlipScript) context));
      }
      boolean _equals_1 = Objects.equal(reference, BlipDSLPackage.Literals.BLIP_USER_TASK__TASK);
      if (_equals_1) {
        return this.getScope_BLIP_USER_TASK(((BlipUserTask) context));
      }
      boolean _equals_2 = Objects.equal(reference, BlipDSLPackage.Literals.BLIP_CALL_ACTIVITY__CALL_ACTIVITY);
      if (_equals_2) {
        return this.getScope_BLIP_CALL_ACTIVITY(((BlipCallActivity) context));
      }
      boolean _equals_3 = Objects.equal(reference, BlipDSLPackage.Literals.BLIP_SPLIT_GATEWAY__GATEWAY);
      if (_equals_3) {
        return this.getScope_BLIP_SPLIT_GATEWAY_GATEWAY(((BlipSplitGateway) context));
      }
      boolean _equals_4 = Objects.equal(reference, BlipDSLPackage.Literals.BLIP_OUT_GOING__SEQUENCE_FLOW);
      if (_equals_4) {
        return this.getScope_BLIP_OUTGOING_SEQUENCE(((BlipOutGoing) context));
      }
      boolean _equals_5 = Objects.equal(reference, BlipDSLPackage.Literals.BLIP_EVENT__EVENT);
      if (_equals_5) {
        boolean _matched = false;
        if (context instanceof BlipStartEvent) {
          _matched=true;
          return this.getScope_BLIP_START_EVENT__EVENT(((BlipStartEvent)context));
        }
        if (!_matched) {
          if (context instanceof BlipEndEvent) {
            _matched=true;
            return this.getScope_BLIP_END_EVENT__EVENT(((BlipEndEvent)context));
          }
        }
      }
      boolean _equals_6 = Objects.equal(reference, BlipDSLPackage.Literals.BLIP_USER_TASK__ON_ENTRY);
      if (_equals_6) {
        return this.getScope_BLIP_ITEM__FUNCTION(((BlipItem) context));
      }
      boolean _equals_7 = Objects.equal(reference, BlipDSLPackage.Literals.BLIP_USER_TASK__ON_EXIT);
      if (_equals_7) {
        return this.getScope_BLIP_ITEM__FUNCTION(((BlipItem) context));
      }
      boolean _equals_8 = Objects.equal(reference, BlipDSLPackage.Literals.BLIP_CALL_ACTIVITY__ON_ENTRY);
      if (_equals_8) {
        return this.getScope_BLIP_ITEM__FUNCTION(((BlipItem) context));
      }
      boolean _equals_9 = Objects.equal(reference, BlipDSLPackage.Literals.BLIP_CALL_ACTIVITY__ON_EXIT);
      if (_equals_9) {
        return this.getScope_BLIP_ITEM__FUNCTION(((BlipItem) context));
      }
      boolean _equals_10 = Objects.equal(reference, BlipDSLPackage.Literals.BLIP_SCRIPT__FUNCTION);
      if (_equals_10) {
        return this.getScope_BLIP_ITEM__FUNCTION(((BlipItem) context));
      }
      boolean _equals_11 = Objects.equal(reference, BlipDSLPackage.Literals.BLIP_SERVICE_TASK__TASK);
      if (_equals_11) {
        return this.getScope_BLIP_SERVICE_TASK(((BlipServiceTask) context));
      }
      boolean _equals_12 = Objects.equal(reference, BlipDSLPackage.Literals.BLIP_SERVICE_TASK__ON_ENTRY);
      if (_equals_12) {
        return this.getScope_BLIP_ITEM__FUNCTION(((BlipItem) context));
      }
      boolean _equals_13 = Objects.equal(reference, BlipDSLPackage.Literals.BLIP_SERVICE_TASK__ON_EXIT);
      if (_equals_13) {
        return this.getScope_BLIP_ITEM__FUNCTION(((BlipItem) context));
      }
      boolean _equals_14 = Objects.equal(reference, BlipDSLPackage.Literals.BLIP_SERVICE_TASK__FUNCTION);
      if (_equals_14) {
        return this.getScope_BLIP_ITEM__FUNCTION(((BlipItem) context));
      }
      boolean _equals_15 = Objects.equal(reference, BlipDSLPackage.Literals.BLIP_OUT_GOING__CONSTRAINT);
      if (_equals_15) {
        return this.getScope_BLIP_ITEM__TEST(((BlipItem) context));
      }
      _xblockexpression = super.getScope(context, reference);
    }
    return _xblockexpression;
  }
  
  public IScope getScope_BLIP_ITEM__FUNCTION(final BlipItem blipItem) {
    ArrayList<IEObjectDescription> result = CollectionLiterals.<IEObjectDescription>newArrayList();
    EObject blip = ((EObject) blipItem);
    while ((!(blip instanceof Blip))) {
      blip = blip.eContainer();
    }
    if (((blip instanceof Blip) && (!Objects.equal(((Blip) blip).getFunctionGroup(), null)))) {
      EList<FunctionLibraryFunction> _functions = ((Blip) blip).getFunctionGroup().getFunctions();
      for (final FunctionLibraryFunction element : _functions) {
        result.add(EObjectDescription.create(element.getName(), ((FunctionLibraryFunction) element)));
      }
    }
    return MapBasedScope.createScope(IScope.NULLSCOPE, result);
  }
  
  public IScope getScope_BLIP_ITEM__TEST(final BlipItem blipItem) {
    ArrayList<IEObjectDescription> result = CollectionLiterals.<IEObjectDescription>newArrayList();
    EObject blip = ((EObject) blipItem);
    while ((!(blip instanceof Blip))) {
      blip = blip.eContainer();
    }
    if (((blip instanceof Blip) && (!Objects.equal(((Blip) blip).getFunctionGroup(), null)))) {
      EList<FunctionLibraryTest> _tests = ((Blip) blip).getFunctionGroup().getTests();
      for (final FunctionLibraryTest element : _tests) {
        result.add(EObjectDescription.create(element.getName(), ((FunctionLibraryTest) element)));
      }
    }
    return MapBasedScope.createScope(IScope.NULLSCOPE, result);
  }
  
  public IScope getScope_BLIP_SCRIPT_TASK(final BlipScript scriptTask) {
    ArrayList<IEObjectDescription> result = CollectionLiterals.<IEObjectDescription>newArrayList();
    EObject _eContainer = scriptTask.eContainer();
    Blip blip = ((Blip) _eContainer);
    if ((((!Objects.equal(blip, null)) && (!Objects.equal(blip.getProcess(), null))) && (!Objects.equal(blip.getProcess().getFlowElements(), null)))) {
      List<FlowElement> _flowElements = blip.getProcess().getFlowElements();
      for (final FlowElement element : _flowElements) {
        if ((element instanceof ScriptTaskImpl)) {
          result.add(EObjectDescription.create(((ScriptTaskImpl)element).getId(), ((ScriptTaskImpl) element)));
        }
      }
    }
    return MapBasedScope.createScope(IScope.NULLSCOPE, result);
  }
  
  public IScope getScope_BLIP_SERVICE_TASK(final BlipServiceTask serviceTask) {
    ArrayList<IEObjectDescription> result = CollectionLiterals.<IEObjectDescription>newArrayList();
    EObject _eContainer = serviceTask.eContainer();
    Blip blip = ((Blip) _eContainer);
    if ((((!Objects.equal(blip, null)) && (!Objects.equal(blip.getProcess(), null))) && (!Objects.equal(blip.getProcess().getFlowElements(), null)))) {
      List<FlowElement> _flowElements = blip.getProcess().getFlowElements();
      for (final FlowElement element : _flowElements) {
        if ((element instanceof ServiceTaskImpl)) {
          result.add(EObjectDescription.create(((ServiceTaskImpl)element).getId(), ((ServiceTaskImpl) element)));
        }
      }
    }
    return MapBasedScope.createScope(IScope.NULLSCOPE, result);
  }
  
  public IScope getScope_BLIP_USER_TASK(final BlipUserTask humanTask) {
    ArrayList<IEObjectDescription> result = CollectionLiterals.<IEObjectDescription>newArrayList();
    EObject _eContainer = humanTask.eContainer();
    Blip blip = ((Blip) _eContainer);
    if ((((!Objects.equal(blip, null)) && (!Objects.equal(blip.getProcess(), null))) && (!Objects.equal(blip.getProcess().getFlowElements(), null)))) {
      List<FlowElement> _flowElements = blip.getProcess().getFlowElements();
      for (final FlowElement element : _flowElements) {
        if ((element instanceof UserTask)) {
          result.add(EObjectDescription.create(((UserTask)element).getId(), ((UserTask) element)));
        }
      }
    }
    return MapBasedScope.createScope(IScope.NULLSCOPE, result);
  }
  
  public IScope getScope_BLIP_CALL_ACTIVITY(final BlipCallActivity callActivity) {
    ArrayList<IEObjectDescription> result = CollectionLiterals.<IEObjectDescription>newArrayList();
    EObject _eContainer = callActivity.eContainer();
    Blip blip = ((Blip) _eContainer);
    if ((((!Objects.equal(blip, null)) && (!Objects.equal(blip.getProcess(), null))) && (!Objects.equal(blip.getProcess().getFlowElements(), null)))) {
      List<FlowElement> _flowElements = blip.getProcess().getFlowElements();
      for (final FlowElement element : _flowElements) {
        if ((element instanceof CallActivity)) {
          result.add(EObjectDescription.create(((CallActivity)element).getId(), ((CallActivity) element)));
        }
      }
    }
    return MapBasedScope.createScope(IScope.NULLSCOPE, result);
  }
  
  public IScope getScope_BLIP_SPLIT_GATEWAY_GATEWAY(final BlipSplitGateway gateway) {
    ArrayList<IEObjectDescription> result = CollectionLiterals.<IEObjectDescription>newArrayList();
    EObject _eContainer = gateway.eContainer();
    Blip blip = ((Blip) _eContainer);
    if ((((!Objects.equal(blip, null)) && (!Objects.equal(blip.getProcess(), null))) && (!Objects.equal(blip.getProcess().getFlowElements(), null)))) {
      List<FlowElement> _flowElements = blip.getProcess().getFlowElements();
      for (final FlowElement element : _flowElements) {
        if ((element instanceof Gateway)) {
          GatewayDirection _gatewayDirection = ((Gateway)element).getGatewayDirection();
          boolean _equals = Objects.equal(_gatewayDirection, GatewayDirection.DIVERGING);
          if (_equals) {
            if (((gateway instanceof BlipExclusiveSplitGateway) && (element instanceof ExclusiveGateway))) {
              result.add(EObjectDescription.create(((Gateway)element).getId(), ((Gateway) element)));
            } else {
              if (((gateway instanceof BlipInclusiveSplitGateway) && (element instanceof InclusiveGateway))) {
                result.add(EObjectDescription.create(((Gateway)element).getId(), ((Gateway) element)));
              }
            }
          }
        }
      }
    }
    return MapBasedScope.createScope(IScope.NULLSCOPE, result);
  }
  
  public IScope getScope_BLIP_OUTGOING_SEQUENCE(final BlipOutGoing outgoing) {
    ArrayList<IEObjectDescription> result = CollectionLiterals.<IEObjectDescription>newArrayList();
    EObject _eContainer = outgoing.eContainer();
    BlipSplitGateway gateway = ((BlipSplitGateway) _eContainer);
    if ((((!Objects.equal(gateway, null)) && (!Objects.equal(gateway.getGateway(), null))) && (!gateway.getGateway().getOutgoing().isEmpty()))) {
      List<SequenceFlow> _outgoing = gateway.getGateway().getOutgoing();
      for (final FlowElement element : _outgoing) {
        if ((element instanceof SequenceFlow)) {
          result.add(EObjectDescription.create(((SequenceFlow)element).getId(), ((SequenceFlow) element)));
        }
      }
    }
    return MapBasedScope.createScope(IScope.NULLSCOPE, result);
  }
  
  public IScope getScope_BLIP_START_EVENT__EVENT(final BlipStartEvent blipStartEvent) {
    ArrayList<IEObjectDescription> result = CollectionLiterals.<IEObjectDescription>newArrayList();
    EObject _eContainer = blipStartEvent.eContainer();
    Blip blip = ((Blip) _eContainer);
    if ((((!Objects.equal(blip, null)) && (!Objects.equal(blip.getProcess(), null))) && (!Objects.equal(blip.getProcess().getFlowElements(), null)))) {
      List<FlowElement> _flowElements = blip.getProcess().getFlowElements();
      for (final FlowElement element : _flowElements) {
        if ((element instanceof StartEvent)) {
          result.add(EObjectDescription.create(((StartEvent)element).getId(), ((StartEvent) element)));
        }
      }
    }
    return MapBasedScope.createScope(IScope.NULLSCOPE, result);
  }
  
  public IScope getScope_BLIP_END_EVENT__EVENT(final BlipEndEvent blipEndEvent) {
    ArrayList<IEObjectDescription> result = CollectionLiterals.<IEObjectDescription>newArrayList();
    EObject _eContainer = blipEndEvent.eContainer();
    Blip blip = ((Blip) _eContainer);
    if ((((!Objects.equal(blip, null)) && (!Objects.equal(blip.getProcess(), null))) && (!Objects.equal(blip.getProcess().getFlowElements(), null)))) {
      List<FlowElement> _flowElements = blip.getProcess().getFlowElements();
      for (final FlowElement element : _flowElements) {
        if ((element instanceof EndEvent)) {
          result.add(EObjectDescription.create(((EndEvent)element).getId(), ((EndEvent) element)));
        }
      }
    }
    return MapBasedScope.createScope(IScope.NULLSCOPE, result);
  }
}
