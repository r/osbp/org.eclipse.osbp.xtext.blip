/**
 * Copyright (c) 2014, Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Loetz GmbH&Co.KG - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.blip;

import org.eclipse.bpmn2.CallActivity;

import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryFunction;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Blip Call Activity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.blip.BlipCallActivity#getCallActivity <em>Call Activity</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.blip.BlipCallActivity#getOnEntry <em>On Entry</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.blip.BlipCallActivity#getOnExit <em>On Exit</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipCallActivity()
 * @model
 * @generated
 */
public interface BlipCallActivity extends BlipItem {
	/**
	 * Returns the value of the '<em><b>Call Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Call Activity</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Call Activity</em>' reference.
	 * @see #setCallActivity(CallActivity)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipCallActivity_CallActivity()
	 * @model
	 * @generated
	 */
	CallActivity getCallActivity();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.BlipCallActivity#getCallActivity <em>Call Activity</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Call Activity</em>' reference.
	 * @see #getCallActivity()
	 * @generated
	 */
	void setCallActivity(CallActivity value);

	/**
	 * Returns the value of the '<em><b>On Entry</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>On Entry</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>On Entry</em>' reference.
	 * @see #setOnEntry(FunctionLibraryFunction)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipCallActivity_OnEntry()
	 * @model
	 * @generated
	 */
	FunctionLibraryFunction getOnEntry();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.BlipCallActivity#getOnEntry <em>On Entry</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>On Entry</em>' reference.
	 * @see #getOnEntry()
	 * @generated
	 */
	void setOnEntry(FunctionLibraryFunction value);

	/**
	 * Returns the value of the '<em><b>On Exit</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>On Exit</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>On Exit</em>' reference.
	 * @see #setOnExit(FunctionLibraryFunction)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipCallActivity_OnExit()
	 * @model
	 * @generated
	 */
	FunctionLibraryFunction getOnExit();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.BlipCallActivity#getOnExit <em>On Exit</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>On Exit</em>' reference.
	 * @see #getOnExit()
	 * @generated
	 */
	void setOnExit(FunctionLibraryFunction value);

} // BlipCallActivity
