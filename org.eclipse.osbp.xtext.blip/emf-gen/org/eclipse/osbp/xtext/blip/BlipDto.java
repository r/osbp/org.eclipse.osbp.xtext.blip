/**
 * Copyright (c) 2014, Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Loetz GmbH&Co.KG - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.blip;

import org.eclipse.osbp.dsl.semantic.dto.LDto;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Blip Dto</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.blip.BlipDto#getDtoRef <em>Dto Ref</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipDto()
 * @model
 * @generated
 */
public interface BlipDto extends BlipLazyResolver {
	/**
	 * Returns the value of the '<em><b>Dto Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dto Ref</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dto Ref</em>' reference.
	 * @see #setDtoRef(LDto)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipDto_DtoRef()
	 * @model
	 * @generated
	 */
	LDto getDtoRef();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.BlipDto#getDtoRef <em>Dto Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dto Ref</em>' reference.
	 * @see #getDtoRef()
	 * @generated
	 */
	void setDtoRef(LDto value);

} // BlipDto
