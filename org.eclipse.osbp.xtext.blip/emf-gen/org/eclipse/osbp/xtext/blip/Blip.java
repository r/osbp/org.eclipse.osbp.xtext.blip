/**
 * Copyright (c) 2014, Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Loetz GmbH&Co.KG - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.blip;

import org.eclipse.emf.common.util.EList;

import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryBlipGroup;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Blip</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.blip.Blip#getProcess <em>Process</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.blip.Blip#isDescription <em>Description</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.blip.Blip#getDescriptionValue <em>Description Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.blip.Blip#isHasImage <em>Has Image</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.blip.Blip#getImage <em>Image</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.blip.Blip#isHasIcon <em>Has Icon</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.blip.Blip#getIcon <em>Icon</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.blip.Blip#isHasLogging <em>Has Logging</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.blip.Blip#getFunctionGroup <em>Function Group</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.blip.Blip#getWorkload <em>Workload</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.blip.Blip#getItems <em>Items</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlip()
 * @model
 * @generated
 */
public interface Blip extends BlipBase {
	/**
	 * Returns the value of the '<em><b>Process</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process</em>' reference.
	 * @see #setProcess(org.eclipse.bpmn2.Process)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlip_Process()
	 * @model
	 * @generated
	 */
	org.eclipse.bpmn2.Process getProcess();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.Blip#getProcess <em>Process</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process</em>' reference.
	 * @see #getProcess()
	 * @generated
	 */
	void setProcess(org.eclipse.bpmn2.Process value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(boolean)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlip_Description()
	 * @model unique="false"
	 * @generated
	 */
	boolean isDescription();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.Blip#isDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #isDescription()
	 * @generated
	 */
	void setDescription(boolean value);

	/**
	 * Returns the value of the '<em><b>Description Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description Value</em>' attribute.
	 * @see #setDescriptionValue(String)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlip_DescriptionValue()
	 * @model unique="false"
	 * @generated
	 */
	String getDescriptionValue();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.Blip#getDescriptionValue <em>Description Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description Value</em>' attribute.
	 * @see #getDescriptionValue()
	 * @generated
	 */
	void setDescriptionValue(String value);

	/**
	 * Returns the value of the '<em><b>Has Image</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Image</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Image</em>' attribute.
	 * @see #setHasImage(boolean)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlip_HasImage()
	 * @model unique="false"
	 * @generated
	 */
	boolean isHasImage();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.Blip#isHasImage <em>Has Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Image</em>' attribute.
	 * @see #isHasImage()
	 * @generated
	 */
	void setHasImage(boolean value);

	/**
	 * Returns the value of the '<em><b>Image</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Image</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Image</em>' attribute.
	 * @see #setImage(String)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlip_Image()
	 * @model unique="false"
	 * @generated
	 */
	String getImage();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.Blip#getImage <em>Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Image</em>' attribute.
	 * @see #getImage()
	 * @generated
	 */
	void setImage(String value);

	/**
	 * Returns the value of the '<em><b>Has Icon</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Icon</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Icon</em>' attribute.
	 * @see #setHasIcon(boolean)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlip_HasIcon()
	 * @model unique="false"
	 * @generated
	 */
	boolean isHasIcon();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.Blip#isHasIcon <em>Has Icon</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Icon</em>' attribute.
	 * @see #isHasIcon()
	 * @generated
	 */
	void setHasIcon(boolean value);

	/**
	 * Returns the value of the '<em><b>Icon</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.xtext.blip.IconsEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Icon</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Icon</em>' attribute.
	 * @see org.eclipse.osbp.xtext.blip.IconsEnum
	 * @see #setIcon(IconsEnum)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlip_Icon()
	 * @model unique="false"
	 * @generated
	 */
	IconsEnum getIcon();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.Blip#getIcon <em>Icon</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Icon</em>' attribute.
	 * @see org.eclipse.osbp.xtext.blip.IconsEnum
	 * @see #getIcon()
	 * @generated
	 */
	void setIcon(IconsEnum value);

	/**
	 * Returns the value of the '<em><b>Has Logging</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Logging</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Logging</em>' attribute.
	 * @see #setHasLogging(boolean)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlip_HasLogging()
	 * @model unique="false"
	 * @generated
	 */
	boolean isHasLogging();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.Blip#isHasLogging <em>Has Logging</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Logging</em>' attribute.
	 * @see #isHasLogging()
	 * @generated
	 */
	void setHasLogging(boolean value);

	/**
	 * Returns the value of the '<em><b>Function Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Group</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Group</em>' reference.
	 * @see #setFunctionGroup(FunctionLibraryBlipGroup)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlip_FunctionGroup()
	 * @model
	 * @generated
	 */
	FunctionLibraryBlipGroup getFunctionGroup();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.Blip#getFunctionGroup <em>Function Group</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Function Group</em>' reference.
	 * @see #getFunctionGroup()
	 * @generated
	 */
	void setFunctionGroup(FunctionLibraryBlipGroup value);

	/**
	 * Returns the value of the '<em><b>Workload</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Workload</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Workload</em>' containment reference.
	 * @see #setWorkload(BlipWorkload)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlip_Workload()
	 * @model containment="true"
	 * @generated
	 */
	BlipWorkload getWorkload();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.Blip#getWorkload <em>Workload</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Workload</em>' containment reference.
	 * @see #getWorkload()
	 * @generated
	 */
	void setWorkload(BlipWorkload value);

	/**
	 * Returns the value of the '<em><b>Items</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.blip.BlipItem}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Items</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Items</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlip_Items()
	 * @model containment="true"
	 * @generated
	 */
	EList<BlipItem> getItems();

} // Blip
