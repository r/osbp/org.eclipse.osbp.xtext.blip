/**
 * Copyright (c) 2014, Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Loetz GmbH&Co.KG - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.blip.impl;

import org.eclipse.bpmn2.Bpmn2Package;

import org.eclipse.bpmn2.di.BpmnDiPackage;

import org.eclipse.dd.dc.DcPackage;

import org.eclipse.dd.di.DiPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage;

import org.eclipse.osbp.dsl.semantic.dto.OSBPDtoPackage;

import org.eclipse.osbp.xtext.blip.Blip;
import org.eclipse.osbp.xtext.blip.BlipBase;
import org.eclipse.osbp.xtext.blip.BlipCallActivity;
import org.eclipse.osbp.xtext.blip.BlipDSLFactory;
import org.eclipse.osbp.xtext.blip.BlipDSLPackage;
import org.eclipse.osbp.xtext.blip.BlipDto;
import org.eclipse.osbp.xtext.blip.BlipDtoPath;
import org.eclipse.osbp.xtext.blip.BlipEndEvent;
import org.eclipse.osbp.xtext.blip.BlipEvent;
import org.eclipse.osbp.xtext.blip.BlipExclusiveSplitGateway;
import org.eclipse.osbp.xtext.blip.BlipFilter;
import org.eclipse.osbp.xtext.blip.BlipInclusiveSplitGateway;
import org.eclipse.osbp.xtext.blip.BlipItem;
import org.eclipse.osbp.xtext.blip.BlipLazyResolver;
import org.eclipse.osbp.xtext.blip.BlipModel;
import org.eclipse.osbp.xtext.blip.BlipOutGoing;
import org.eclipse.osbp.xtext.blip.BlipOutGoingDefault;
import org.eclipse.osbp.xtext.blip.BlipPackage;
import org.eclipse.osbp.xtext.blip.BlipPersistTask;
import org.eclipse.osbp.xtext.blip.BlipScript;
import org.eclipse.osbp.xtext.blip.BlipScriptTask;
import org.eclipse.osbp.xtext.blip.BlipServiceTask;
import org.eclipse.osbp.xtext.blip.BlipSplitGateway;
import org.eclipse.osbp.xtext.blip.BlipStartEvent;
import org.eclipse.osbp.xtext.blip.BlipUserTask;
import org.eclipse.osbp.xtext.blip.BlipWorkload;
import org.eclipse.osbp.xtext.blip.EndEventHandlingEnum;
import org.eclipse.osbp.xtext.blip.IconsEnum;
import org.eclipse.osbp.xtext.blip.ServiceExecutionModeEnum;

import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryDSLPackage;

import org.eclipse.xtext.xtype.XtypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BlipDSLPackageImpl extends EPackageImpl implements BlipDSLPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blipModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blipLazyResolverEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blipPackageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blipBaseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blipEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blipWorkloadEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blipFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blipDtoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blipDtoPathEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blipItemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blipEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blipStartEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blipEndEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blipUserTaskEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blipCallActivityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blipScriptEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blipScriptTaskEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blipPersistTaskEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blipServiceTaskEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blipOutGoingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blipOutGoingDefaultEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blipSplitGatewayEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blipExclusiveSplitGatewayEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blipInclusiveSplitGatewayEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum endEventHandlingEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum serviceExecutionModeEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum iconsEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType internalEObjectEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private BlipDSLPackageImpl() {
		super(eNS_URI, BlipDSLFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link BlipDSLPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static BlipDSLPackage init() {
		if (isInited) return (BlipDSLPackage)EPackage.Registry.INSTANCE.getEPackage(BlipDSLPackage.eNS_URI);

		// Obtain or create and register package
		BlipDSLPackageImpl theBlipDSLPackage = (BlipDSLPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof BlipDSLPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new BlipDSLPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		Bpmn2Package.eINSTANCE.eClass();
		FunctionLibraryDSLPackage.eINSTANCE.eClass();
		OSBPDtoPackage.eINSTANCE.eClass();
		BpmnDiPackage.eINSTANCE.eClass();
		DiPackage.eINSTANCE.eClass();
		DcPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theBlipDSLPackage.createPackageContents();

		// Initialize created meta-data
		theBlipDSLPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theBlipDSLPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(BlipDSLPackage.eNS_URI, theBlipDSLPackage);
		return theBlipDSLPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlipModel() {
		return blipModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlipModel_ImportSection() {
		return (EReference)blipModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlipModel_Blippackage() {
		return (EReference)blipModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlipLazyResolver() {
		return blipLazyResolverEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBlipLazyResolver__EResolveProxy__InternalEObject() {
		return blipLazyResolverEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlipPackage() {
		return blipPackageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlipPackage_Blips() {
		return (EReference)blipPackageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlipBase() {
		return blipBaseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlipBase_Name() {
		return (EAttribute)blipBaseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlip() {
		return blipEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlip_Process() {
		return (EReference)blipEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlip_Description() {
		return (EAttribute)blipEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlip_DescriptionValue() {
		return (EAttribute)blipEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlip_HasImage() {
		return (EAttribute)blipEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlip_Image() {
		return (EAttribute)blipEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlip_HasIcon() {
		return (EAttribute)blipEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlip_Icon() {
		return (EAttribute)blipEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlip_HasLogging() {
		return (EAttribute)blipEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlip_FunctionGroup() {
		return (EReference)blipEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlip_Workload() {
		return (EReference)blipEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlip_Items() {
		return (EReference)blipEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlipWorkload() {
		return blipWorkloadEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlipWorkload_Source() {
		return (EReference)blipWorkloadEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlipWorkload_Filters() {
		return (EReference)blipWorkloadEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlipFilter() {
		return blipFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlipFilter_FilterName() {
		return (EAttribute)blipFilterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlipFilter_FilterValue() {
		return (EAttribute)blipFilterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlipDto() {
		return blipDtoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlipDto_DtoRef() {
		return (EReference)blipDtoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlipDtoPath() {
		return blipDtoPathEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlipDtoPath_DtoPath() {
		return (EAttribute)blipDtoPathEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlipItem() {
		return blipItemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlipItem_Name() {
		return (EAttribute)blipItemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlipItem_DtoPath() {
		return (EReference)blipItemEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBlipItem__GetOperativeLDto() {
		return blipItemEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlipEvent() {
		return blipEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlipEvent_Event() {
		return (EReference)blipEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlipStartEvent() {
		return blipStartEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlipEndEvent() {
		return blipEndEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlipEndEvent_EndHandling() {
		return (EAttribute)blipEndEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlipUserTask() {
		return blipUserTaskEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlipUserTask_Task() {
		return (EReference)blipUserTaskEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlipUserTask_OnEntry() {
		return (EReference)blipUserTaskEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlipUserTask_OnExit() {
		return (EReference)blipUserTaskEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlipCallActivity() {
		return blipCallActivityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlipCallActivity_CallActivity() {
		return (EReference)blipCallActivityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlipCallActivity_OnEntry() {
		return (EReference)blipCallActivityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlipCallActivity_OnExit() {
		return (EReference)blipCallActivityEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlipScript() {
		return blipScriptEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlipScript_Task() {
		return (EReference)blipScriptEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlipScript_Function() {
		return (EReference)blipScriptEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlipScriptTask() {
		return blipScriptTaskEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlipPersistTask() {
		return blipPersistTaskEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlipServiceTask() {
		return blipServiceTaskEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlipServiceTask_Task() {
		return (EReference)blipServiceTaskEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlipServiceTask_OnEntry() {
		return (EReference)blipServiceTaskEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlipServiceTask_OnExit() {
		return (EReference)blipServiceTaskEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlipServiceTask_ExecutionMode() {
		return (EAttribute)blipServiceTaskEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlipServiceTask_TimeoutInSecs() {
		return (EAttribute)blipServiceTaskEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlipServiceTask_Function() {
		return (EReference)blipServiceTaskEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlipServiceTask_WebServiceInterface() {
		return (EAttribute)blipServiceTaskEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlipServiceTask_WebServiceOperation() {
		return (EAttribute)blipServiceTaskEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlipOutGoing() {
		return blipOutGoingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlipOutGoing_SequenceFlow() {
		return (EReference)blipOutGoingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlipOutGoing_Constraint() {
		return (EReference)blipOutGoingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlipOutGoing_IsDefault() {
		return (EAttribute)blipOutGoingEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlipOutGoingDefault() {
		return blipOutGoingDefaultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlipSplitGateway() {
		return blipSplitGatewayEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlipSplitGateway_Gateway() {
		return (EReference)blipSplitGatewayEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlipSplitGateway_Outgoings() {
		return (EReference)blipSplitGatewayEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlipExclusiveSplitGateway() {
		return blipExclusiveSplitGatewayEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlipInclusiveSplitGateway() {
		return blipInclusiveSplitGatewayEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getEndEventHandlingEnum() {
		return endEventHandlingEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getServiceExecutionModeEnum() {
		return serviceExecutionModeEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getIconsEnum() {
		return iconsEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getInternalEObject() {
		return internalEObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlipDSLFactory getBlipDSLFactory() {
		return (BlipDSLFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		blipModelEClass = createEClass(BLIP_MODEL);
		createEReference(blipModelEClass, BLIP_MODEL__IMPORT_SECTION);
		createEReference(blipModelEClass, BLIP_MODEL__BLIPPACKAGE);

		blipLazyResolverEClass = createEClass(BLIP_LAZY_RESOLVER);
		createEOperation(blipLazyResolverEClass, BLIP_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT);

		blipPackageEClass = createEClass(BLIP_PACKAGE);
		createEReference(blipPackageEClass, BLIP_PACKAGE__BLIPS);

		blipBaseEClass = createEClass(BLIP_BASE);
		createEAttribute(blipBaseEClass, BLIP_BASE__NAME);

		blipEClass = createEClass(BLIP);
		createEReference(blipEClass, BLIP__PROCESS);
		createEAttribute(blipEClass, BLIP__DESCRIPTION);
		createEAttribute(blipEClass, BLIP__DESCRIPTION_VALUE);
		createEAttribute(blipEClass, BLIP__HAS_IMAGE);
		createEAttribute(blipEClass, BLIP__IMAGE);
		createEAttribute(blipEClass, BLIP__HAS_ICON);
		createEAttribute(blipEClass, BLIP__ICON);
		createEAttribute(blipEClass, BLIP__HAS_LOGGING);
		createEReference(blipEClass, BLIP__FUNCTION_GROUP);
		createEReference(blipEClass, BLIP__WORKLOAD);
		createEReference(blipEClass, BLIP__ITEMS);

		blipWorkloadEClass = createEClass(BLIP_WORKLOAD);
		createEReference(blipWorkloadEClass, BLIP_WORKLOAD__SOURCE);
		createEReference(blipWorkloadEClass, BLIP_WORKLOAD__FILTERS);

		blipFilterEClass = createEClass(BLIP_FILTER);
		createEAttribute(blipFilterEClass, BLIP_FILTER__FILTER_NAME);
		createEAttribute(blipFilterEClass, BLIP_FILTER__FILTER_VALUE);

		blipDtoEClass = createEClass(BLIP_DTO);
		createEReference(blipDtoEClass, BLIP_DTO__DTO_REF);

		blipDtoPathEClass = createEClass(BLIP_DTO_PATH);
		createEAttribute(blipDtoPathEClass, BLIP_DTO_PATH__DTO_PATH);

		blipItemEClass = createEClass(BLIP_ITEM);
		createEAttribute(blipItemEClass, BLIP_ITEM__NAME);
		createEReference(blipItemEClass, BLIP_ITEM__DTO_PATH);
		createEOperation(blipItemEClass, BLIP_ITEM___GET_OPERATIVE_LDTO);

		blipEventEClass = createEClass(BLIP_EVENT);
		createEReference(blipEventEClass, BLIP_EVENT__EVENT);

		blipStartEventEClass = createEClass(BLIP_START_EVENT);

		blipEndEventEClass = createEClass(BLIP_END_EVENT);
		createEAttribute(blipEndEventEClass, BLIP_END_EVENT__END_HANDLING);

		blipUserTaskEClass = createEClass(BLIP_USER_TASK);
		createEReference(blipUserTaskEClass, BLIP_USER_TASK__TASK);
		createEReference(blipUserTaskEClass, BLIP_USER_TASK__ON_ENTRY);
		createEReference(blipUserTaskEClass, BLIP_USER_TASK__ON_EXIT);

		blipCallActivityEClass = createEClass(BLIP_CALL_ACTIVITY);
		createEReference(blipCallActivityEClass, BLIP_CALL_ACTIVITY__CALL_ACTIVITY);
		createEReference(blipCallActivityEClass, BLIP_CALL_ACTIVITY__ON_ENTRY);
		createEReference(blipCallActivityEClass, BLIP_CALL_ACTIVITY__ON_EXIT);

		blipScriptEClass = createEClass(BLIP_SCRIPT);
		createEReference(blipScriptEClass, BLIP_SCRIPT__TASK);
		createEReference(blipScriptEClass, BLIP_SCRIPT__FUNCTION);

		blipScriptTaskEClass = createEClass(BLIP_SCRIPT_TASK);

		blipPersistTaskEClass = createEClass(BLIP_PERSIST_TASK);

		blipServiceTaskEClass = createEClass(BLIP_SERVICE_TASK);
		createEReference(blipServiceTaskEClass, BLIP_SERVICE_TASK__TASK);
		createEReference(blipServiceTaskEClass, BLIP_SERVICE_TASK__ON_ENTRY);
		createEReference(blipServiceTaskEClass, BLIP_SERVICE_TASK__ON_EXIT);
		createEAttribute(blipServiceTaskEClass, BLIP_SERVICE_TASK__EXECUTION_MODE);
		createEAttribute(blipServiceTaskEClass, BLIP_SERVICE_TASK__TIMEOUT_IN_SECS);
		createEReference(blipServiceTaskEClass, BLIP_SERVICE_TASK__FUNCTION);
		createEAttribute(blipServiceTaskEClass, BLIP_SERVICE_TASK__WEB_SERVICE_INTERFACE);
		createEAttribute(blipServiceTaskEClass, BLIP_SERVICE_TASK__WEB_SERVICE_OPERATION);

		blipOutGoingEClass = createEClass(BLIP_OUT_GOING);
		createEReference(blipOutGoingEClass, BLIP_OUT_GOING__SEQUENCE_FLOW);
		createEReference(blipOutGoingEClass, BLIP_OUT_GOING__CONSTRAINT);
		createEAttribute(blipOutGoingEClass, BLIP_OUT_GOING__IS_DEFAULT);

		blipOutGoingDefaultEClass = createEClass(BLIP_OUT_GOING_DEFAULT);

		blipSplitGatewayEClass = createEClass(BLIP_SPLIT_GATEWAY);
		createEReference(blipSplitGatewayEClass, BLIP_SPLIT_GATEWAY__GATEWAY);
		createEReference(blipSplitGatewayEClass, BLIP_SPLIT_GATEWAY__OUTGOINGS);

		blipExclusiveSplitGatewayEClass = createEClass(BLIP_EXCLUSIVE_SPLIT_GATEWAY);

		blipInclusiveSplitGatewayEClass = createEClass(BLIP_INCLUSIVE_SPLIT_GATEWAY);

		// Create enums
		endEventHandlingEnumEEnum = createEEnum(END_EVENT_HANDLING_ENUM);
		serviceExecutionModeEnumEEnum = createEEnum(SERVICE_EXECUTION_MODE_ENUM);
		iconsEnumEEnum = createEEnum(ICONS_ENUM);

		// Create data types
		internalEObjectEDataType = createEDataType(INTERNAL_EOBJECT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		XtypePackage theXtypePackage = (XtypePackage)EPackage.Registry.INSTANCE.getEPackage(XtypePackage.eNS_URI);
		OSBPTypesPackage theOSBPTypesPackage = (OSBPTypesPackage)EPackage.Registry.INSTANCE.getEPackage(OSBPTypesPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		Bpmn2Package theBpmn2Package = (Bpmn2Package)EPackage.Registry.INSTANCE.getEPackage(Bpmn2Package.eNS_URI);
		FunctionLibraryDSLPackage theFunctionLibraryDSLPackage = (FunctionLibraryDSLPackage)EPackage.Registry.INSTANCE.getEPackage(FunctionLibraryDSLPackage.eNS_URI);
		OSBPDtoPackage theOSBPDtoPackage = (OSBPDtoPackage)EPackage.Registry.INSTANCE.getEPackage(OSBPDtoPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		blipPackageEClass.getESuperTypes().add(theOSBPTypesPackage.getLPackage());
		blipBaseEClass.getESuperTypes().add(this.getBlipLazyResolver());
		blipEClass.getESuperTypes().add(this.getBlipBase());
		blipWorkloadEClass.getESuperTypes().add(this.getBlipLazyResolver());
		blipFilterEClass.getESuperTypes().add(this.getBlipLazyResolver());
		blipDtoEClass.getESuperTypes().add(this.getBlipLazyResolver());
		blipEventEClass.getESuperTypes().add(this.getBlipItem());
		blipStartEventEClass.getESuperTypes().add(this.getBlipEvent());
		blipEndEventEClass.getESuperTypes().add(this.getBlipEvent());
		blipUserTaskEClass.getESuperTypes().add(this.getBlipItem());
		blipCallActivityEClass.getESuperTypes().add(this.getBlipItem());
		blipScriptEClass.getESuperTypes().add(this.getBlipItem());
		blipScriptTaskEClass.getESuperTypes().add(this.getBlipScript());
		blipPersistTaskEClass.getESuperTypes().add(this.getBlipScript());
		blipServiceTaskEClass.getESuperTypes().add(this.getBlipItem());
		blipOutGoingEClass.getESuperTypes().add(this.getBlipItem());
		blipOutGoingDefaultEClass.getESuperTypes().add(this.getBlipOutGoing());
		blipSplitGatewayEClass.getESuperTypes().add(this.getBlipItem());
		blipExclusiveSplitGatewayEClass.getESuperTypes().add(this.getBlipSplitGateway());
		blipInclusiveSplitGatewayEClass.getESuperTypes().add(this.getBlipSplitGateway());

		// Initialize classes, features, and operations; add parameters
		initEClass(blipModelEClass, BlipModel.class, "BlipModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBlipModel_ImportSection(), theXtypePackage.getXImportSection(), null, "importSection", null, 0, 1, BlipModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlipModel_Blippackage(), theOSBPTypesPackage.getLPackage(), null, "blippackage", null, 0, 1, BlipModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(blipLazyResolverEClass, BlipLazyResolver.class, "BlipLazyResolver", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getBlipLazyResolver__EResolveProxy__InternalEObject(), theEcorePackage.getEObject(), "eResolveProxy", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInternalEObject(), "proxy", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(blipPackageEClass, BlipPackage.class, "BlipPackage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBlipPackage_Blips(), this.getBlip(), null, "blips", null, 0, -1, BlipPackage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(blipBaseEClass, BlipBase.class, "BlipBase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBlipBase_Name(), theEcorePackage.getEString(), "name", null, 0, 1, BlipBase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(blipEClass, Blip.class, "Blip", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBlip_Process(), theBpmn2Package.getProcess(), null, "process", null, 0, 1, Blip.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBlip_Description(), theEcorePackage.getEBoolean(), "description", null, 0, 1, Blip.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBlip_DescriptionValue(), theEcorePackage.getEString(), "descriptionValue", null, 0, 1, Blip.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBlip_HasImage(), theEcorePackage.getEBoolean(), "hasImage", null, 0, 1, Blip.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBlip_Image(), theEcorePackage.getEString(), "image", null, 0, 1, Blip.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBlip_HasIcon(), theEcorePackage.getEBoolean(), "hasIcon", null, 0, 1, Blip.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBlip_Icon(), this.getIconsEnum(), "icon", null, 0, 1, Blip.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBlip_HasLogging(), theEcorePackage.getEBoolean(), "hasLogging", null, 0, 1, Blip.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlip_FunctionGroup(), theFunctionLibraryDSLPackage.getFunctionLibraryBlipGroup(), null, "functionGroup", null, 0, 1, Blip.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlip_Workload(), this.getBlipWorkload(), null, "workload", null, 0, 1, Blip.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlip_Items(), this.getBlipItem(), null, "items", null, 0, -1, Blip.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(blipWorkloadEClass, BlipWorkload.class, "BlipWorkload", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBlipWorkload_Source(), this.getBlipDto(), null, "source", null, 0, 1, BlipWorkload.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlipWorkload_Filters(), this.getBlipFilter(), null, "filters", null, 0, -1, BlipWorkload.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(blipFilterEClass, BlipFilter.class, "BlipFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBlipFilter_FilterName(), theEcorePackage.getEString(), "filterName", null, 0, 1, BlipFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBlipFilter_FilterValue(), theEcorePackage.getEString(), "filterValue", null, 0, 1, BlipFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(blipDtoEClass, BlipDto.class, "BlipDto", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBlipDto_DtoRef(), theOSBPDtoPackage.getLDto(), null, "dtoRef", null, 0, 1, BlipDto.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(blipDtoPathEClass, BlipDtoPath.class, "BlipDtoPath", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBlipDtoPath_DtoPath(), theEcorePackage.getEString(), "dtoPath", null, 0, 1, BlipDtoPath.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(blipItemEClass, BlipItem.class, "BlipItem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBlipItem_Name(), theEcorePackage.getEString(), "name", null, 0, 1, BlipItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlipItem_DtoPath(), this.getBlipDtoPath(), null, "dtoPath", null, 0, 1, BlipItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getBlipItem__GetOperativeLDto(), theOSBPDtoPackage.getLDto(), "getOperativeLDto", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(blipEventEClass, BlipEvent.class, "BlipEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBlipEvent_Event(), theBpmn2Package.getEvent(), null, "event", null, 0, 1, BlipEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(blipStartEventEClass, BlipStartEvent.class, "BlipStartEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(blipEndEventEClass, BlipEndEvent.class, "BlipEndEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBlipEndEvent_EndHandling(), this.getEndEventHandlingEnum(), "endHandling", null, 0, 1, BlipEndEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(blipUserTaskEClass, BlipUserTask.class, "BlipUserTask", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBlipUserTask_Task(), theBpmn2Package.getTask(), null, "task", null, 0, 1, BlipUserTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlipUserTask_OnEntry(), theFunctionLibraryDSLPackage.getFunctionLibraryFunction(), null, "onEntry", null, 0, 1, BlipUserTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlipUserTask_OnExit(), theFunctionLibraryDSLPackage.getFunctionLibraryFunction(), null, "onExit", null, 0, 1, BlipUserTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(blipCallActivityEClass, BlipCallActivity.class, "BlipCallActivity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBlipCallActivity_CallActivity(), theBpmn2Package.getCallActivity(), null, "callActivity", null, 0, 1, BlipCallActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlipCallActivity_OnEntry(), theFunctionLibraryDSLPackage.getFunctionLibraryFunction(), null, "onEntry", null, 0, 1, BlipCallActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlipCallActivity_OnExit(), theFunctionLibraryDSLPackage.getFunctionLibraryFunction(), null, "onExit", null, 0, 1, BlipCallActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(blipScriptEClass, BlipScript.class, "BlipScript", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBlipScript_Task(), theBpmn2Package.getScriptTask(), null, "task", null, 0, 1, BlipScript.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlipScript_Function(), theFunctionLibraryDSLPackage.getFunctionLibraryFunction(), null, "function", null, 0, 1, BlipScript.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(blipScriptTaskEClass, BlipScriptTask.class, "BlipScriptTask", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(blipPersistTaskEClass, BlipPersistTask.class, "BlipPersistTask", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(blipServiceTaskEClass, BlipServiceTask.class, "BlipServiceTask", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBlipServiceTask_Task(), theBpmn2Package.getServiceTask(), null, "task", null, 0, 1, BlipServiceTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlipServiceTask_OnEntry(), theFunctionLibraryDSLPackage.getFunctionLibraryFunction(), null, "onEntry", null, 0, 1, BlipServiceTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlipServiceTask_OnExit(), theFunctionLibraryDSLPackage.getFunctionLibraryFunction(), null, "onExit", null, 0, 1, BlipServiceTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBlipServiceTask_ExecutionMode(), this.getServiceExecutionModeEnum(), "executionMode", null, 0, 1, BlipServiceTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBlipServiceTask_TimeoutInSecs(), theEcorePackage.getEInt(), "timeoutInSecs", null, 0, 1, BlipServiceTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlipServiceTask_Function(), theFunctionLibraryDSLPackage.getFunctionLibraryFunction(), null, "function", null, 0, 1, BlipServiceTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBlipServiceTask_WebServiceInterface(), theEcorePackage.getEString(), "webServiceInterface", null, 0, 1, BlipServiceTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBlipServiceTask_WebServiceOperation(), theEcorePackage.getEString(), "webServiceOperation", null, 0, 1, BlipServiceTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(blipOutGoingEClass, BlipOutGoing.class, "BlipOutGoing", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBlipOutGoing_SequenceFlow(), theBpmn2Package.getSequenceFlow(), null, "sequenceFlow", null, 0, 1, BlipOutGoing.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlipOutGoing_Constraint(), theFunctionLibraryDSLPackage.getFunctionLibraryTest(), null, "constraint", null, 0, 1, BlipOutGoing.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBlipOutGoing_IsDefault(), theEcorePackage.getEBoolean(), "isDefault", null, 0, 1, BlipOutGoing.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(blipOutGoingDefaultEClass, BlipOutGoingDefault.class, "BlipOutGoingDefault", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(blipSplitGatewayEClass, BlipSplitGateway.class, "BlipSplitGateway", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBlipSplitGateway_Gateway(), theBpmn2Package.getGateway(), null, "gateway", null, 0, 1, BlipSplitGateway.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlipSplitGateway_Outgoings(), this.getBlipOutGoing(), null, "outgoings", null, 0, -1, BlipSplitGateway.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(blipExclusiveSplitGatewayEClass, BlipExclusiveSplitGateway.class, "BlipExclusiveSplitGateway", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(blipInclusiveSplitGatewayEClass, BlipInclusiveSplitGateway.class, "BlipInclusiveSplitGateway", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize enums and add enum literals
		initEEnum(endEventHandlingEnumEEnum, EndEventHandlingEnum.class, "EndEventHandlingEnum");
		addEEnumLiteral(endEventHandlingEnumEEnum, EndEventHandlingEnum.TERMINATES_PROCESS);
		addEEnumLiteral(endEventHandlingEnumEEnum, EndEventHandlingEnum.ENDS_TOKEN_PATH);

		initEEnum(serviceExecutionModeEnumEEnum, ServiceExecutionModeEnum.class, "ServiceExecutionModeEnum");
		addEEnumLiteral(serviceExecutionModeEnumEEnum, ServiceExecutionModeEnum.SYNC);
		addEEnumLiteral(serviceExecutionModeEnumEEnum, ServiceExecutionModeEnum.ONEWAY);

		initEEnum(iconsEnumEEnum, IconsEnum.class, "IconsEnum");
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_PLUS);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_MINUS);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_INFO);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_LEFT);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_UP);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_RIGHT);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_DOWN);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_EXCHANGE);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_HOME);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_HOME_1);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_UP_DIR);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_RIGHT_DIR);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_DOWN_DIR);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_LEFT_DIR);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_STAR);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_STAR_EMPTY);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_TH_LIST);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_HEART_EMPTY);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_HEART);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_MUSIC);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_TH);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_FLAG);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_COG);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_ATTENTION);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_MAIL);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_EDIT);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_PENCIL);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_OK);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_CANCEL_1);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_CANCEL);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_CANCEL_CIRCLE);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_HELP);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_PLUS_CIRCLE);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_MINUS_CIRCLE);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_RIGHT_THIN);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_FORWARD);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_CW);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_LEFT_THIN);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_UP_THIN);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_DOWN_THIN);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_LEFT_BOLD);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_RIGHT_BOLD);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_UP_BOLD);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_DOWN_BOLD);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_USER_ADD);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_HELP_CIRCLE);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_INFO_CIRCLE);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_BACK);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_EYE);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_TAG);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_UPLOAD_CLOUD);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_REPLY);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_EXPORT);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_PRINT);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_RETWEET);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_COMMENT);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_VCARD);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_LOCATION);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_TRASH);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_RESIZE_FULL);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_RESIZE_SMALL);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_DOWN_OPEN);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_LEFT_OPEN);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_RIGHT_OPEN);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_UP_OPEN);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_ARROWS_CW);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_CHART_PIE);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_SEARCH_1);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_USER);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_USERS);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_MONITOR);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_FOLDER);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_DOC);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_CALENDAR);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_CHART);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_ATTACH);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_UPLOAD);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_DOWNLOAD);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_MOBILE);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_CAMERA);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_LOCK);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_LOCK_OPEN);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_BELL);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_LINK);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_CLOCK);
		addEEnumLiteral(iconsEnumEEnum, IconsEnum.ICON_BLOCK);

		// Initialize data types
		initEDataType(internalEObjectEDataType, InternalEObject.class, "InternalEObject", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "rootPackage", "blipdsl"
		   });
	}

} //BlipDSLPackageImpl
