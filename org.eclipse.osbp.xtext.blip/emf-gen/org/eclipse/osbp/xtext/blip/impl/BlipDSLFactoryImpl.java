/**
 * Copyright (c) 2014, Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Loetz GmbH&Co.KG - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.blip.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.osbp.xtext.blip.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BlipDSLFactoryImpl extends EFactoryImpl implements BlipDSLFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static BlipDSLFactory init() {
		try {
			BlipDSLFactory theBlipDSLFactory = (BlipDSLFactory)EPackage.Registry.INSTANCE.getEFactory(BlipDSLPackage.eNS_URI);
			if (theBlipDSLFactory != null) {
				return theBlipDSLFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new BlipDSLFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlipDSLFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case BlipDSLPackage.BLIP_MODEL: return createBlipModel();
			case BlipDSLPackage.BLIP_LAZY_RESOLVER: return createBlipLazyResolver();
			case BlipDSLPackage.BLIP_PACKAGE: return createBlipPackage();
			case BlipDSLPackage.BLIP_BASE: return createBlipBase();
			case BlipDSLPackage.BLIP: return createBlip();
			case BlipDSLPackage.BLIP_WORKLOAD: return createBlipWorkload();
			case BlipDSLPackage.BLIP_FILTER: return createBlipFilter();
			case BlipDSLPackage.BLIP_DTO: return createBlipDto();
			case BlipDSLPackage.BLIP_DTO_PATH: return createBlipDtoPath();
			case BlipDSLPackage.BLIP_ITEM: return createBlipItem();
			case BlipDSLPackage.BLIP_EVENT: return createBlipEvent();
			case BlipDSLPackage.BLIP_START_EVENT: return createBlipStartEvent();
			case BlipDSLPackage.BLIP_END_EVENT: return createBlipEndEvent();
			case BlipDSLPackage.BLIP_USER_TASK: return createBlipUserTask();
			case BlipDSLPackage.BLIP_CALL_ACTIVITY: return createBlipCallActivity();
			case BlipDSLPackage.BLIP_SCRIPT: return createBlipScript();
			case BlipDSLPackage.BLIP_SCRIPT_TASK: return createBlipScriptTask();
			case BlipDSLPackage.BLIP_PERSIST_TASK: return createBlipPersistTask();
			case BlipDSLPackage.BLIP_SERVICE_TASK: return createBlipServiceTask();
			case BlipDSLPackage.BLIP_OUT_GOING: return createBlipOutGoing();
			case BlipDSLPackage.BLIP_OUT_GOING_DEFAULT: return createBlipOutGoingDefault();
			case BlipDSLPackage.BLIP_SPLIT_GATEWAY: return createBlipSplitGateway();
			case BlipDSLPackage.BLIP_EXCLUSIVE_SPLIT_GATEWAY: return createBlipExclusiveSplitGateway();
			case BlipDSLPackage.BLIP_INCLUSIVE_SPLIT_GATEWAY: return createBlipInclusiveSplitGateway();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case BlipDSLPackage.END_EVENT_HANDLING_ENUM:
				return createEndEventHandlingEnumFromString(eDataType, initialValue);
			case BlipDSLPackage.SERVICE_EXECUTION_MODE_ENUM:
				return createServiceExecutionModeEnumFromString(eDataType, initialValue);
			case BlipDSLPackage.ICONS_ENUM:
				return createIconsEnumFromString(eDataType, initialValue);
			case BlipDSLPackage.INTERNAL_EOBJECT:
				return createInternalEObjectFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case BlipDSLPackage.END_EVENT_HANDLING_ENUM:
				return convertEndEventHandlingEnumToString(eDataType, instanceValue);
			case BlipDSLPackage.SERVICE_EXECUTION_MODE_ENUM:
				return convertServiceExecutionModeEnumToString(eDataType, instanceValue);
			case BlipDSLPackage.ICONS_ENUM:
				return convertIconsEnumToString(eDataType, instanceValue);
			case BlipDSLPackage.INTERNAL_EOBJECT:
				return convertInternalEObjectToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlipModel createBlipModel() {
		BlipModelImpl blipModel = new BlipModelImpl();
		return blipModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlipLazyResolver createBlipLazyResolver() {
		BlipLazyResolverImpl blipLazyResolver = new BlipLazyResolverImpl();
		return blipLazyResolver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlipPackage createBlipPackage() {
		BlipPackageImpl blipPackage = new BlipPackageImpl();
		return blipPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlipBase createBlipBase() {
		BlipBaseImpl blipBase = new BlipBaseImpl();
		return blipBase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Blip createBlip() {
		BlipImpl blip = new BlipImpl();
		return blip;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlipWorkload createBlipWorkload() {
		BlipWorkloadImpl blipWorkload = new BlipWorkloadImpl();
		return blipWorkload;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlipFilter createBlipFilter() {
		BlipFilterImpl blipFilter = new BlipFilterImpl();
		return blipFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlipDto createBlipDto() {
		BlipDtoImpl blipDto = new BlipDtoImpl();
		return blipDto;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlipDtoPath createBlipDtoPath() {
		BlipDtoPathImpl blipDtoPath = new BlipDtoPathImpl();
		return blipDtoPath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlipItem createBlipItem() {
		BlipItemImpl blipItem = new BlipItemImpl();
		return blipItem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlipEvent createBlipEvent() {
		BlipEventImpl blipEvent = new BlipEventImpl();
		return blipEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlipStartEvent createBlipStartEvent() {
		BlipStartEventImpl blipStartEvent = new BlipStartEventImpl();
		return blipStartEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlipEndEvent createBlipEndEvent() {
		BlipEndEventImpl blipEndEvent = new BlipEndEventImpl();
		return blipEndEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlipUserTask createBlipUserTask() {
		BlipUserTaskImpl blipUserTask = new BlipUserTaskImpl();
		return blipUserTask;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlipCallActivity createBlipCallActivity() {
		BlipCallActivityImpl blipCallActivity = new BlipCallActivityImpl();
		return blipCallActivity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlipScript createBlipScript() {
		BlipScriptImpl blipScript = new BlipScriptImpl();
		return blipScript;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlipScriptTask createBlipScriptTask() {
		BlipScriptTaskImpl blipScriptTask = new BlipScriptTaskImpl();
		return blipScriptTask;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlipPersistTask createBlipPersistTask() {
		BlipPersistTaskImpl blipPersistTask = new BlipPersistTaskImpl();
		return blipPersistTask;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlipServiceTask createBlipServiceTask() {
		BlipServiceTaskImpl blipServiceTask = new BlipServiceTaskImpl();
		return blipServiceTask;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlipOutGoing createBlipOutGoing() {
		BlipOutGoingImpl blipOutGoing = new BlipOutGoingImpl();
		return blipOutGoing;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlipOutGoingDefault createBlipOutGoingDefault() {
		BlipOutGoingDefaultImpl blipOutGoingDefault = new BlipOutGoingDefaultImpl();
		return blipOutGoingDefault;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlipSplitGateway createBlipSplitGateway() {
		BlipSplitGatewayImpl blipSplitGateway = new BlipSplitGatewayImpl();
		return blipSplitGateway;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlipExclusiveSplitGateway createBlipExclusiveSplitGateway() {
		BlipExclusiveSplitGatewayImpl blipExclusiveSplitGateway = new BlipExclusiveSplitGatewayImpl();
		return blipExclusiveSplitGateway;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlipInclusiveSplitGateway createBlipInclusiveSplitGateway() {
		BlipInclusiveSplitGatewayImpl blipInclusiveSplitGateway = new BlipInclusiveSplitGatewayImpl();
		return blipInclusiveSplitGateway;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EndEventHandlingEnum createEndEventHandlingEnumFromString(EDataType eDataType, String initialValue) {
		EndEventHandlingEnum result = EndEventHandlingEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEndEventHandlingEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceExecutionModeEnum createServiceExecutionModeEnumFromString(EDataType eDataType, String initialValue) {
		ServiceExecutionModeEnum result = ServiceExecutionModeEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertServiceExecutionModeEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IconsEnum createIconsEnumFromString(EDataType eDataType, String initialValue) {
		IconsEnum result = IconsEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIconsEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InternalEObject createInternalEObjectFromString(EDataType eDataType, String initialValue) {
		return (InternalEObject)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertInternalEObjectToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlipDSLPackage getBlipDSLPackage() {
		return (BlipDSLPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static BlipDSLPackage getPackage() {
		return BlipDSLPackage.eINSTANCE;
	}

} //BlipDSLFactoryImpl
