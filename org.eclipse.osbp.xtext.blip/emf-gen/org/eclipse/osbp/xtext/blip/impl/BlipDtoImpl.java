/**
 * Copyright (c) 2014, Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Loetz GmbH&Co.KG - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.blip.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.dsl.semantic.dto.LDto;

import org.eclipse.osbp.xtext.blip.BlipDSLPackage;
import org.eclipse.osbp.xtext.blip.BlipDto;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Blip Dto</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.blip.impl.BlipDtoImpl#getDtoRef <em>Dto Ref</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BlipDtoImpl extends BlipLazyResolverImpl implements BlipDto {
	/**
	 * The cached value of the '{@link #getDtoRef() <em>Dto Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDtoRef()
	 * @generated
	 * @ordered
	 */
	protected LDto dtoRef;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BlipDtoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BlipDSLPackage.Literals.BLIP_DTO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDto getDtoRef() {
		if (dtoRef != null && dtoRef.eIsProxy()) {
			InternalEObject oldDtoRef = (InternalEObject)dtoRef;
			dtoRef = (LDto)eResolveProxy(oldDtoRef);
			if (dtoRef != oldDtoRef) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BlipDSLPackage.BLIP_DTO__DTO_REF, oldDtoRef, dtoRef));
			}
		}
		return dtoRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDto basicGetDtoRef() {
		return dtoRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDtoRef(LDto newDtoRef) {
		LDto oldDtoRef = dtoRef;
		dtoRef = newDtoRef;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlipDSLPackage.BLIP_DTO__DTO_REF, oldDtoRef, dtoRef));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BlipDSLPackage.BLIP_DTO__DTO_REF:
				if (resolve) return getDtoRef();
				return basicGetDtoRef();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BlipDSLPackage.BLIP_DTO__DTO_REF:
				setDtoRef((LDto)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BlipDSLPackage.BLIP_DTO__DTO_REF:
				setDtoRef((LDto)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BlipDSLPackage.BLIP_DTO__DTO_REF:
				return dtoRef != null;
		}
		return super.eIsSet(featureID);
	}

} //BlipDtoImpl
