/**
 * Copyright (c) 2014, Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Loetz GmbH&Co.KG - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.blip.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.osbp.xtext.blip.BlipDSLPackage;
import org.eclipse.osbp.xtext.blip.BlipExclusiveSplitGateway;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Blip Exclusive Split Gateway</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BlipExclusiveSplitGatewayImpl extends BlipSplitGatewayImpl implements BlipExclusiveSplitGateway {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BlipExclusiveSplitGatewayImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BlipDSLPackage.Literals.BLIP_EXCLUSIVE_SPLIT_GATEWAY;
	}

} //BlipExclusiveSplitGatewayImpl
