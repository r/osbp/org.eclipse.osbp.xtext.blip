/**
 * Copyright (c) 2014, Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Loetz GmbH&Co.KG - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.blip;

import org.eclipse.bpmn2.ServiceTask;

import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryFunction;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Blip Service Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.blip.BlipServiceTask#getTask <em>Task</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.blip.BlipServiceTask#getOnEntry <em>On Entry</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.blip.BlipServiceTask#getOnExit <em>On Exit</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.blip.BlipServiceTask#getExecutionMode <em>Execution Mode</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.blip.BlipServiceTask#getTimeoutInSecs <em>Timeout In Secs</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.blip.BlipServiceTask#getFunction <em>Function</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.blip.BlipServiceTask#getWebServiceInterface <em>Web Service Interface</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.blip.BlipServiceTask#getWebServiceOperation <em>Web Service Operation</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipServiceTask()
 * @model
 * @generated
 */
public interface BlipServiceTask extends BlipItem {
	/**
	 * Returns the value of the '<em><b>Task</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task</em>' reference.
	 * @see #setTask(ServiceTask)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipServiceTask_Task()
	 * @model
	 * @generated
	 */
	ServiceTask getTask();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.BlipServiceTask#getTask <em>Task</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task</em>' reference.
	 * @see #getTask()
	 * @generated
	 */
	void setTask(ServiceTask value);

	/**
	 * Returns the value of the '<em><b>On Entry</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>On Entry</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>On Entry</em>' reference.
	 * @see #setOnEntry(FunctionLibraryFunction)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipServiceTask_OnEntry()
	 * @model
	 * @generated
	 */
	FunctionLibraryFunction getOnEntry();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.BlipServiceTask#getOnEntry <em>On Entry</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>On Entry</em>' reference.
	 * @see #getOnEntry()
	 * @generated
	 */
	void setOnEntry(FunctionLibraryFunction value);

	/**
	 * Returns the value of the '<em><b>On Exit</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>On Exit</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>On Exit</em>' reference.
	 * @see #setOnExit(FunctionLibraryFunction)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipServiceTask_OnExit()
	 * @model
	 * @generated
	 */
	FunctionLibraryFunction getOnExit();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.BlipServiceTask#getOnExit <em>On Exit</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>On Exit</em>' reference.
	 * @see #getOnExit()
	 * @generated
	 */
	void setOnExit(FunctionLibraryFunction value);

	/**
	 * Returns the value of the '<em><b>Execution Mode</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.xtext.blip.ServiceExecutionModeEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Execution Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Execution Mode</em>' attribute.
	 * @see org.eclipse.osbp.xtext.blip.ServiceExecutionModeEnum
	 * @see #setExecutionMode(ServiceExecutionModeEnum)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipServiceTask_ExecutionMode()
	 * @model unique="false"
	 * @generated
	 */
	ServiceExecutionModeEnum getExecutionMode();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.BlipServiceTask#getExecutionMode <em>Execution Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Execution Mode</em>' attribute.
	 * @see org.eclipse.osbp.xtext.blip.ServiceExecutionModeEnum
	 * @see #getExecutionMode()
	 * @generated
	 */
	void setExecutionMode(ServiceExecutionModeEnum value);

	/**
	 * Returns the value of the '<em><b>Timeout In Secs</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timeout In Secs</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timeout In Secs</em>' attribute.
	 * @see #setTimeoutInSecs(int)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipServiceTask_TimeoutInSecs()
	 * @model unique="false"
	 * @generated
	 */
	int getTimeoutInSecs();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.BlipServiceTask#getTimeoutInSecs <em>Timeout In Secs</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timeout In Secs</em>' attribute.
	 * @see #getTimeoutInSecs()
	 * @generated
	 */
	void setTimeoutInSecs(int value);

	/**
	 * Returns the value of the '<em><b>Function</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function</em>' reference.
	 * @see #setFunction(FunctionLibraryFunction)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipServiceTask_Function()
	 * @model
	 * @generated
	 */
	FunctionLibraryFunction getFunction();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.BlipServiceTask#getFunction <em>Function</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Function</em>' reference.
	 * @see #getFunction()
	 * @generated
	 */
	void setFunction(FunctionLibraryFunction value);

	/**
	 * Returns the value of the '<em><b>Web Service Interface</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Web Service Interface</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Web Service Interface</em>' attribute.
	 * @see #setWebServiceInterface(String)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipServiceTask_WebServiceInterface()
	 * @model unique="false"
	 * @generated
	 */
	String getWebServiceInterface();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.BlipServiceTask#getWebServiceInterface <em>Web Service Interface</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Web Service Interface</em>' attribute.
	 * @see #getWebServiceInterface()
	 * @generated
	 */
	void setWebServiceInterface(String value);

	/**
	 * Returns the value of the '<em><b>Web Service Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Web Service Operation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Web Service Operation</em>' attribute.
	 * @see #setWebServiceOperation(String)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipServiceTask_WebServiceOperation()
	 * @model unique="false"
	 * @generated
	 */
	String getWebServiceOperation();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.BlipServiceTask#getWebServiceOperation <em>Web Service Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Web Service Operation</em>' attribute.
	 * @see #getWebServiceOperation()
	 * @generated
	 */
	void setWebServiceOperation(String value);

} // BlipServiceTask
