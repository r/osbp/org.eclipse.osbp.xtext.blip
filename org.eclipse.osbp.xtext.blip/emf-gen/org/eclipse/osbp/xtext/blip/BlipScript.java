/**
 * Copyright (c) 2014, Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Loetz GmbH&Co.KG - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.blip;

import org.eclipse.bpmn2.ScriptTask;

import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryFunction;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Blip Script</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.blip.BlipScript#getTask <em>Task</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.blip.BlipScript#getFunction <em>Function</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipScript()
 * @model
 * @generated
 */
public interface BlipScript extends BlipItem {
	/**
	 * Returns the value of the '<em><b>Task</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task</em>' reference.
	 * @see #setTask(ScriptTask)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipScript_Task()
	 * @model
	 * @generated
	 */
	ScriptTask getTask();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.BlipScript#getTask <em>Task</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task</em>' reference.
	 * @see #getTask()
	 * @generated
	 */
	void setTask(ScriptTask value);

	/**
	 * Returns the value of the '<em><b>Function</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function</em>' reference.
	 * @see #setFunction(FunctionLibraryFunction)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipScript_Function()
	 * @model
	 * @generated
	 */
	FunctionLibraryFunction getFunction();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.BlipScript#getFunction <em>Function</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Function</em>' reference.
	 * @see #getFunction()
	 * @generated
	 */
	void setFunction(FunctionLibraryFunction value);

} // BlipScript
