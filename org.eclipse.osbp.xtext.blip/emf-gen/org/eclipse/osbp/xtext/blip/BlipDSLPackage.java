/**
 * Copyright (c) 2014, Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Loetz GmbH&Co.KG - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.blip;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.blip.BlipDSLFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel fileExtensions='blip' modelName='BlipDSL' prefix='BlipDSL' updateClasspath='false' loadInitialization='false' literalsInterface='true' copyrightText='Copyright (c) 2014, Loetz GmbH&Co.KG (Heidelberg)\r\n All rights reserved. This program and the accompanying materials \r\n are made available under the terms of the Eclipse Public License 2.0  \r\n which accompanies this distribution, and is available at \r\n https://www.eclipse.org/legal/epl-2.0/ \r\n \r\n SPDX-License-Identifier: EPL-2.0 \r\n\r\n Based on ideas from Xtext, Xtend, Xcore\r\n  \r\n Contributors:  \r\n \t\tLoetz GmbH&Co.KG - Initial implementation \r\n ' basePackage='org.eclipse.osbp.xtext'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore rootPackage='blipdsl'"
 * @generated
 */
public interface BlipDSLPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "blip";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://osbp.eclipse.org/xtext/blip/BlipDSL";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "blipdsl";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BlipDSLPackage eINSTANCE = org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipModelImpl <em>Blip Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipModelImpl
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipModel()
	 * @generated
	 */
	int BLIP_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Import Section</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_MODEL__IMPORT_SECTION = 0;

	/**
	 * The feature id for the '<em><b>Blippackage</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_MODEL__BLIPPACKAGE = 1;

	/**
	 * The number of structural features of the '<em>Blip Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_MODEL_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Blip Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipLazyResolverImpl <em>Blip Lazy Resolver</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipLazyResolverImpl
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipLazyResolver()
	 * @generated
	 */
	int BLIP_LAZY_RESOLVER = 1;

	/**
	 * The number of structural features of the '<em>Blip Lazy Resolver</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_LAZY_RESOLVER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT = 0;

	/**
	 * The number of operations of the '<em>Blip Lazy Resolver</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_LAZY_RESOLVER_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipPackageImpl <em>Blip Package</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipPackageImpl
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipPackage()
	 * @generated
	 */
	int BLIP_PACKAGE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_PACKAGE__NAME = OSBPTypesPackage.LPACKAGE__NAME;

	/**
	 * The feature id for the '<em><b>Blips</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_PACKAGE__BLIPS = OSBPTypesPackage.LPACKAGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Blip Package</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_PACKAGE_FEATURE_COUNT = OSBPTypesPackage.LPACKAGE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_PACKAGE___ERESOLVE_PROXY__INTERNALEOBJECT = OSBPTypesPackage.LPACKAGE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Blip Package</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_PACKAGE_OPERATION_COUNT = OSBPTypesPackage.LPACKAGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipBaseImpl <em>Blip Base</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipBaseImpl
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipBase()
	 * @generated
	 */
	int BLIP_BASE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_BASE__NAME = BLIP_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Blip Base</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_BASE_FEATURE_COUNT = BLIP_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_BASE___ERESOLVE_PROXY__INTERNALEOBJECT = BLIP_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Blip Base</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_BASE_OPERATION_COUNT = BLIP_LAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipImpl <em>Blip</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipImpl
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlip()
	 * @generated
	 */
	int BLIP = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP__NAME = BLIP_BASE__NAME;

	/**
	 * The feature id for the '<em><b>Process</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP__PROCESS = BLIP_BASE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP__DESCRIPTION = BLIP_BASE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Description Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP__DESCRIPTION_VALUE = BLIP_BASE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Has Image</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP__HAS_IMAGE = BLIP_BASE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Image</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP__IMAGE = BLIP_BASE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Has Icon</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP__HAS_ICON = BLIP_BASE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Icon</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP__ICON = BLIP_BASE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Has Logging</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP__HAS_LOGGING = BLIP_BASE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Function Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP__FUNCTION_GROUP = BLIP_BASE_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Workload</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP__WORKLOAD = BLIP_BASE_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Items</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP__ITEMS = BLIP_BASE_FEATURE_COUNT + 10;

	/**
	 * The number of structural features of the '<em>Blip</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_FEATURE_COUNT = BLIP_BASE_FEATURE_COUNT + 11;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP___ERESOLVE_PROXY__INTERNALEOBJECT = BLIP_BASE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Blip</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_OPERATION_COUNT = BLIP_BASE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipWorkloadImpl <em>Blip Workload</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipWorkloadImpl
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipWorkload()
	 * @generated
	 */
	int BLIP_WORKLOAD = 5;

	/**
	 * The feature id for the '<em><b>Source</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_WORKLOAD__SOURCE = BLIP_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Filters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_WORKLOAD__FILTERS = BLIP_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Blip Workload</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_WORKLOAD_FEATURE_COUNT = BLIP_LAZY_RESOLVER_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_WORKLOAD___ERESOLVE_PROXY__INTERNALEOBJECT = BLIP_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Blip Workload</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_WORKLOAD_OPERATION_COUNT = BLIP_LAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipFilterImpl <em>Blip Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipFilterImpl
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipFilter()
	 * @generated
	 */
	int BLIP_FILTER = 6;

	/**
	 * The feature id for the '<em><b>Filter Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_FILTER__FILTER_NAME = BLIP_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Filter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_FILTER__FILTER_VALUE = BLIP_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Blip Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_FILTER_FEATURE_COUNT = BLIP_LAZY_RESOLVER_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_FILTER___ERESOLVE_PROXY__INTERNALEOBJECT = BLIP_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Blip Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_FILTER_OPERATION_COUNT = BLIP_LAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipDtoImpl <em>Blip Dto</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipDtoImpl
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipDto()
	 * @generated
	 */
	int BLIP_DTO = 7;

	/**
	 * The feature id for the '<em><b>Dto Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_DTO__DTO_REF = BLIP_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Blip Dto</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_DTO_FEATURE_COUNT = BLIP_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_DTO___ERESOLVE_PROXY__INTERNALEOBJECT = BLIP_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Blip Dto</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_DTO_OPERATION_COUNT = BLIP_LAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipDtoPathImpl <em>Blip Dto Path</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipDtoPathImpl
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipDtoPath()
	 * @generated
	 */
	int BLIP_DTO_PATH = 8;

	/**
	 * The feature id for the '<em><b>Dto Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_DTO_PATH__DTO_PATH = 0;

	/**
	 * The number of structural features of the '<em>Blip Dto Path</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_DTO_PATH_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Blip Dto Path</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_DTO_PATH_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipItemImpl <em>Blip Item</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipItemImpl
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipItem()
	 * @generated
	 */
	int BLIP_ITEM = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_ITEM__NAME = 0;

	/**
	 * The feature id for the '<em><b>Dto Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_ITEM__DTO_PATH = 1;

	/**
	 * The number of structural features of the '<em>Blip Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_ITEM_FEATURE_COUNT = 2;

	/**
	 * The operation id for the '<em>Get Operative LDto</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_ITEM___GET_OPERATIVE_LDTO = 0;

	/**
	 * The number of operations of the '<em>Blip Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_ITEM_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipEventImpl <em>Blip Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipEventImpl
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipEvent()
	 * @generated
	 */
	int BLIP_EVENT = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_EVENT__NAME = BLIP_ITEM__NAME;

	/**
	 * The feature id for the '<em><b>Dto Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_EVENT__DTO_PATH = BLIP_ITEM__DTO_PATH;

	/**
	 * The feature id for the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_EVENT__EVENT = BLIP_ITEM_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Blip Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_EVENT_FEATURE_COUNT = BLIP_ITEM_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Operative LDto</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_EVENT___GET_OPERATIVE_LDTO = BLIP_ITEM___GET_OPERATIVE_LDTO;

	/**
	 * The number of operations of the '<em>Blip Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_EVENT_OPERATION_COUNT = BLIP_ITEM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipStartEventImpl <em>Blip Start Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipStartEventImpl
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipStartEvent()
	 * @generated
	 */
	int BLIP_START_EVENT = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_START_EVENT__NAME = BLIP_EVENT__NAME;

	/**
	 * The feature id for the '<em><b>Dto Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_START_EVENT__DTO_PATH = BLIP_EVENT__DTO_PATH;

	/**
	 * The feature id for the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_START_EVENT__EVENT = BLIP_EVENT__EVENT;

	/**
	 * The number of structural features of the '<em>Blip Start Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_START_EVENT_FEATURE_COUNT = BLIP_EVENT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Operative LDto</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_START_EVENT___GET_OPERATIVE_LDTO = BLIP_EVENT___GET_OPERATIVE_LDTO;

	/**
	 * The number of operations of the '<em>Blip Start Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_START_EVENT_OPERATION_COUNT = BLIP_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipEndEventImpl <em>Blip End Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipEndEventImpl
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipEndEvent()
	 * @generated
	 */
	int BLIP_END_EVENT = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_END_EVENT__NAME = BLIP_EVENT__NAME;

	/**
	 * The feature id for the '<em><b>Dto Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_END_EVENT__DTO_PATH = BLIP_EVENT__DTO_PATH;

	/**
	 * The feature id for the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_END_EVENT__EVENT = BLIP_EVENT__EVENT;

	/**
	 * The feature id for the '<em><b>End Handling</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_END_EVENT__END_HANDLING = BLIP_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Blip End Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_END_EVENT_FEATURE_COUNT = BLIP_EVENT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Operative LDto</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_END_EVENT___GET_OPERATIVE_LDTO = BLIP_EVENT___GET_OPERATIVE_LDTO;

	/**
	 * The number of operations of the '<em>Blip End Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_END_EVENT_OPERATION_COUNT = BLIP_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipUserTaskImpl <em>Blip User Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipUserTaskImpl
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipUserTask()
	 * @generated
	 */
	int BLIP_USER_TASK = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_USER_TASK__NAME = BLIP_ITEM__NAME;

	/**
	 * The feature id for the '<em><b>Dto Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_USER_TASK__DTO_PATH = BLIP_ITEM__DTO_PATH;

	/**
	 * The feature id for the '<em><b>Task</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_USER_TASK__TASK = BLIP_ITEM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>On Entry</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_USER_TASK__ON_ENTRY = BLIP_ITEM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>On Exit</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_USER_TASK__ON_EXIT = BLIP_ITEM_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Blip User Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_USER_TASK_FEATURE_COUNT = BLIP_ITEM_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Operative LDto</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_USER_TASK___GET_OPERATIVE_LDTO = BLIP_ITEM___GET_OPERATIVE_LDTO;

	/**
	 * The number of operations of the '<em>Blip User Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_USER_TASK_OPERATION_COUNT = BLIP_ITEM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipCallActivityImpl <em>Blip Call Activity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipCallActivityImpl
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipCallActivity()
	 * @generated
	 */
	int BLIP_CALL_ACTIVITY = 14;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_CALL_ACTIVITY__NAME = BLIP_ITEM__NAME;

	/**
	 * The feature id for the '<em><b>Dto Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_CALL_ACTIVITY__DTO_PATH = BLIP_ITEM__DTO_PATH;

	/**
	 * The feature id for the '<em><b>Call Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_CALL_ACTIVITY__CALL_ACTIVITY = BLIP_ITEM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>On Entry</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_CALL_ACTIVITY__ON_ENTRY = BLIP_ITEM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>On Exit</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_CALL_ACTIVITY__ON_EXIT = BLIP_ITEM_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Blip Call Activity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_CALL_ACTIVITY_FEATURE_COUNT = BLIP_ITEM_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Operative LDto</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_CALL_ACTIVITY___GET_OPERATIVE_LDTO = BLIP_ITEM___GET_OPERATIVE_LDTO;

	/**
	 * The number of operations of the '<em>Blip Call Activity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_CALL_ACTIVITY_OPERATION_COUNT = BLIP_ITEM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipScriptImpl <em>Blip Script</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipScriptImpl
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipScript()
	 * @generated
	 */
	int BLIP_SCRIPT = 15;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SCRIPT__NAME = BLIP_ITEM__NAME;

	/**
	 * The feature id for the '<em><b>Dto Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SCRIPT__DTO_PATH = BLIP_ITEM__DTO_PATH;

	/**
	 * The feature id for the '<em><b>Task</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SCRIPT__TASK = BLIP_ITEM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Function</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SCRIPT__FUNCTION = BLIP_ITEM_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Blip Script</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SCRIPT_FEATURE_COUNT = BLIP_ITEM_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Operative LDto</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SCRIPT___GET_OPERATIVE_LDTO = BLIP_ITEM___GET_OPERATIVE_LDTO;

	/**
	 * The number of operations of the '<em>Blip Script</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SCRIPT_OPERATION_COUNT = BLIP_ITEM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipScriptTaskImpl <em>Blip Script Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipScriptTaskImpl
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipScriptTask()
	 * @generated
	 */
	int BLIP_SCRIPT_TASK = 16;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SCRIPT_TASK__NAME = BLIP_SCRIPT__NAME;

	/**
	 * The feature id for the '<em><b>Dto Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SCRIPT_TASK__DTO_PATH = BLIP_SCRIPT__DTO_PATH;

	/**
	 * The feature id for the '<em><b>Task</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SCRIPT_TASK__TASK = BLIP_SCRIPT__TASK;

	/**
	 * The feature id for the '<em><b>Function</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SCRIPT_TASK__FUNCTION = BLIP_SCRIPT__FUNCTION;

	/**
	 * The number of structural features of the '<em>Blip Script Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SCRIPT_TASK_FEATURE_COUNT = BLIP_SCRIPT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Operative LDto</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SCRIPT_TASK___GET_OPERATIVE_LDTO = BLIP_SCRIPT___GET_OPERATIVE_LDTO;

	/**
	 * The number of operations of the '<em>Blip Script Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SCRIPT_TASK_OPERATION_COUNT = BLIP_SCRIPT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipPersistTaskImpl <em>Blip Persist Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipPersistTaskImpl
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipPersistTask()
	 * @generated
	 */
	int BLIP_PERSIST_TASK = 17;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_PERSIST_TASK__NAME = BLIP_SCRIPT__NAME;

	/**
	 * The feature id for the '<em><b>Dto Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_PERSIST_TASK__DTO_PATH = BLIP_SCRIPT__DTO_PATH;

	/**
	 * The feature id for the '<em><b>Task</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_PERSIST_TASK__TASK = BLIP_SCRIPT__TASK;

	/**
	 * The feature id for the '<em><b>Function</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_PERSIST_TASK__FUNCTION = BLIP_SCRIPT__FUNCTION;

	/**
	 * The number of structural features of the '<em>Blip Persist Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_PERSIST_TASK_FEATURE_COUNT = BLIP_SCRIPT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Operative LDto</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_PERSIST_TASK___GET_OPERATIVE_LDTO = BLIP_SCRIPT___GET_OPERATIVE_LDTO;

	/**
	 * The number of operations of the '<em>Blip Persist Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_PERSIST_TASK_OPERATION_COUNT = BLIP_SCRIPT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipServiceTaskImpl <em>Blip Service Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipServiceTaskImpl
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipServiceTask()
	 * @generated
	 */
	int BLIP_SERVICE_TASK = 18;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SERVICE_TASK__NAME = BLIP_ITEM__NAME;

	/**
	 * The feature id for the '<em><b>Dto Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SERVICE_TASK__DTO_PATH = BLIP_ITEM__DTO_PATH;

	/**
	 * The feature id for the '<em><b>Task</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SERVICE_TASK__TASK = BLIP_ITEM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>On Entry</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SERVICE_TASK__ON_ENTRY = BLIP_ITEM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>On Exit</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SERVICE_TASK__ON_EXIT = BLIP_ITEM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Execution Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SERVICE_TASK__EXECUTION_MODE = BLIP_ITEM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Timeout In Secs</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SERVICE_TASK__TIMEOUT_IN_SECS = BLIP_ITEM_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Function</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SERVICE_TASK__FUNCTION = BLIP_ITEM_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Web Service Interface</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SERVICE_TASK__WEB_SERVICE_INTERFACE = BLIP_ITEM_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Web Service Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SERVICE_TASK__WEB_SERVICE_OPERATION = BLIP_ITEM_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Blip Service Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SERVICE_TASK_FEATURE_COUNT = BLIP_ITEM_FEATURE_COUNT + 8;

	/**
	 * The operation id for the '<em>Get Operative LDto</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SERVICE_TASK___GET_OPERATIVE_LDTO = BLIP_ITEM___GET_OPERATIVE_LDTO;

	/**
	 * The number of operations of the '<em>Blip Service Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SERVICE_TASK_OPERATION_COUNT = BLIP_ITEM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipOutGoingImpl <em>Blip Out Going</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipOutGoingImpl
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipOutGoing()
	 * @generated
	 */
	int BLIP_OUT_GOING = 19;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_OUT_GOING__NAME = BLIP_ITEM__NAME;

	/**
	 * The feature id for the '<em><b>Dto Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_OUT_GOING__DTO_PATH = BLIP_ITEM__DTO_PATH;

	/**
	 * The feature id for the '<em><b>Sequence Flow</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_OUT_GOING__SEQUENCE_FLOW = BLIP_ITEM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_OUT_GOING__CONSTRAINT = BLIP_ITEM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Is Default</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_OUT_GOING__IS_DEFAULT = BLIP_ITEM_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Blip Out Going</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_OUT_GOING_FEATURE_COUNT = BLIP_ITEM_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Operative LDto</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_OUT_GOING___GET_OPERATIVE_LDTO = BLIP_ITEM___GET_OPERATIVE_LDTO;

	/**
	 * The number of operations of the '<em>Blip Out Going</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_OUT_GOING_OPERATION_COUNT = BLIP_ITEM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipOutGoingDefaultImpl <em>Blip Out Going Default</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipOutGoingDefaultImpl
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipOutGoingDefault()
	 * @generated
	 */
	int BLIP_OUT_GOING_DEFAULT = 20;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_OUT_GOING_DEFAULT__NAME = BLIP_OUT_GOING__NAME;

	/**
	 * The feature id for the '<em><b>Dto Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_OUT_GOING_DEFAULT__DTO_PATH = BLIP_OUT_GOING__DTO_PATH;

	/**
	 * The feature id for the '<em><b>Sequence Flow</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_OUT_GOING_DEFAULT__SEQUENCE_FLOW = BLIP_OUT_GOING__SEQUENCE_FLOW;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_OUT_GOING_DEFAULT__CONSTRAINT = BLIP_OUT_GOING__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Is Default</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_OUT_GOING_DEFAULT__IS_DEFAULT = BLIP_OUT_GOING__IS_DEFAULT;

	/**
	 * The number of structural features of the '<em>Blip Out Going Default</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_OUT_GOING_DEFAULT_FEATURE_COUNT = BLIP_OUT_GOING_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Operative LDto</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_OUT_GOING_DEFAULT___GET_OPERATIVE_LDTO = BLIP_OUT_GOING___GET_OPERATIVE_LDTO;

	/**
	 * The number of operations of the '<em>Blip Out Going Default</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_OUT_GOING_DEFAULT_OPERATION_COUNT = BLIP_OUT_GOING_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipSplitGatewayImpl <em>Blip Split Gateway</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipSplitGatewayImpl
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipSplitGateway()
	 * @generated
	 */
	int BLIP_SPLIT_GATEWAY = 21;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SPLIT_GATEWAY__NAME = BLIP_ITEM__NAME;

	/**
	 * The feature id for the '<em><b>Dto Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SPLIT_GATEWAY__DTO_PATH = BLIP_ITEM__DTO_PATH;

	/**
	 * The feature id for the '<em><b>Gateway</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SPLIT_GATEWAY__GATEWAY = BLIP_ITEM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Outgoings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SPLIT_GATEWAY__OUTGOINGS = BLIP_ITEM_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Blip Split Gateway</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SPLIT_GATEWAY_FEATURE_COUNT = BLIP_ITEM_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Operative LDto</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SPLIT_GATEWAY___GET_OPERATIVE_LDTO = BLIP_ITEM___GET_OPERATIVE_LDTO;

	/**
	 * The number of operations of the '<em>Blip Split Gateway</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_SPLIT_GATEWAY_OPERATION_COUNT = BLIP_ITEM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipExclusiveSplitGatewayImpl <em>Blip Exclusive Split Gateway</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipExclusiveSplitGatewayImpl
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipExclusiveSplitGateway()
	 * @generated
	 */
	int BLIP_EXCLUSIVE_SPLIT_GATEWAY = 22;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_EXCLUSIVE_SPLIT_GATEWAY__NAME = BLIP_SPLIT_GATEWAY__NAME;

	/**
	 * The feature id for the '<em><b>Dto Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_EXCLUSIVE_SPLIT_GATEWAY__DTO_PATH = BLIP_SPLIT_GATEWAY__DTO_PATH;

	/**
	 * The feature id for the '<em><b>Gateway</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_EXCLUSIVE_SPLIT_GATEWAY__GATEWAY = BLIP_SPLIT_GATEWAY__GATEWAY;

	/**
	 * The feature id for the '<em><b>Outgoings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_EXCLUSIVE_SPLIT_GATEWAY__OUTGOINGS = BLIP_SPLIT_GATEWAY__OUTGOINGS;

	/**
	 * The number of structural features of the '<em>Blip Exclusive Split Gateway</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_EXCLUSIVE_SPLIT_GATEWAY_FEATURE_COUNT = BLIP_SPLIT_GATEWAY_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Operative LDto</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_EXCLUSIVE_SPLIT_GATEWAY___GET_OPERATIVE_LDTO = BLIP_SPLIT_GATEWAY___GET_OPERATIVE_LDTO;

	/**
	 * The number of operations of the '<em>Blip Exclusive Split Gateway</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_EXCLUSIVE_SPLIT_GATEWAY_OPERATION_COUNT = BLIP_SPLIT_GATEWAY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipInclusiveSplitGatewayImpl <em>Blip Inclusive Split Gateway</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipInclusiveSplitGatewayImpl
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipInclusiveSplitGateway()
	 * @generated
	 */
	int BLIP_INCLUSIVE_SPLIT_GATEWAY = 23;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_INCLUSIVE_SPLIT_GATEWAY__NAME = BLIP_SPLIT_GATEWAY__NAME;

	/**
	 * The feature id for the '<em><b>Dto Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_INCLUSIVE_SPLIT_GATEWAY__DTO_PATH = BLIP_SPLIT_GATEWAY__DTO_PATH;

	/**
	 * The feature id for the '<em><b>Gateway</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_INCLUSIVE_SPLIT_GATEWAY__GATEWAY = BLIP_SPLIT_GATEWAY__GATEWAY;

	/**
	 * The feature id for the '<em><b>Outgoings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_INCLUSIVE_SPLIT_GATEWAY__OUTGOINGS = BLIP_SPLIT_GATEWAY__OUTGOINGS;

	/**
	 * The number of structural features of the '<em>Blip Inclusive Split Gateway</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_INCLUSIVE_SPLIT_GATEWAY_FEATURE_COUNT = BLIP_SPLIT_GATEWAY_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Operative LDto</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_INCLUSIVE_SPLIT_GATEWAY___GET_OPERATIVE_LDTO = BLIP_SPLIT_GATEWAY___GET_OPERATIVE_LDTO;

	/**
	 * The number of operations of the '<em>Blip Inclusive Split Gateway</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLIP_INCLUSIVE_SPLIT_GATEWAY_OPERATION_COUNT = BLIP_SPLIT_GATEWAY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.blip.EndEventHandlingEnum <em>End Event Handling Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.blip.EndEventHandlingEnum
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getEndEventHandlingEnum()
	 * @generated
	 */
	int END_EVENT_HANDLING_ENUM = 24;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.blip.ServiceExecutionModeEnum <em>Service Execution Mode Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.blip.ServiceExecutionModeEnum
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getServiceExecutionModeEnum()
	 * @generated
	 */
	int SERVICE_EXECUTION_MODE_ENUM = 25;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.blip.IconsEnum <em>Icons Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.blip.IconsEnum
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getIconsEnum()
	 * @generated
	 */
	int ICONS_ENUM = 26;

	/**
	 * The meta object id for the '<em>Internal EObject</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.InternalEObject
	 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getInternalEObject()
	 * @generated
	 */
	int INTERNAL_EOBJECT = 27;


	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.blip.BlipModel <em>Blip Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Blip Model</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipModel
	 * @generated
	 */
	EClass getBlipModel();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.blip.BlipModel#getImportSection <em>Import Section</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Import Section</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipModel#getImportSection()
	 * @see #getBlipModel()
	 * @generated
	 */
	EReference getBlipModel_ImportSection();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.blip.BlipModel#getBlippackage <em>Blippackage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Blippackage</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipModel#getBlippackage()
	 * @see #getBlipModel()
	 * @generated
	 */
	EReference getBlipModel_Blippackage();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.blip.BlipLazyResolver <em>Blip Lazy Resolver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Blip Lazy Resolver</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipLazyResolver
	 * @generated
	 */
	EClass getBlipLazyResolver();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.xtext.blip.BlipLazyResolver#eResolveProxy(org.eclipse.emf.ecore.InternalEObject) <em>EResolve Proxy</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>EResolve Proxy</em>' operation.
	 * @see org.eclipse.osbp.xtext.blip.BlipLazyResolver#eResolveProxy(org.eclipse.emf.ecore.InternalEObject)
	 * @generated
	 */
	EOperation getBlipLazyResolver__EResolveProxy__InternalEObject();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.blip.BlipPackage <em>Blip Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Blip Package</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipPackage
	 * @generated
	 */
	EClass getBlipPackage();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.blip.BlipPackage#getBlips <em>Blips</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Blips</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipPackage#getBlips()
	 * @see #getBlipPackage()
	 * @generated
	 */
	EReference getBlipPackage_Blips();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.blip.BlipBase <em>Blip Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Blip Base</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipBase
	 * @generated
	 */
	EClass getBlipBase();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.blip.BlipBase#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipBase#getName()
	 * @see #getBlipBase()
	 * @generated
	 */
	EAttribute getBlipBase_Name();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.blip.Blip <em>Blip</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Blip</em>'.
	 * @see org.eclipse.osbp.xtext.blip.Blip
	 * @generated
	 */
	EClass getBlip();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.blip.Blip#getProcess <em>Process</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Process</em>'.
	 * @see org.eclipse.osbp.xtext.blip.Blip#getProcess()
	 * @see #getBlip()
	 * @generated
	 */
	EReference getBlip_Process();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.blip.Blip#isDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see org.eclipse.osbp.xtext.blip.Blip#isDescription()
	 * @see #getBlip()
	 * @generated
	 */
	EAttribute getBlip_Description();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.blip.Blip#getDescriptionValue <em>Description Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description Value</em>'.
	 * @see org.eclipse.osbp.xtext.blip.Blip#getDescriptionValue()
	 * @see #getBlip()
	 * @generated
	 */
	EAttribute getBlip_DescriptionValue();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.blip.Blip#isHasImage <em>Has Image</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Has Image</em>'.
	 * @see org.eclipse.osbp.xtext.blip.Blip#isHasImage()
	 * @see #getBlip()
	 * @generated
	 */
	EAttribute getBlip_HasImage();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.blip.Blip#getImage <em>Image</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Image</em>'.
	 * @see org.eclipse.osbp.xtext.blip.Blip#getImage()
	 * @see #getBlip()
	 * @generated
	 */
	EAttribute getBlip_Image();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.blip.Blip#isHasIcon <em>Has Icon</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Has Icon</em>'.
	 * @see org.eclipse.osbp.xtext.blip.Blip#isHasIcon()
	 * @see #getBlip()
	 * @generated
	 */
	EAttribute getBlip_HasIcon();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.blip.Blip#getIcon <em>Icon</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Icon</em>'.
	 * @see org.eclipse.osbp.xtext.blip.Blip#getIcon()
	 * @see #getBlip()
	 * @generated
	 */
	EAttribute getBlip_Icon();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.blip.Blip#isHasLogging <em>Has Logging</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Has Logging</em>'.
	 * @see org.eclipse.osbp.xtext.blip.Blip#isHasLogging()
	 * @see #getBlip()
	 * @generated
	 */
	EAttribute getBlip_HasLogging();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.blip.Blip#getFunctionGroup <em>Function Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Function Group</em>'.
	 * @see org.eclipse.osbp.xtext.blip.Blip#getFunctionGroup()
	 * @see #getBlip()
	 * @generated
	 */
	EReference getBlip_FunctionGroup();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.blip.Blip#getWorkload <em>Workload</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Workload</em>'.
	 * @see org.eclipse.osbp.xtext.blip.Blip#getWorkload()
	 * @see #getBlip()
	 * @generated
	 */
	EReference getBlip_Workload();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.blip.Blip#getItems <em>Items</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Items</em>'.
	 * @see org.eclipse.osbp.xtext.blip.Blip#getItems()
	 * @see #getBlip()
	 * @generated
	 */
	EReference getBlip_Items();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.blip.BlipWorkload <em>Blip Workload</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Blip Workload</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipWorkload
	 * @generated
	 */
	EClass getBlipWorkload();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.blip.BlipWorkload#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Source</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipWorkload#getSource()
	 * @see #getBlipWorkload()
	 * @generated
	 */
	EReference getBlipWorkload_Source();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.blip.BlipWorkload#getFilters <em>Filters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Filters</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipWorkload#getFilters()
	 * @see #getBlipWorkload()
	 * @generated
	 */
	EReference getBlipWorkload_Filters();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.blip.BlipFilter <em>Blip Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Blip Filter</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipFilter
	 * @generated
	 */
	EClass getBlipFilter();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.blip.BlipFilter#getFilterName <em>Filter Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Filter Name</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipFilter#getFilterName()
	 * @see #getBlipFilter()
	 * @generated
	 */
	EAttribute getBlipFilter_FilterName();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.blip.BlipFilter#getFilterValue <em>Filter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Filter Value</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipFilter#getFilterValue()
	 * @see #getBlipFilter()
	 * @generated
	 */
	EAttribute getBlipFilter_FilterValue();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.blip.BlipDto <em>Blip Dto</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Blip Dto</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipDto
	 * @generated
	 */
	EClass getBlipDto();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.blip.BlipDto#getDtoRef <em>Dto Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Dto Ref</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipDto#getDtoRef()
	 * @see #getBlipDto()
	 * @generated
	 */
	EReference getBlipDto_DtoRef();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.blip.BlipDtoPath <em>Blip Dto Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Blip Dto Path</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipDtoPath
	 * @generated
	 */
	EClass getBlipDtoPath();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.blip.BlipDtoPath#getDtoPath <em>Dto Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Dto Path</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipDtoPath#getDtoPath()
	 * @see #getBlipDtoPath()
	 * @generated
	 */
	EAttribute getBlipDtoPath_DtoPath();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.blip.BlipItem <em>Blip Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Blip Item</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipItem
	 * @generated
	 */
	EClass getBlipItem();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.blip.BlipItem#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipItem#getName()
	 * @see #getBlipItem()
	 * @generated
	 */
	EAttribute getBlipItem_Name();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.blip.BlipItem#getDtoPath <em>Dto Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Dto Path</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipItem#getDtoPath()
	 * @see #getBlipItem()
	 * @generated
	 */
	EReference getBlipItem_DtoPath();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.xtext.blip.BlipItem#getOperativeLDto() <em>Get Operative LDto</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Operative LDto</em>' operation.
	 * @see org.eclipse.osbp.xtext.blip.BlipItem#getOperativeLDto()
	 * @generated
	 */
	EOperation getBlipItem__GetOperativeLDto();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.blip.BlipEvent <em>Blip Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Blip Event</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipEvent
	 * @generated
	 */
	EClass getBlipEvent();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.blip.BlipEvent#getEvent <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Event</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipEvent#getEvent()
	 * @see #getBlipEvent()
	 * @generated
	 */
	EReference getBlipEvent_Event();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.blip.BlipStartEvent <em>Blip Start Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Blip Start Event</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipStartEvent
	 * @generated
	 */
	EClass getBlipStartEvent();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.blip.BlipEndEvent <em>Blip End Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Blip End Event</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipEndEvent
	 * @generated
	 */
	EClass getBlipEndEvent();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.blip.BlipEndEvent#getEndHandling <em>End Handling</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Handling</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipEndEvent#getEndHandling()
	 * @see #getBlipEndEvent()
	 * @generated
	 */
	EAttribute getBlipEndEvent_EndHandling();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.blip.BlipUserTask <em>Blip User Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Blip User Task</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipUserTask
	 * @generated
	 */
	EClass getBlipUserTask();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.blip.BlipUserTask#getTask <em>Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Task</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipUserTask#getTask()
	 * @see #getBlipUserTask()
	 * @generated
	 */
	EReference getBlipUserTask_Task();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.blip.BlipUserTask#getOnEntry <em>On Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>On Entry</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipUserTask#getOnEntry()
	 * @see #getBlipUserTask()
	 * @generated
	 */
	EReference getBlipUserTask_OnEntry();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.blip.BlipUserTask#getOnExit <em>On Exit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>On Exit</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipUserTask#getOnExit()
	 * @see #getBlipUserTask()
	 * @generated
	 */
	EReference getBlipUserTask_OnExit();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.blip.BlipCallActivity <em>Blip Call Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Blip Call Activity</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipCallActivity
	 * @generated
	 */
	EClass getBlipCallActivity();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.blip.BlipCallActivity#getCallActivity <em>Call Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Call Activity</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipCallActivity#getCallActivity()
	 * @see #getBlipCallActivity()
	 * @generated
	 */
	EReference getBlipCallActivity_CallActivity();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.blip.BlipCallActivity#getOnEntry <em>On Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>On Entry</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipCallActivity#getOnEntry()
	 * @see #getBlipCallActivity()
	 * @generated
	 */
	EReference getBlipCallActivity_OnEntry();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.blip.BlipCallActivity#getOnExit <em>On Exit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>On Exit</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipCallActivity#getOnExit()
	 * @see #getBlipCallActivity()
	 * @generated
	 */
	EReference getBlipCallActivity_OnExit();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.blip.BlipScript <em>Blip Script</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Blip Script</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipScript
	 * @generated
	 */
	EClass getBlipScript();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.blip.BlipScript#getTask <em>Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Task</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipScript#getTask()
	 * @see #getBlipScript()
	 * @generated
	 */
	EReference getBlipScript_Task();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.blip.BlipScript#getFunction <em>Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Function</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipScript#getFunction()
	 * @see #getBlipScript()
	 * @generated
	 */
	EReference getBlipScript_Function();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.blip.BlipScriptTask <em>Blip Script Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Blip Script Task</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipScriptTask
	 * @generated
	 */
	EClass getBlipScriptTask();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.blip.BlipPersistTask <em>Blip Persist Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Blip Persist Task</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipPersistTask
	 * @generated
	 */
	EClass getBlipPersistTask();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.blip.BlipServiceTask <em>Blip Service Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Blip Service Task</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipServiceTask
	 * @generated
	 */
	EClass getBlipServiceTask();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.blip.BlipServiceTask#getTask <em>Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Task</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipServiceTask#getTask()
	 * @see #getBlipServiceTask()
	 * @generated
	 */
	EReference getBlipServiceTask_Task();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.blip.BlipServiceTask#getOnEntry <em>On Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>On Entry</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipServiceTask#getOnEntry()
	 * @see #getBlipServiceTask()
	 * @generated
	 */
	EReference getBlipServiceTask_OnEntry();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.blip.BlipServiceTask#getOnExit <em>On Exit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>On Exit</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipServiceTask#getOnExit()
	 * @see #getBlipServiceTask()
	 * @generated
	 */
	EReference getBlipServiceTask_OnExit();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.blip.BlipServiceTask#getExecutionMode <em>Execution Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Execution Mode</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipServiceTask#getExecutionMode()
	 * @see #getBlipServiceTask()
	 * @generated
	 */
	EAttribute getBlipServiceTask_ExecutionMode();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.blip.BlipServiceTask#getTimeoutInSecs <em>Timeout In Secs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Timeout In Secs</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipServiceTask#getTimeoutInSecs()
	 * @see #getBlipServiceTask()
	 * @generated
	 */
	EAttribute getBlipServiceTask_TimeoutInSecs();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.blip.BlipServiceTask#getFunction <em>Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Function</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipServiceTask#getFunction()
	 * @see #getBlipServiceTask()
	 * @generated
	 */
	EReference getBlipServiceTask_Function();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.blip.BlipServiceTask#getWebServiceInterface <em>Web Service Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Web Service Interface</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipServiceTask#getWebServiceInterface()
	 * @see #getBlipServiceTask()
	 * @generated
	 */
	EAttribute getBlipServiceTask_WebServiceInterface();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.blip.BlipServiceTask#getWebServiceOperation <em>Web Service Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Web Service Operation</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipServiceTask#getWebServiceOperation()
	 * @see #getBlipServiceTask()
	 * @generated
	 */
	EAttribute getBlipServiceTask_WebServiceOperation();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.blip.BlipOutGoing <em>Blip Out Going</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Blip Out Going</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipOutGoing
	 * @generated
	 */
	EClass getBlipOutGoing();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.blip.BlipOutGoing#getSequenceFlow <em>Sequence Flow</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sequence Flow</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipOutGoing#getSequenceFlow()
	 * @see #getBlipOutGoing()
	 * @generated
	 */
	EReference getBlipOutGoing_SequenceFlow();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.blip.BlipOutGoing#getConstraint <em>Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Constraint</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipOutGoing#getConstraint()
	 * @see #getBlipOutGoing()
	 * @generated
	 */
	EReference getBlipOutGoing_Constraint();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.blip.BlipOutGoing#isIsDefault <em>Is Default</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Default</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipOutGoing#isIsDefault()
	 * @see #getBlipOutGoing()
	 * @generated
	 */
	EAttribute getBlipOutGoing_IsDefault();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.blip.BlipOutGoingDefault <em>Blip Out Going Default</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Blip Out Going Default</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipOutGoingDefault
	 * @generated
	 */
	EClass getBlipOutGoingDefault();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.blip.BlipSplitGateway <em>Blip Split Gateway</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Blip Split Gateway</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipSplitGateway
	 * @generated
	 */
	EClass getBlipSplitGateway();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.blip.BlipSplitGateway#getGateway <em>Gateway</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Gateway</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipSplitGateway#getGateway()
	 * @see #getBlipSplitGateway()
	 * @generated
	 */
	EReference getBlipSplitGateway_Gateway();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.blip.BlipSplitGateway#getOutgoings <em>Outgoings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Outgoings</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipSplitGateway#getOutgoings()
	 * @see #getBlipSplitGateway()
	 * @generated
	 */
	EReference getBlipSplitGateway_Outgoings();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.blip.BlipExclusiveSplitGateway <em>Blip Exclusive Split Gateway</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Blip Exclusive Split Gateway</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipExclusiveSplitGateway
	 * @generated
	 */
	EClass getBlipExclusiveSplitGateway();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.blip.BlipInclusiveSplitGateway <em>Blip Inclusive Split Gateway</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Blip Inclusive Split Gateway</em>'.
	 * @see org.eclipse.osbp.xtext.blip.BlipInclusiveSplitGateway
	 * @generated
	 */
	EClass getBlipInclusiveSplitGateway();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.xtext.blip.EndEventHandlingEnum <em>End Event Handling Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>End Event Handling Enum</em>'.
	 * @see org.eclipse.osbp.xtext.blip.EndEventHandlingEnum
	 * @generated
	 */
	EEnum getEndEventHandlingEnum();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.xtext.blip.ServiceExecutionModeEnum <em>Service Execution Mode Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Service Execution Mode Enum</em>'.
	 * @see org.eclipse.osbp.xtext.blip.ServiceExecutionModeEnum
	 * @generated
	 */
	EEnum getServiceExecutionModeEnum();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.xtext.blip.IconsEnum <em>Icons Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Icons Enum</em>'.
	 * @see org.eclipse.osbp.xtext.blip.IconsEnum
	 * @generated
	 */
	EEnum getIconsEnum();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.emf.ecore.InternalEObject <em>Internal EObject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Internal EObject</em>'.
	 * @see org.eclipse.emf.ecore.InternalEObject
	 * @model instanceClass="org.eclipse.emf.ecore.InternalEObject"
	 * @generated
	 */
	EDataType getInternalEObject();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	BlipDSLFactory getBlipDSLFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipModelImpl <em>Blip Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipModelImpl
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipModel()
		 * @generated
		 */
		EClass BLIP_MODEL = eINSTANCE.getBlipModel();

		/**
		 * The meta object literal for the '<em><b>Import Section</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLIP_MODEL__IMPORT_SECTION = eINSTANCE.getBlipModel_ImportSection();

		/**
		 * The meta object literal for the '<em><b>Blippackage</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLIP_MODEL__BLIPPACKAGE = eINSTANCE.getBlipModel_Blippackage();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipLazyResolverImpl <em>Blip Lazy Resolver</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipLazyResolverImpl
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipLazyResolver()
		 * @generated
		 */
		EClass BLIP_LAZY_RESOLVER = eINSTANCE.getBlipLazyResolver();

		/**
		 * The meta object literal for the '<em><b>EResolve Proxy</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BLIP_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT = eINSTANCE.getBlipLazyResolver__EResolveProxy__InternalEObject();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipPackageImpl <em>Blip Package</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipPackageImpl
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipPackage()
		 * @generated
		 */
		EClass BLIP_PACKAGE = eINSTANCE.getBlipPackage();

		/**
		 * The meta object literal for the '<em><b>Blips</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLIP_PACKAGE__BLIPS = eINSTANCE.getBlipPackage_Blips();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipBaseImpl <em>Blip Base</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipBaseImpl
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipBase()
		 * @generated
		 */
		EClass BLIP_BASE = eINSTANCE.getBlipBase();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLIP_BASE__NAME = eINSTANCE.getBlipBase_Name();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipImpl <em>Blip</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipImpl
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlip()
		 * @generated
		 */
		EClass BLIP = eINSTANCE.getBlip();

		/**
		 * The meta object literal for the '<em><b>Process</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLIP__PROCESS = eINSTANCE.getBlip_Process();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLIP__DESCRIPTION = eINSTANCE.getBlip_Description();

		/**
		 * The meta object literal for the '<em><b>Description Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLIP__DESCRIPTION_VALUE = eINSTANCE.getBlip_DescriptionValue();

		/**
		 * The meta object literal for the '<em><b>Has Image</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLIP__HAS_IMAGE = eINSTANCE.getBlip_HasImage();

		/**
		 * The meta object literal for the '<em><b>Image</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLIP__IMAGE = eINSTANCE.getBlip_Image();

		/**
		 * The meta object literal for the '<em><b>Has Icon</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLIP__HAS_ICON = eINSTANCE.getBlip_HasIcon();

		/**
		 * The meta object literal for the '<em><b>Icon</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLIP__ICON = eINSTANCE.getBlip_Icon();

		/**
		 * The meta object literal for the '<em><b>Has Logging</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLIP__HAS_LOGGING = eINSTANCE.getBlip_HasLogging();

		/**
		 * The meta object literal for the '<em><b>Function Group</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLIP__FUNCTION_GROUP = eINSTANCE.getBlip_FunctionGroup();

		/**
		 * The meta object literal for the '<em><b>Workload</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLIP__WORKLOAD = eINSTANCE.getBlip_Workload();

		/**
		 * The meta object literal for the '<em><b>Items</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLIP__ITEMS = eINSTANCE.getBlip_Items();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipWorkloadImpl <em>Blip Workload</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipWorkloadImpl
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipWorkload()
		 * @generated
		 */
		EClass BLIP_WORKLOAD = eINSTANCE.getBlipWorkload();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLIP_WORKLOAD__SOURCE = eINSTANCE.getBlipWorkload_Source();

		/**
		 * The meta object literal for the '<em><b>Filters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLIP_WORKLOAD__FILTERS = eINSTANCE.getBlipWorkload_Filters();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipFilterImpl <em>Blip Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipFilterImpl
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipFilter()
		 * @generated
		 */
		EClass BLIP_FILTER = eINSTANCE.getBlipFilter();

		/**
		 * The meta object literal for the '<em><b>Filter Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLIP_FILTER__FILTER_NAME = eINSTANCE.getBlipFilter_FilterName();

		/**
		 * The meta object literal for the '<em><b>Filter Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLIP_FILTER__FILTER_VALUE = eINSTANCE.getBlipFilter_FilterValue();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipDtoImpl <em>Blip Dto</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipDtoImpl
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipDto()
		 * @generated
		 */
		EClass BLIP_DTO = eINSTANCE.getBlipDto();

		/**
		 * The meta object literal for the '<em><b>Dto Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLIP_DTO__DTO_REF = eINSTANCE.getBlipDto_DtoRef();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipDtoPathImpl <em>Blip Dto Path</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipDtoPathImpl
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipDtoPath()
		 * @generated
		 */
		EClass BLIP_DTO_PATH = eINSTANCE.getBlipDtoPath();

		/**
		 * The meta object literal for the '<em><b>Dto Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLIP_DTO_PATH__DTO_PATH = eINSTANCE.getBlipDtoPath_DtoPath();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipItemImpl <em>Blip Item</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipItemImpl
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipItem()
		 * @generated
		 */
		EClass BLIP_ITEM = eINSTANCE.getBlipItem();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLIP_ITEM__NAME = eINSTANCE.getBlipItem_Name();

		/**
		 * The meta object literal for the '<em><b>Dto Path</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLIP_ITEM__DTO_PATH = eINSTANCE.getBlipItem_DtoPath();

		/**
		 * The meta object literal for the '<em><b>Get Operative LDto</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BLIP_ITEM___GET_OPERATIVE_LDTO = eINSTANCE.getBlipItem__GetOperativeLDto();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipEventImpl <em>Blip Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipEventImpl
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipEvent()
		 * @generated
		 */
		EClass BLIP_EVENT = eINSTANCE.getBlipEvent();

		/**
		 * The meta object literal for the '<em><b>Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLIP_EVENT__EVENT = eINSTANCE.getBlipEvent_Event();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipStartEventImpl <em>Blip Start Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipStartEventImpl
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipStartEvent()
		 * @generated
		 */
		EClass BLIP_START_EVENT = eINSTANCE.getBlipStartEvent();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipEndEventImpl <em>Blip End Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipEndEventImpl
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipEndEvent()
		 * @generated
		 */
		EClass BLIP_END_EVENT = eINSTANCE.getBlipEndEvent();

		/**
		 * The meta object literal for the '<em><b>End Handling</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLIP_END_EVENT__END_HANDLING = eINSTANCE.getBlipEndEvent_EndHandling();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipUserTaskImpl <em>Blip User Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipUserTaskImpl
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipUserTask()
		 * @generated
		 */
		EClass BLIP_USER_TASK = eINSTANCE.getBlipUserTask();

		/**
		 * The meta object literal for the '<em><b>Task</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLIP_USER_TASK__TASK = eINSTANCE.getBlipUserTask_Task();

		/**
		 * The meta object literal for the '<em><b>On Entry</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLIP_USER_TASK__ON_ENTRY = eINSTANCE.getBlipUserTask_OnEntry();

		/**
		 * The meta object literal for the '<em><b>On Exit</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLIP_USER_TASK__ON_EXIT = eINSTANCE.getBlipUserTask_OnExit();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipCallActivityImpl <em>Blip Call Activity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipCallActivityImpl
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipCallActivity()
		 * @generated
		 */
		EClass BLIP_CALL_ACTIVITY = eINSTANCE.getBlipCallActivity();

		/**
		 * The meta object literal for the '<em><b>Call Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLIP_CALL_ACTIVITY__CALL_ACTIVITY = eINSTANCE.getBlipCallActivity_CallActivity();

		/**
		 * The meta object literal for the '<em><b>On Entry</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLIP_CALL_ACTIVITY__ON_ENTRY = eINSTANCE.getBlipCallActivity_OnEntry();

		/**
		 * The meta object literal for the '<em><b>On Exit</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLIP_CALL_ACTIVITY__ON_EXIT = eINSTANCE.getBlipCallActivity_OnExit();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipScriptImpl <em>Blip Script</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipScriptImpl
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipScript()
		 * @generated
		 */
		EClass BLIP_SCRIPT = eINSTANCE.getBlipScript();

		/**
		 * The meta object literal for the '<em><b>Task</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLIP_SCRIPT__TASK = eINSTANCE.getBlipScript_Task();

		/**
		 * The meta object literal for the '<em><b>Function</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLIP_SCRIPT__FUNCTION = eINSTANCE.getBlipScript_Function();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipScriptTaskImpl <em>Blip Script Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipScriptTaskImpl
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipScriptTask()
		 * @generated
		 */
		EClass BLIP_SCRIPT_TASK = eINSTANCE.getBlipScriptTask();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipPersistTaskImpl <em>Blip Persist Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipPersistTaskImpl
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipPersistTask()
		 * @generated
		 */
		EClass BLIP_PERSIST_TASK = eINSTANCE.getBlipPersistTask();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipServiceTaskImpl <em>Blip Service Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipServiceTaskImpl
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipServiceTask()
		 * @generated
		 */
		EClass BLIP_SERVICE_TASK = eINSTANCE.getBlipServiceTask();

		/**
		 * The meta object literal for the '<em><b>Task</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLIP_SERVICE_TASK__TASK = eINSTANCE.getBlipServiceTask_Task();

		/**
		 * The meta object literal for the '<em><b>On Entry</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLIP_SERVICE_TASK__ON_ENTRY = eINSTANCE.getBlipServiceTask_OnEntry();

		/**
		 * The meta object literal for the '<em><b>On Exit</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLIP_SERVICE_TASK__ON_EXIT = eINSTANCE.getBlipServiceTask_OnExit();

		/**
		 * The meta object literal for the '<em><b>Execution Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLIP_SERVICE_TASK__EXECUTION_MODE = eINSTANCE.getBlipServiceTask_ExecutionMode();

		/**
		 * The meta object literal for the '<em><b>Timeout In Secs</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLIP_SERVICE_TASK__TIMEOUT_IN_SECS = eINSTANCE.getBlipServiceTask_TimeoutInSecs();

		/**
		 * The meta object literal for the '<em><b>Function</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLIP_SERVICE_TASK__FUNCTION = eINSTANCE.getBlipServiceTask_Function();

		/**
		 * The meta object literal for the '<em><b>Web Service Interface</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLIP_SERVICE_TASK__WEB_SERVICE_INTERFACE = eINSTANCE.getBlipServiceTask_WebServiceInterface();

		/**
		 * The meta object literal for the '<em><b>Web Service Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLIP_SERVICE_TASK__WEB_SERVICE_OPERATION = eINSTANCE.getBlipServiceTask_WebServiceOperation();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipOutGoingImpl <em>Blip Out Going</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipOutGoingImpl
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipOutGoing()
		 * @generated
		 */
		EClass BLIP_OUT_GOING = eINSTANCE.getBlipOutGoing();

		/**
		 * The meta object literal for the '<em><b>Sequence Flow</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLIP_OUT_GOING__SEQUENCE_FLOW = eINSTANCE.getBlipOutGoing_SequenceFlow();

		/**
		 * The meta object literal for the '<em><b>Constraint</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLIP_OUT_GOING__CONSTRAINT = eINSTANCE.getBlipOutGoing_Constraint();

		/**
		 * The meta object literal for the '<em><b>Is Default</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLIP_OUT_GOING__IS_DEFAULT = eINSTANCE.getBlipOutGoing_IsDefault();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipOutGoingDefaultImpl <em>Blip Out Going Default</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipOutGoingDefaultImpl
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipOutGoingDefault()
		 * @generated
		 */
		EClass BLIP_OUT_GOING_DEFAULT = eINSTANCE.getBlipOutGoingDefault();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipSplitGatewayImpl <em>Blip Split Gateway</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipSplitGatewayImpl
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipSplitGateway()
		 * @generated
		 */
		EClass BLIP_SPLIT_GATEWAY = eINSTANCE.getBlipSplitGateway();

		/**
		 * The meta object literal for the '<em><b>Gateway</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLIP_SPLIT_GATEWAY__GATEWAY = eINSTANCE.getBlipSplitGateway_Gateway();

		/**
		 * The meta object literal for the '<em><b>Outgoings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLIP_SPLIT_GATEWAY__OUTGOINGS = eINSTANCE.getBlipSplitGateway_Outgoings();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipExclusiveSplitGatewayImpl <em>Blip Exclusive Split Gateway</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipExclusiveSplitGatewayImpl
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipExclusiveSplitGateway()
		 * @generated
		 */
		EClass BLIP_EXCLUSIVE_SPLIT_GATEWAY = eINSTANCE.getBlipExclusiveSplitGateway();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.blip.impl.BlipInclusiveSplitGatewayImpl <em>Blip Inclusive Split Gateway</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipInclusiveSplitGatewayImpl
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getBlipInclusiveSplitGateway()
		 * @generated
		 */
		EClass BLIP_INCLUSIVE_SPLIT_GATEWAY = eINSTANCE.getBlipInclusiveSplitGateway();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.blip.EndEventHandlingEnum <em>End Event Handling Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.blip.EndEventHandlingEnum
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getEndEventHandlingEnum()
		 * @generated
		 */
		EEnum END_EVENT_HANDLING_ENUM = eINSTANCE.getEndEventHandlingEnum();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.blip.ServiceExecutionModeEnum <em>Service Execution Mode Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.blip.ServiceExecutionModeEnum
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getServiceExecutionModeEnum()
		 * @generated
		 */
		EEnum SERVICE_EXECUTION_MODE_ENUM = eINSTANCE.getServiceExecutionModeEnum();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.blip.IconsEnum <em>Icons Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.blip.IconsEnum
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getIconsEnum()
		 * @generated
		 */
		EEnum ICONS_ENUM = eINSTANCE.getIconsEnum();

		/**
		 * The meta object literal for the '<em>Internal EObject</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.emf.ecore.InternalEObject
		 * @see org.eclipse.osbp.xtext.blip.impl.BlipDSLPackageImpl#getInternalEObject()
		 * @generated
		 */
		EDataType INTERNAL_EOBJECT = eINSTANCE.getInternalEObject();

	}

} //BlipDSLPackage
