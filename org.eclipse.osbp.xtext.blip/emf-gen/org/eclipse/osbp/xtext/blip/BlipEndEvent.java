/**
 * Copyright (c) 2014, Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Loetz GmbH&Co.KG - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.blip;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Blip End Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.blip.BlipEndEvent#getEndHandling <em>End Handling</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipEndEvent()
 * @model
 * @generated
 */
public interface BlipEndEvent extends BlipEvent {
	/**
	 * Returns the value of the '<em><b>End Handling</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.xtext.blip.EndEventHandlingEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Handling</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Handling</em>' attribute.
	 * @see org.eclipse.osbp.xtext.blip.EndEventHandlingEnum
	 * @see #setEndHandling(EndEventHandlingEnum)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipEndEvent_EndHandling()
	 * @model unique="false"
	 * @generated
	 */
	EndEventHandlingEnum getEndHandling();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.BlipEndEvent#getEndHandling <em>End Handling</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Handling</em>' attribute.
	 * @see org.eclipse.osbp.xtext.blip.EndEventHandlingEnum
	 * @see #getEndHandling()
	 * @generated
	 */
	void setEndHandling(EndEventHandlingEnum value);

} // BlipEndEvent
