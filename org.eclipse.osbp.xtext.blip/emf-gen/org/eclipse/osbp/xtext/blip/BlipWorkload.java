/**
 * Copyright (c) 2014, Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Loetz GmbH&Co.KG - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.blip;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Blip Workload</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.blip.BlipWorkload#getSource <em>Source</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.blip.BlipWorkload#getFilters <em>Filters</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipWorkload()
 * @model
 * @generated
 */
public interface BlipWorkload extends BlipLazyResolver {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' containment reference.
	 * @see #setSource(BlipDto)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipWorkload_Source()
	 * @model containment="true"
	 * @generated
	 */
	BlipDto getSource();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.BlipWorkload#getSource <em>Source</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' containment reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(BlipDto value);

	/**
	 * Returns the value of the '<em><b>Filters</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.blip.BlipFilter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Filters</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Filters</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipWorkload_Filters()
	 * @model containment="true"
	 * @generated
	 */
	EList<BlipFilter> getFilters();

} // BlipWorkload
