/**
 * Copyright (c) 2014, Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Loetz GmbH&Co.KG - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.blip;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Blip Exclusive Split Gateway</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipExclusiveSplitGateway()
 * @model
 * @generated
 */
public interface BlipExclusiveSplitGateway extends BlipSplitGateway {
} // BlipExclusiveSplitGateway
