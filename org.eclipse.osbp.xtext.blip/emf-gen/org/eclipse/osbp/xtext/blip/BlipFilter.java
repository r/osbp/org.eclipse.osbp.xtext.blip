/**
 * Copyright (c) 2014, Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Loetz GmbH&Co.KG - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.blip;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Blip Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.blip.BlipFilter#getFilterName <em>Filter Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.blip.BlipFilter#getFilterValue <em>Filter Value</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipFilter()
 * @model
 * @generated
 */
public interface BlipFilter extends BlipLazyResolver {
	/**
	 * Returns the value of the '<em><b>Filter Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Filter Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Filter Name</em>' attribute.
	 * @see #setFilterName(String)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipFilter_FilterName()
	 * @model unique="false"
	 * @generated
	 */
	String getFilterName();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.BlipFilter#getFilterName <em>Filter Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Filter Name</em>' attribute.
	 * @see #getFilterName()
	 * @generated
	 */
	void setFilterName(String value);

	/**
	 * Returns the value of the '<em><b>Filter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Filter Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Filter Value</em>' attribute.
	 * @see #setFilterValue(String)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipFilter_FilterValue()
	 * @model unique="false"
	 * @generated
	 */
	String getFilterValue();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.BlipFilter#getFilterValue <em>Filter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Filter Value</em>' attribute.
	 * @see #getFilterValue()
	 * @generated
	 */
	void setFilterValue(String value);

} // BlipFilter
