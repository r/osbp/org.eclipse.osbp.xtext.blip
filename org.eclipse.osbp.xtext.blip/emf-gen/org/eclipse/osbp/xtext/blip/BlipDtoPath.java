/**
 * Copyright (c) 2014, Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Loetz GmbH&Co.KG - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.blip;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Blip Dto Path</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.blip.BlipDtoPath#getDtoPath <em>Dto Path</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipDtoPath()
 * @model
 * @generated
 */
public interface BlipDtoPath extends EObject {
	/**
	 * Returns the value of the '<em><b>Dto Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dto Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dto Path</em>' attribute.
	 * @see #setDtoPath(String)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipDtoPath_DtoPath()
	 * @model unique="false"
	 * @generated
	 */
	String getDtoPath();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.BlipDtoPath#getDtoPath <em>Dto Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dto Path</em>' attribute.
	 * @see #getDtoPath()
	 * @generated
	 */
	void setDtoPath(String value);

} // BlipDtoPath
