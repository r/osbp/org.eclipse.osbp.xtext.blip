/**
 * Copyright (c) 2014, Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Loetz GmbH&Co.KG - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.blip;

import org.eclipse.bpmn2.SequenceFlow;

import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryTest;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Blip Out Going</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.blip.BlipOutGoing#getSequenceFlow <em>Sequence Flow</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.blip.BlipOutGoing#getConstraint <em>Constraint</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.blip.BlipOutGoing#isIsDefault <em>Is Default</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipOutGoing()
 * @model
 * @generated
 */
public interface BlipOutGoing extends BlipItem {
	/**
	 * Returns the value of the '<em><b>Sequence Flow</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sequence Flow</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sequence Flow</em>' reference.
	 * @see #setSequenceFlow(SequenceFlow)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipOutGoing_SequenceFlow()
	 * @model
	 * @generated
	 */
	SequenceFlow getSequenceFlow();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.BlipOutGoing#getSequenceFlow <em>Sequence Flow</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sequence Flow</em>' reference.
	 * @see #getSequenceFlow()
	 * @generated
	 */
	void setSequenceFlow(SequenceFlow value);

	/**
	 * Returns the value of the '<em><b>Constraint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraint</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraint</em>' reference.
	 * @see #setConstraint(FunctionLibraryTest)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipOutGoing_Constraint()
	 * @model
	 * @generated
	 */
	FunctionLibraryTest getConstraint();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.BlipOutGoing#getConstraint <em>Constraint</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constraint</em>' reference.
	 * @see #getConstraint()
	 * @generated
	 */
	void setConstraint(FunctionLibraryTest value);

	/**
	 * Returns the value of the '<em><b>Is Default</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Default</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Default</em>' attribute.
	 * @see #setIsDefault(boolean)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipOutGoing_IsDefault()
	 * @model unique="false"
	 * @generated
	 */
	boolean isIsDefault();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.BlipOutGoing#isIsDefault <em>Is Default</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Default</em>' attribute.
	 * @see #isIsDefault()
	 * @generated
	 */
	void setIsDefault(boolean value);

} // BlipOutGoing
