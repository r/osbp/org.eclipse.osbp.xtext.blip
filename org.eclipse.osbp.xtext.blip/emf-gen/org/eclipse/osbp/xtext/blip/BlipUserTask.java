/**
 * Copyright (c) 2014, Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Loetz GmbH&Co.KG - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.blip;

import org.eclipse.bpmn2.Task;

import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryFunction;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Blip User Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.blip.BlipUserTask#getTask <em>Task</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.blip.BlipUserTask#getOnEntry <em>On Entry</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.blip.BlipUserTask#getOnExit <em>On Exit</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipUserTask()
 * @model
 * @generated
 */
public interface BlipUserTask extends BlipItem {
	/**
	 * Returns the value of the '<em><b>Task</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task</em>' reference.
	 * @see #setTask(Task)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipUserTask_Task()
	 * @model
	 * @generated
	 */
	Task getTask();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.BlipUserTask#getTask <em>Task</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task</em>' reference.
	 * @see #getTask()
	 * @generated
	 */
	void setTask(Task value);

	/**
	 * Returns the value of the '<em><b>On Entry</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>On Entry</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>On Entry</em>' reference.
	 * @see #setOnEntry(FunctionLibraryFunction)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipUserTask_OnEntry()
	 * @model
	 * @generated
	 */
	FunctionLibraryFunction getOnEntry();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.BlipUserTask#getOnEntry <em>On Entry</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>On Entry</em>' reference.
	 * @see #getOnEntry()
	 * @generated
	 */
	void setOnEntry(FunctionLibraryFunction value);

	/**
	 * Returns the value of the '<em><b>On Exit</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>On Exit</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>On Exit</em>' reference.
	 * @see #setOnExit(FunctionLibraryFunction)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipUserTask_OnExit()
	 * @model
	 * @generated
	 */
	FunctionLibraryFunction getOnExit();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.BlipUserTask#getOnExit <em>On Exit</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>On Exit</em>' reference.
	 * @see #getOnExit()
	 * @generated
	 */
	void setOnExit(FunctionLibraryFunction value);

} // BlipUserTask
