/**
 * Copyright (c) 2014, Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Loetz GmbH&Co.KG - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.blip;

import org.eclipse.bpmn2.Gateway;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Blip Split Gateway</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.blip.BlipSplitGateway#getGateway <em>Gateway</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.blip.BlipSplitGateway#getOutgoings <em>Outgoings</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipSplitGateway()
 * @model
 * @generated
 */
public interface BlipSplitGateway extends BlipItem {
	/**
	 * Returns the value of the '<em><b>Gateway</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Gateway</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Gateway</em>' reference.
	 * @see #setGateway(Gateway)
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipSplitGateway_Gateway()
	 * @model
	 * @generated
	 */
	Gateway getGateway();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.blip.BlipSplitGateway#getGateway <em>Gateway</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Gateway</em>' reference.
	 * @see #getGateway()
	 * @generated
	 */
	void setGateway(Gateway value);

	/**
	 * Returns the value of the '<em><b>Outgoings</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.blip.BlipOutGoing}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outgoings</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outgoings</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipSplitGateway_Outgoings()
	 * @model containment="true"
	 * @generated
	 */
	EList<BlipOutGoing> getOutgoings();

} // BlipSplitGateway
