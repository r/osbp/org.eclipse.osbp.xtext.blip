/**
 * Copyright (c) 2014, Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Loetz GmbH&Co.KG - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.blip.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.eclipse.osbp.dsl.semantic.common.types.LLazyResolver;
import org.eclipse.osbp.dsl.semantic.common.types.LPackage;

import org.eclipse.osbp.xtext.blip.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage
 * @generated
 */
public class BlipDSLSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static BlipDSLPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlipDSLSwitch() {
		if (modelPackage == null) {
			modelPackage = BlipDSLPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case BlipDSLPackage.BLIP_MODEL: {
				BlipModel blipModel = (BlipModel)theEObject;
				T result = caseBlipModel(blipModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlipDSLPackage.BLIP_LAZY_RESOLVER: {
				BlipLazyResolver blipLazyResolver = (BlipLazyResolver)theEObject;
				T result = caseBlipLazyResolver(blipLazyResolver);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlipDSLPackage.BLIP_PACKAGE: {
				BlipPackage blipPackage = (BlipPackage)theEObject;
				T result = caseBlipPackage(blipPackage);
				if (result == null) result = caseLPackage(blipPackage);
				if (result == null) result = caseLLazyResolver(blipPackage);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlipDSLPackage.BLIP_BASE: {
				BlipBase blipBase = (BlipBase)theEObject;
				T result = caseBlipBase(blipBase);
				if (result == null) result = caseBlipLazyResolver(blipBase);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlipDSLPackage.BLIP: {
				Blip blip = (Blip)theEObject;
				T result = caseBlip(blip);
				if (result == null) result = caseBlipBase(blip);
				if (result == null) result = caseBlipLazyResolver(blip);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlipDSLPackage.BLIP_WORKLOAD: {
				BlipWorkload blipWorkload = (BlipWorkload)theEObject;
				T result = caseBlipWorkload(blipWorkload);
				if (result == null) result = caseBlipLazyResolver(blipWorkload);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlipDSLPackage.BLIP_FILTER: {
				BlipFilter blipFilter = (BlipFilter)theEObject;
				T result = caseBlipFilter(blipFilter);
				if (result == null) result = caseBlipLazyResolver(blipFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlipDSLPackage.BLIP_DTO: {
				BlipDto blipDto = (BlipDto)theEObject;
				T result = caseBlipDto(blipDto);
				if (result == null) result = caseBlipLazyResolver(blipDto);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlipDSLPackage.BLIP_DTO_PATH: {
				BlipDtoPath blipDtoPath = (BlipDtoPath)theEObject;
				T result = caseBlipDtoPath(blipDtoPath);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlipDSLPackage.BLIP_ITEM: {
				BlipItem blipItem = (BlipItem)theEObject;
				T result = caseBlipItem(blipItem);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlipDSLPackage.BLIP_EVENT: {
				BlipEvent blipEvent = (BlipEvent)theEObject;
				T result = caseBlipEvent(blipEvent);
				if (result == null) result = caseBlipItem(blipEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlipDSLPackage.BLIP_START_EVENT: {
				BlipStartEvent blipStartEvent = (BlipStartEvent)theEObject;
				T result = caseBlipStartEvent(blipStartEvent);
				if (result == null) result = caseBlipEvent(blipStartEvent);
				if (result == null) result = caseBlipItem(blipStartEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlipDSLPackage.BLIP_END_EVENT: {
				BlipEndEvent blipEndEvent = (BlipEndEvent)theEObject;
				T result = caseBlipEndEvent(blipEndEvent);
				if (result == null) result = caseBlipEvent(blipEndEvent);
				if (result == null) result = caseBlipItem(blipEndEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlipDSLPackage.BLIP_USER_TASK: {
				BlipUserTask blipUserTask = (BlipUserTask)theEObject;
				T result = caseBlipUserTask(blipUserTask);
				if (result == null) result = caseBlipItem(blipUserTask);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlipDSLPackage.BLIP_CALL_ACTIVITY: {
				BlipCallActivity blipCallActivity = (BlipCallActivity)theEObject;
				T result = caseBlipCallActivity(blipCallActivity);
				if (result == null) result = caseBlipItem(blipCallActivity);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlipDSLPackage.BLIP_SCRIPT: {
				BlipScript blipScript = (BlipScript)theEObject;
				T result = caseBlipScript(blipScript);
				if (result == null) result = caseBlipItem(blipScript);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlipDSLPackage.BLIP_SCRIPT_TASK: {
				BlipScriptTask blipScriptTask = (BlipScriptTask)theEObject;
				T result = caseBlipScriptTask(blipScriptTask);
				if (result == null) result = caseBlipScript(blipScriptTask);
				if (result == null) result = caseBlipItem(blipScriptTask);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlipDSLPackage.BLIP_PERSIST_TASK: {
				BlipPersistTask blipPersistTask = (BlipPersistTask)theEObject;
				T result = caseBlipPersistTask(blipPersistTask);
				if (result == null) result = caseBlipScript(blipPersistTask);
				if (result == null) result = caseBlipItem(blipPersistTask);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlipDSLPackage.BLIP_SERVICE_TASK: {
				BlipServiceTask blipServiceTask = (BlipServiceTask)theEObject;
				T result = caseBlipServiceTask(blipServiceTask);
				if (result == null) result = caseBlipItem(blipServiceTask);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlipDSLPackage.BLIP_OUT_GOING: {
				BlipOutGoing blipOutGoing = (BlipOutGoing)theEObject;
				T result = caseBlipOutGoing(blipOutGoing);
				if (result == null) result = caseBlipItem(blipOutGoing);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlipDSLPackage.BLIP_OUT_GOING_DEFAULT: {
				BlipOutGoingDefault blipOutGoingDefault = (BlipOutGoingDefault)theEObject;
				T result = caseBlipOutGoingDefault(blipOutGoingDefault);
				if (result == null) result = caseBlipOutGoing(blipOutGoingDefault);
				if (result == null) result = caseBlipItem(blipOutGoingDefault);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlipDSLPackage.BLIP_SPLIT_GATEWAY: {
				BlipSplitGateway blipSplitGateway = (BlipSplitGateway)theEObject;
				T result = caseBlipSplitGateway(blipSplitGateway);
				if (result == null) result = caseBlipItem(blipSplitGateway);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlipDSLPackage.BLIP_EXCLUSIVE_SPLIT_GATEWAY: {
				BlipExclusiveSplitGateway blipExclusiveSplitGateway = (BlipExclusiveSplitGateway)theEObject;
				T result = caseBlipExclusiveSplitGateway(blipExclusiveSplitGateway);
				if (result == null) result = caseBlipSplitGateway(blipExclusiveSplitGateway);
				if (result == null) result = caseBlipItem(blipExclusiveSplitGateway);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlipDSLPackage.BLIP_INCLUSIVE_SPLIT_GATEWAY: {
				BlipInclusiveSplitGateway blipInclusiveSplitGateway = (BlipInclusiveSplitGateway)theEObject;
				T result = caseBlipInclusiveSplitGateway(blipInclusiveSplitGateway);
				if (result == null) result = caseBlipSplitGateway(blipInclusiveSplitGateway);
				if (result == null) result = caseBlipItem(blipInclusiveSplitGateway);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Blip Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Blip Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlipModel(BlipModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Blip Lazy Resolver</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Blip Lazy Resolver</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlipLazyResolver(BlipLazyResolver object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Blip Package</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Blip Package</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlipPackage(BlipPackage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Blip Base</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Blip Base</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlipBase(BlipBase object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Blip</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Blip</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlip(Blip object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Blip Workload</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Blip Workload</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlipWorkload(BlipWorkload object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Blip Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Blip Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlipFilter(BlipFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Blip Dto</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Blip Dto</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlipDto(BlipDto object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Blip Dto Path</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Blip Dto Path</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlipDtoPath(BlipDtoPath object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Blip Item</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Blip Item</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlipItem(BlipItem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Blip Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Blip Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlipEvent(BlipEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Blip Start Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Blip Start Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlipStartEvent(BlipStartEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Blip End Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Blip End Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlipEndEvent(BlipEndEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Blip User Task</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Blip User Task</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlipUserTask(BlipUserTask object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Blip Call Activity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Blip Call Activity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlipCallActivity(BlipCallActivity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Blip Script</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Blip Script</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlipScript(BlipScript object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Blip Script Task</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Blip Script Task</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlipScriptTask(BlipScriptTask object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Blip Persist Task</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Blip Persist Task</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlipPersistTask(BlipPersistTask object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Blip Service Task</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Blip Service Task</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlipServiceTask(BlipServiceTask object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Blip Out Going</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Blip Out Going</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlipOutGoing(BlipOutGoing object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Blip Out Going Default</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Blip Out Going Default</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlipOutGoingDefault(BlipOutGoingDefault object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Blip Split Gateway</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Blip Split Gateway</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlipSplitGateway(BlipSplitGateway object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Blip Exclusive Split Gateway</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Blip Exclusive Split Gateway</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlipExclusiveSplitGateway(BlipExclusiveSplitGateway object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Blip Inclusive Split Gateway</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Blip Inclusive Split Gateway</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlipInclusiveSplitGateway(BlipInclusiveSplitGateway object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>LLazy Resolver</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>LLazy Resolver</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLLazyResolver(LLazyResolver object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>LPackage</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>LPackage</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLPackage(LPackage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //BlipDSLSwitch
