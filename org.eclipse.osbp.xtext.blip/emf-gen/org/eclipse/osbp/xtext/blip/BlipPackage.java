/**
 * Copyright (c) 2014, Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Loetz GmbH&Co.KG - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.blip;

import org.eclipse.emf.common.util.EList;

import org.eclipse.osbp.dsl.semantic.common.types.LPackage;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Blip Package</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.blip.BlipPackage#getBlips <em>Blips</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipPackage()
 * @model
 * @generated
 */
public interface BlipPackage extends LPackage {
	/**
	 * Returns the value of the '<em><b>Blips</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.blip.Blip}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Blips</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Blips</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getBlipPackage_Blips()
	 * @model containment="true"
	 * @generated
	 */
	EList<Blip> getBlips();

} // BlipPackage
