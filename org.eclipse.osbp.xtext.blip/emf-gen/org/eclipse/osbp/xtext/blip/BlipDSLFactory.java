/**
 * Copyright (c) 2014, Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Loetz GmbH&Co.KG - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.blip;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage
 * @generated
 */
public interface BlipDSLFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BlipDSLFactory eINSTANCE = org.eclipse.osbp.xtext.blip.impl.BlipDSLFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Blip Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Blip Model</em>'.
	 * @generated
	 */
	BlipModel createBlipModel();

	/**
	 * Returns a new object of class '<em>Blip Lazy Resolver</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Blip Lazy Resolver</em>'.
	 * @generated
	 */
	BlipLazyResolver createBlipLazyResolver();

	/**
	 * Returns a new object of class '<em>Blip Package</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Blip Package</em>'.
	 * @generated
	 */
	BlipPackage createBlipPackage();

	/**
	 * Returns a new object of class '<em>Blip Base</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Blip Base</em>'.
	 * @generated
	 */
	BlipBase createBlipBase();

	/**
	 * Returns a new object of class '<em>Blip</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Blip</em>'.
	 * @generated
	 */
	Blip createBlip();

	/**
	 * Returns a new object of class '<em>Blip Workload</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Blip Workload</em>'.
	 * @generated
	 */
	BlipWorkload createBlipWorkload();

	/**
	 * Returns a new object of class '<em>Blip Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Blip Filter</em>'.
	 * @generated
	 */
	BlipFilter createBlipFilter();

	/**
	 * Returns a new object of class '<em>Blip Dto</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Blip Dto</em>'.
	 * @generated
	 */
	BlipDto createBlipDto();

	/**
	 * Returns a new object of class '<em>Blip Dto Path</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Blip Dto Path</em>'.
	 * @generated
	 */
	BlipDtoPath createBlipDtoPath();

	/**
	 * Returns a new object of class '<em>Blip Item</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Blip Item</em>'.
	 * @generated
	 */
	BlipItem createBlipItem();

	/**
	 * Returns a new object of class '<em>Blip Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Blip Event</em>'.
	 * @generated
	 */
	BlipEvent createBlipEvent();

	/**
	 * Returns a new object of class '<em>Blip Start Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Blip Start Event</em>'.
	 * @generated
	 */
	BlipStartEvent createBlipStartEvent();

	/**
	 * Returns a new object of class '<em>Blip End Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Blip End Event</em>'.
	 * @generated
	 */
	BlipEndEvent createBlipEndEvent();

	/**
	 * Returns a new object of class '<em>Blip User Task</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Blip User Task</em>'.
	 * @generated
	 */
	BlipUserTask createBlipUserTask();

	/**
	 * Returns a new object of class '<em>Blip Call Activity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Blip Call Activity</em>'.
	 * @generated
	 */
	BlipCallActivity createBlipCallActivity();

	/**
	 * Returns a new object of class '<em>Blip Script</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Blip Script</em>'.
	 * @generated
	 */
	BlipScript createBlipScript();

	/**
	 * Returns a new object of class '<em>Blip Script Task</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Blip Script Task</em>'.
	 * @generated
	 */
	BlipScriptTask createBlipScriptTask();

	/**
	 * Returns a new object of class '<em>Blip Persist Task</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Blip Persist Task</em>'.
	 * @generated
	 */
	BlipPersistTask createBlipPersistTask();

	/**
	 * Returns a new object of class '<em>Blip Service Task</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Blip Service Task</em>'.
	 * @generated
	 */
	BlipServiceTask createBlipServiceTask();

	/**
	 * Returns a new object of class '<em>Blip Out Going</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Blip Out Going</em>'.
	 * @generated
	 */
	BlipOutGoing createBlipOutGoing();

	/**
	 * Returns a new object of class '<em>Blip Out Going Default</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Blip Out Going Default</em>'.
	 * @generated
	 */
	BlipOutGoingDefault createBlipOutGoingDefault();

	/**
	 * Returns a new object of class '<em>Blip Split Gateway</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Blip Split Gateway</em>'.
	 * @generated
	 */
	BlipSplitGateway createBlipSplitGateway();

	/**
	 * Returns a new object of class '<em>Blip Exclusive Split Gateway</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Blip Exclusive Split Gateway</em>'.
	 * @generated
	 */
	BlipExclusiveSplitGateway createBlipExclusiveSplitGateway();

	/**
	 * Returns a new object of class '<em>Blip Inclusive Split Gateway</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Blip Inclusive Split Gateway</em>'.
	 * @generated
	 */
	BlipInclusiveSplitGateway createBlipInclusiveSplitGateway();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	BlipDSLPackage getBlipDSLPackage();

} //BlipDSLFactory
