/**
 * Copyright (c) 2014, Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Loetz GmbH&Co.KG - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.blip;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Icons Enum</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.blip.BlipDSLPackage#getIconsEnum()
 * @model
 * @generated
 */
public enum IconsEnum implements Enumerator {
	/**
	 * The '<em><b>ICON PLUS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_PLUS_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_PLUS(0, "ICON_PLUS", "plus"),

	/**
	 * The '<em><b>ICON MINUS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_MINUS_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_MINUS(0, "ICON_MINUS", "minus"),

	/**
	 * The '<em><b>ICON INFO</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_INFO_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_INFO(0, "ICON_INFO", "info"),

	/**
	 * The '<em><b>ICON LEFT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_LEFT_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_LEFT(0, "ICON_LEFT", "left"),

	/**
	 * The '<em><b>ICON UP</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_UP_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_UP(0, "ICON_UP", "up"),

	/**
	 * The '<em><b>ICON RIGHT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_RIGHT_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_RIGHT(0, "ICON_RIGHT", "right"),

	/**
	 * The '<em><b>ICON DOWN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_DOWN_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_DOWN(0, "ICON_DOWN", "down"),

	/**
	 * The '<em><b>ICON EXCHANGE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_EXCHANGE_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_EXCHANGE(0, "ICON_EXCHANGE", "exchange"),

	/**
	 * The '<em><b>ICON HOME</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_HOME_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_HOME(0, "ICON_HOME", "home"),

	/**
	 * The '<em><b>ICON HOME 1</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_HOME_1_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_HOME_1(0, "ICON_HOME_1", "home-1"),

	/**
	 * The '<em><b>ICON UP DIR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_UP_DIR_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_UP_DIR(0, "ICON_UP_DIR", "up-dir"),

	/**
	 * The '<em><b>ICON RIGHT DIR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_RIGHT_DIR_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_RIGHT_DIR(0, "ICON_RIGHT_DIR", "right-dir"),

	/**
	 * The '<em><b>ICON DOWN DIR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_DOWN_DIR_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_DOWN_DIR(0, "ICON_DOWN_DIR", "down-dir"),

	/**
	 * The '<em><b>ICON LEFT DIR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_LEFT_DIR_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_LEFT_DIR(0, "ICON_LEFT_DIR", "left-dir"),

	/**
	 * The '<em><b>ICON STAR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_STAR_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_STAR(0, "ICON_STAR", "star"),

	/**
	 * The '<em><b>ICON STAR EMPTY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_STAR_EMPTY_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_STAR_EMPTY(0, "ICON_STAR_EMPTY", "star-empty"),

	/**
	 * The '<em><b>ICON TH LIST</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_TH_LIST_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_TH_LIST(0, "ICON_TH_LIST", "th-list"),

	/**
	 * The '<em><b>ICON HEART EMPTY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_HEART_EMPTY_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_HEART_EMPTY(0, "ICON_HEART_EMPTY", "heart-empty"),

	/**
	 * The '<em><b>ICON HEART</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_HEART_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_HEART(0, "ICON_HEART", "heart"),

	/**
	 * The '<em><b>ICON MUSIC</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_MUSIC_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_MUSIC(0, "ICON_MUSIC", "music"),

	/**
	 * The '<em><b>ICON TH</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_TH_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_TH(0, "ICON_TH", "th"),

	/**
	 * The '<em><b>ICON FLAG</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_FLAG_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_FLAG(0, "ICON_FLAG", "flag"),

	/**
	 * The '<em><b>ICON COG</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_COG_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_COG(0, "ICON_COG", "cog"),

	/**
	 * The '<em><b>ICON ATTENTION</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_ATTENTION_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_ATTENTION(0, "ICON_ATTENTION", "attention"),

	/**
	 * The '<em><b>ICON MAIL</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_MAIL_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_MAIL(0, "ICON_MAIL", "mail"),

	/**
	 * The '<em><b>ICON EDIT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_EDIT_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_EDIT(0, "ICON_EDIT", "edit"),

	/**
	 * The '<em><b>ICON PENCIL</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_PENCIL_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_PENCIL(0, "ICON_PENCIL", "pencil"),

	/**
	 * The '<em><b>ICON OK</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_OK_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_OK(0, "ICON_OK", "ok"),

	/**
	 * The '<em><b>ICON CANCEL 1</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_CANCEL_1_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_CANCEL_1(0, "ICON_CANCEL_1", "cancel-1"),

	/**
	 * The '<em><b>ICON CANCEL</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_CANCEL_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_CANCEL(0, "ICON_CANCEL", "cancel"),

	/**
	 * The '<em><b>ICON CANCEL CIRCLE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_CANCEL_CIRCLE_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_CANCEL_CIRCLE(0, "ICON_CANCEL_CIRCLE", "cancel-circle"),

	/**
	 * The '<em><b>ICON HELP</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_HELP_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_HELP(0, "ICON_HELP", "help"),

	/**
	 * The '<em><b>ICON PLUS CIRCLE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_PLUS_CIRCLE_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_PLUS_CIRCLE(0, "ICON_PLUS_CIRCLE", "plus-circle"),

	/**
	 * The '<em><b>ICON MINUS CIRCLE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_MINUS_CIRCLE_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_MINUS_CIRCLE(0, "ICON_MINUS_CIRCLE", "minus-circle"),

	/**
	 * The '<em><b>ICON RIGHT THIN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_RIGHT_THIN_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_RIGHT_THIN(0, "ICON_RIGHT_THIN", "right-thin"),

	/**
	 * The '<em><b>ICON FORWARD</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_FORWARD_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_FORWARD(0, "ICON_FORWARD", "forward"),

	/**
	 * The '<em><b>ICON CW</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_CW_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_CW(0, "ICON_CW", "cw"),

	/**
	 * The '<em><b>ICON LEFT THIN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_LEFT_THIN_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_LEFT_THIN(0, "ICON_LEFT_THIN", "left-thin"),

	/**
	 * The '<em><b>ICON UP THIN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_UP_THIN_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_UP_THIN(0, "ICON_UP_THIN", "up-thin"),

	/**
	 * The '<em><b>ICON DOWN THIN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_DOWN_THIN_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_DOWN_THIN(0, "ICON_DOWN_THIN", "down-thin"),

	/**
	 * The '<em><b>ICON LEFT BOLD</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_LEFT_BOLD_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_LEFT_BOLD(0, "ICON_LEFT_BOLD", "left-bold"),

	/**
	 * The '<em><b>ICON RIGHT BOLD</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_RIGHT_BOLD_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_RIGHT_BOLD(0, "ICON_RIGHT_BOLD", "right-bold"),

	/**
	 * The '<em><b>ICON UP BOLD</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_UP_BOLD_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_UP_BOLD(0, "ICON_UP_BOLD", "up-bold"),

	/**
	 * The '<em><b>ICON DOWN BOLD</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_DOWN_BOLD_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_DOWN_BOLD(0, "ICON_DOWN_BOLD", "down-bold"),

	/**
	 * The '<em><b>ICON USER ADD</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_USER_ADD_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_USER_ADD(0, "ICON_USER_ADD", "user-add"),

	/**
	 * The '<em><b>ICON HELP CIRCLE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_HELP_CIRCLE_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_HELP_CIRCLE(0, "ICON_HELP_CIRCLE", "help-circle"),

	/**
	 * The '<em><b>ICON INFO CIRCLE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_INFO_CIRCLE_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_INFO_CIRCLE(0, "ICON_INFO_CIRCLE", "info-circle"),

	/**
	 * The '<em><b>ICON BACK</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_BACK_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_BACK(0, "ICON_BACK", "back"),

	/**
	 * The '<em><b>ICON EYE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_EYE_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_EYE(0, "ICON_EYE", "eye"),

	/**
	 * The '<em><b>ICON TAG</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_TAG_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_TAG(0, "ICON_TAG", "tag"),

	/**
	 * The '<em><b>ICON UPLOAD CLOUD</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_UPLOAD_CLOUD_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_UPLOAD_CLOUD(0, "ICON_UPLOAD_CLOUD", "upload-cloud"),

	/**
	 * The '<em><b>ICON REPLY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_REPLY_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_REPLY(0, "ICON_REPLY", "reply"),

	/**
	 * The '<em><b>ICON EXPORT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_EXPORT_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_EXPORT(0, "ICON_EXPORT", "export"),

	/**
	 * The '<em><b>ICON PRINT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_PRINT_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_PRINT(0, "ICON_PRINT", "print"),

	/**
	 * The '<em><b>ICON RETWEET</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_RETWEET_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_RETWEET(0, "ICON_RETWEET", "retweet"),

	/**
	 * The '<em><b>ICON COMMENT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_COMMENT_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_COMMENT(0, "ICON_COMMENT", "comment"),

	/**
	 * The '<em><b>ICON VCARD</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_VCARD_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_VCARD(0, "ICON_VCARD", "vcard"),

	/**
	 * The '<em><b>ICON LOCATION</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_LOCATION_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_LOCATION(0, "ICON_LOCATION", "location"),

	/**
	 * The '<em><b>ICON TRASH</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_TRASH_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_TRASH(0, "ICON_TRASH", "trash"),

	/**
	 * The '<em><b>ICON RESIZE FULL</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_RESIZE_FULL_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_RESIZE_FULL(0, "ICON_RESIZE_FULL", "resize-full"),

	/**
	 * The '<em><b>ICON RESIZE SMALL</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_RESIZE_SMALL_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_RESIZE_SMALL(0, "ICON_RESIZE_SMALL", "resize-small"),

	/**
	 * The '<em><b>ICON DOWN OPEN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_DOWN_OPEN_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_DOWN_OPEN(0, "ICON_DOWN_OPEN", "down-open"),

	/**
	 * The '<em><b>ICON LEFT OPEN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_LEFT_OPEN_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_LEFT_OPEN(0, "ICON_LEFT_OPEN", "left-open"),

	/**
	 * The '<em><b>ICON RIGHT OPEN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_RIGHT_OPEN_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_RIGHT_OPEN(0, "ICON_RIGHT_OPEN", "right-open"),

	/**
	 * The '<em><b>ICON UP OPEN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_UP_OPEN_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_UP_OPEN(0, "ICON_UP_OPEN", "up-open"),

	/**
	 * The '<em><b>ICON ARROWS CW</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_ARROWS_CW_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_ARROWS_CW(0, "ICON_ARROWS_CW", "arrows-cw"),

	/**
	 * The '<em><b>ICON CHART PIE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_CHART_PIE_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_CHART_PIE(0, "ICON_CHART_PIE", "chart-pie"),

	/**
	 * The '<em><b>ICON SEARCH 1</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_SEARCH_1_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_SEARCH_1(0, "ICON_SEARCH_1", "search-1"),

	/**
	 * The '<em><b>ICON USER</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_USER_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_USER(0, "ICON_USER", "user"),

	/**
	 * The '<em><b>ICON USERS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_USERS_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_USERS(0, "ICON_USERS", "users"),

	/**
	 * The '<em><b>ICON MONITOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_MONITOR_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_MONITOR(0, "ICON_MONITOR", "monitor"),

	/**
	 * The '<em><b>ICON FOLDER</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_FOLDER_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_FOLDER(0, "ICON_FOLDER", "folder"),

	/**
	 * The '<em><b>ICON DOC</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_DOC_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_DOC(0, "ICON_DOC", "doc"),

	/**
	 * The '<em><b>ICON CALENDAR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_CALENDAR_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_CALENDAR(0, "ICON_CALENDAR", "calendar"),

	/**
	 * The '<em><b>ICON CHART</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_CHART_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_CHART(0, "ICON_CHART", "chart"),

	/**
	 * The '<em><b>ICON ATTACH</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_ATTACH_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_ATTACH(0, "ICON_ATTACH", "attach"),

	/**
	 * The '<em><b>ICON UPLOAD</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_UPLOAD_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_UPLOAD(0, "ICON_UPLOAD", "upload"),

	/**
	 * The '<em><b>ICON DOWNLOAD</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_DOWNLOAD_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_DOWNLOAD(0, "ICON_DOWNLOAD", "download"),

	/**
	 * The '<em><b>ICON MOBILE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_MOBILE_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_MOBILE(0, "ICON_MOBILE", "mobile"),

	/**
	 * The '<em><b>ICON CAMERA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_CAMERA_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_CAMERA(0, "ICON_CAMERA", "camera"),

	/**
	 * The '<em><b>ICON LOCK</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_LOCK_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_LOCK(0, "ICON_LOCK", "lock"),

	/**
	 * The '<em><b>ICON LOCK OPEN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_LOCK_OPEN_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_LOCK_OPEN(0, "ICON_LOCK_OPEN", "lock-open"),

	/**
	 * The '<em><b>ICON BELL</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_BELL_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_BELL(0, "ICON_BELL", "bell"),

	/**
	 * The '<em><b>ICON LINK</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_LINK_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_LINK(0, "ICON_LINK", "link"),

	/**
	 * The '<em><b>ICON CLOCK</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_CLOCK_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_CLOCK(0, "ICON_CLOCK", "clock"),

	/**
	 * The '<em><b>ICON BLOCK</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ICON_BLOCK_VALUE
	 * @generated
	 * @ordered
	 */
	ICON_BLOCK(0, "ICON_BLOCK", "block");

	/**
	 * The '<em><b>ICON PLUS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON PLUS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_PLUS
	 * @model literal="plus"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_PLUS_VALUE = 0;

	/**
	 * The '<em><b>ICON MINUS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON MINUS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_MINUS
	 * @model literal="minus"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_MINUS_VALUE = 0;

	/**
	 * The '<em><b>ICON INFO</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON INFO</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_INFO
	 * @model literal="info"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_INFO_VALUE = 0;

	/**
	 * The '<em><b>ICON LEFT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON LEFT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_LEFT
	 * @model literal="left"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_LEFT_VALUE = 0;

	/**
	 * The '<em><b>ICON UP</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON UP</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_UP
	 * @model literal="up"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_UP_VALUE = 0;

	/**
	 * The '<em><b>ICON RIGHT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON RIGHT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_RIGHT
	 * @model literal="right"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_RIGHT_VALUE = 0;

	/**
	 * The '<em><b>ICON DOWN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON DOWN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_DOWN
	 * @model literal="down"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_DOWN_VALUE = 0;

	/**
	 * The '<em><b>ICON EXCHANGE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON EXCHANGE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_EXCHANGE
	 * @model literal="exchange"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_EXCHANGE_VALUE = 0;

	/**
	 * The '<em><b>ICON HOME</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON HOME</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_HOME
	 * @model literal="home"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_HOME_VALUE = 0;

	/**
	 * The '<em><b>ICON HOME 1</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON HOME 1</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_HOME_1
	 * @model literal="home-1"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_HOME_1_VALUE = 0;

	/**
	 * The '<em><b>ICON UP DIR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON UP DIR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_UP_DIR
	 * @model literal="up-dir"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_UP_DIR_VALUE = 0;

	/**
	 * The '<em><b>ICON RIGHT DIR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON RIGHT DIR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_RIGHT_DIR
	 * @model literal="right-dir"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_RIGHT_DIR_VALUE = 0;

	/**
	 * The '<em><b>ICON DOWN DIR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON DOWN DIR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_DOWN_DIR
	 * @model literal="down-dir"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_DOWN_DIR_VALUE = 0;

	/**
	 * The '<em><b>ICON LEFT DIR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON LEFT DIR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_LEFT_DIR
	 * @model literal="left-dir"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_LEFT_DIR_VALUE = 0;

	/**
	 * The '<em><b>ICON STAR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON STAR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_STAR
	 * @model literal="star"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_STAR_VALUE = 0;

	/**
	 * The '<em><b>ICON STAR EMPTY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON STAR EMPTY</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_STAR_EMPTY
	 * @model literal="star-empty"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_STAR_EMPTY_VALUE = 0;

	/**
	 * The '<em><b>ICON TH LIST</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON TH LIST</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_TH_LIST
	 * @model literal="th-list"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_TH_LIST_VALUE = 0;

	/**
	 * The '<em><b>ICON HEART EMPTY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON HEART EMPTY</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_HEART_EMPTY
	 * @model literal="heart-empty"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_HEART_EMPTY_VALUE = 0;

	/**
	 * The '<em><b>ICON HEART</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON HEART</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_HEART
	 * @model literal="heart"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_HEART_VALUE = 0;

	/**
	 * The '<em><b>ICON MUSIC</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON MUSIC</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_MUSIC
	 * @model literal="music"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_MUSIC_VALUE = 0;

	/**
	 * The '<em><b>ICON TH</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON TH</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_TH
	 * @model literal="th"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_TH_VALUE = 0;

	/**
	 * The '<em><b>ICON FLAG</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON FLAG</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_FLAG
	 * @model literal="flag"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_FLAG_VALUE = 0;

	/**
	 * The '<em><b>ICON COG</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON COG</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_COG
	 * @model literal="cog"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_COG_VALUE = 0;

	/**
	 * The '<em><b>ICON ATTENTION</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON ATTENTION</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_ATTENTION
	 * @model literal="attention"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_ATTENTION_VALUE = 0;

	/**
	 * The '<em><b>ICON MAIL</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON MAIL</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_MAIL
	 * @model literal="mail"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_MAIL_VALUE = 0;

	/**
	 * The '<em><b>ICON EDIT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON EDIT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_EDIT
	 * @model literal="edit"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_EDIT_VALUE = 0;

	/**
	 * The '<em><b>ICON PENCIL</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON PENCIL</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_PENCIL
	 * @model literal="pencil"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_PENCIL_VALUE = 0;

	/**
	 * The '<em><b>ICON OK</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON OK</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_OK
	 * @model literal="ok"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_OK_VALUE = 0;

	/**
	 * The '<em><b>ICON CANCEL 1</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON CANCEL 1</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_CANCEL_1
	 * @model literal="cancel-1"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_CANCEL_1_VALUE = 0;

	/**
	 * The '<em><b>ICON CANCEL</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON CANCEL</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_CANCEL
	 * @model literal="cancel"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_CANCEL_VALUE = 0;

	/**
	 * The '<em><b>ICON CANCEL CIRCLE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON CANCEL CIRCLE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_CANCEL_CIRCLE
	 * @model literal="cancel-circle"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_CANCEL_CIRCLE_VALUE = 0;

	/**
	 * The '<em><b>ICON HELP</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON HELP</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_HELP
	 * @model literal="help"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_HELP_VALUE = 0;

	/**
	 * The '<em><b>ICON PLUS CIRCLE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON PLUS CIRCLE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_PLUS_CIRCLE
	 * @model literal="plus-circle"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_PLUS_CIRCLE_VALUE = 0;

	/**
	 * The '<em><b>ICON MINUS CIRCLE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON MINUS CIRCLE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_MINUS_CIRCLE
	 * @model literal="minus-circle"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_MINUS_CIRCLE_VALUE = 0;

	/**
	 * The '<em><b>ICON RIGHT THIN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON RIGHT THIN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_RIGHT_THIN
	 * @model literal="right-thin"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_RIGHT_THIN_VALUE = 0;

	/**
	 * The '<em><b>ICON FORWARD</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON FORWARD</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_FORWARD
	 * @model literal="forward"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_FORWARD_VALUE = 0;

	/**
	 * The '<em><b>ICON CW</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON CW</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_CW
	 * @model literal="cw"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_CW_VALUE = 0;

	/**
	 * The '<em><b>ICON LEFT THIN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON LEFT THIN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_LEFT_THIN
	 * @model literal="left-thin"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_LEFT_THIN_VALUE = 0;

	/**
	 * The '<em><b>ICON UP THIN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON UP THIN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_UP_THIN
	 * @model literal="up-thin"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_UP_THIN_VALUE = 0;

	/**
	 * The '<em><b>ICON DOWN THIN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON DOWN THIN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_DOWN_THIN
	 * @model literal="down-thin"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_DOWN_THIN_VALUE = 0;

	/**
	 * The '<em><b>ICON LEFT BOLD</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON LEFT BOLD</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_LEFT_BOLD
	 * @model literal="left-bold"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_LEFT_BOLD_VALUE = 0;

	/**
	 * The '<em><b>ICON RIGHT BOLD</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON RIGHT BOLD</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_RIGHT_BOLD
	 * @model literal="right-bold"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_RIGHT_BOLD_VALUE = 0;

	/**
	 * The '<em><b>ICON UP BOLD</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON UP BOLD</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_UP_BOLD
	 * @model literal="up-bold"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_UP_BOLD_VALUE = 0;

	/**
	 * The '<em><b>ICON DOWN BOLD</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON DOWN BOLD</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_DOWN_BOLD
	 * @model literal="down-bold"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_DOWN_BOLD_VALUE = 0;

	/**
	 * The '<em><b>ICON USER ADD</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON USER ADD</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_USER_ADD
	 * @model literal="user-add"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_USER_ADD_VALUE = 0;

	/**
	 * The '<em><b>ICON HELP CIRCLE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON HELP CIRCLE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_HELP_CIRCLE
	 * @model literal="help-circle"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_HELP_CIRCLE_VALUE = 0;

	/**
	 * The '<em><b>ICON INFO CIRCLE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON INFO CIRCLE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_INFO_CIRCLE
	 * @model literal="info-circle"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_INFO_CIRCLE_VALUE = 0;

	/**
	 * The '<em><b>ICON BACK</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON BACK</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_BACK
	 * @model literal="back"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_BACK_VALUE = 0;

	/**
	 * The '<em><b>ICON EYE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON EYE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_EYE
	 * @model literal="eye"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_EYE_VALUE = 0;

	/**
	 * The '<em><b>ICON TAG</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON TAG</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_TAG
	 * @model literal="tag"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_TAG_VALUE = 0;

	/**
	 * The '<em><b>ICON UPLOAD CLOUD</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON UPLOAD CLOUD</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_UPLOAD_CLOUD
	 * @model literal="upload-cloud"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_UPLOAD_CLOUD_VALUE = 0;

	/**
	 * The '<em><b>ICON REPLY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON REPLY</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_REPLY
	 * @model literal="reply"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_REPLY_VALUE = 0;

	/**
	 * The '<em><b>ICON EXPORT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON EXPORT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_EXPORT
	 * @model literal="export"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_EXPORT_VALUE = 0;

	/**
	 * The '<em><b>ICON PRINT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON PRINT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_PRINT
	 * @model literal="print"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_PRINT_VALUE = 0;

	/**
	 * The '<em><b>ICON RETWEET</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON RETWEET</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_RETWEET
	 * @model literal="retweet"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_RETWEET_VALUE = 0;

	/**
	 * The '<em><b>ICON COMMENT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON COMMENT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_COMMENT
	 * @model literal="comment"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_COMMENT_VALUE = 0;

	/**
	 * The '<em><b>ICON VCARD</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON VCARD</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_VCARD
	 * @model literal="vcard"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_VCARD_VALUE = 0;

	/**
	 * The '<em><b>ICON LOCATION</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON LOCATION</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_LOCATION
	 * @model literal="location"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_LOCATION_VALUE = 0;

	/**
	 * The '<em><b>ICON TRASH</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON TRASH</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_TRASH
	 * @model literal="trash"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_TRASH_VALUE = 0;

	/**
	 * The '<em><b>ICON RESIZE FULL</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON RESIZE FULL</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_RESIZE_FULL
	 * @model literal="resize-full"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_RESIZE_FULL_VALUE = 0;

	/**
	 * The '<em><b>ICON RESIZE SMALL</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON RESIZE SMALL</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_RESIZE_SMALL
	 * @model literal="resize-small"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_RESIZE_SMALL_VALUE = 0;

	/**
	 * The '<em><b>ICON DOWN OPEN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON DOWN OPEN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_DOWN_OPEN
	 * @model literal="down-open"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_DOWN_OPEN_VALUE = 0;

	/**
	 * The '<em><b>ICON LEFT OPEN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON LEFT OPEN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_LEFT_OPEN
	 * @model literal="left-open"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_LEFT_OPEN_VALUE = 0;

	/**
	 * The '<em><b>ICON RIGHT OPEN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON RIGHT OPEN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_RIGHT_OPEN
	 * @model literal="right-open"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_RIGHT_OPEN_VALUE = 0;

	/**
	 * The '<em><b>ICON UP OPEN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON UP OPEN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_UP_OPEN
	 * @model literal="up-open"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_UP_OPEN_VALUE = 0;

	/**
	 * The '<em><b>ICON ARROWS CW</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON ARROWS CW</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_ARROWS_CW
	 * @model literal="arrows-cw"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_ARROWS_CW_VALUE = 0;

	/**
	 * The '<em><b>ICON CHART PIE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON CHART PIE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_CHART_PIE
	 * @model literal="chart-pie"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_CHART_PIE_VALUE = 0;

	/**
	 * The '<em><b>ICON SEARCH 1</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON SEARCH 1</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_SEARCH_1
	 * @model literal="search-1"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_SEARCH_1_VALUE = 0;

	/**
	 * The '<em><b>ICON USER</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON USER</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_USER
	 * @model literal="user"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_USER_VALUE = 0;

	/**
	 * The '<em><b>ICON USERS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON USERS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_USERS
	 * @model literal="users"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_USERS_VALUE = 0;

	/**
	 * The '<em><b>ICON MONITOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON MONITOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_MONITOR
	 * @model literal="monitor"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_MONITOR_VALUE = 0;

	/**
	 * The '<em><b>ICON FOLDER</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON FOLDER</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_FOLDER
	 * @model literal="folder"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_FOLDER_VALUE = 0;

	/**
	 * The '<em><b>ICON DOC</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON DOC</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_DOC
	 * @model literal="doc"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_DOC_VALUE = 0;

	/**
	 * The '<em><b>ICON CALENDAR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON CALENDAR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_CALENDAR
	 * @model literal="calendar"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_CALENDAR_VALUE = 0;

	/**
	 * The '<em><b>ICON CHART</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON CHART</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_CHART
	 * @model literal="chart"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_CHART_VALUE = 0;

	/**
	 * The '<em><b>ICON ATTACH</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON ATTACH</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_ATTACH
	 * @model literal="attach"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_ATTACH_VALUE = 0;

	/**
	 * The '<em><b>ICON UPLOAD</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON UPLOAD</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_UPLOAD
	 * @model literal="upload"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_UPLOAD_VALUE = 0;

	/**
	 * The '<em><b>ICON DOWNLOAD</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON DOWNLOAD</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_DOWNLOAD
	 * @model literal="download"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_DOWNLOAD_VALUE = 0;

	/**
	 * The '<em><b>ICON MOBILE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON MOBILE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_MOBILE
	 * @model literal="mobile"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_MOBILE_VALUE = 0;

	/**
	 * The '<em><b>ICON CAMERA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON CAMERA</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_CAMERA
	 * @model literal="camera"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_CAMERA_VALUE = 0;

	/**
	 * The '<em><b>ICON LOCK</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON LOCK</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_LOCK
	 * @model literal="lock"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_LOCK_VALUE = 0;

	/**
	 * The '<em><b>ICON LOCK OPEN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON LOCK OPEN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_LOCK_OPEN
	 * @model literal="lock-open"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_LOCK_OPEN_VALUE = 0;

	/**
	 * The '<em><b>ICON BELL</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON BELL</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_BELL
	 * @model literal="bell"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_BELL_VALUE = 0;

	/**
	 * The '<em><b>ICON LINK</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON LINK</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_LINK
	 * @model literal="link"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_LINK_VALUE = 0;

	/**
	 * The '<em><b>ICON CLOCK</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON CLOCK</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_CLOCK
	 * @model literal="clock"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_CLOCK_VALUE = 0;

	/**
	 * The '<em><b>ICON BLOCK</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ICON BLOCK</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ICON_BLOCK
	 * @model literal="block"
	 * @generated
	 * @ordered
	 */
	public static final int ICON_BLOCK_VALUE = 0;

	/**
	 * An array of all the '<em><b>Icons Enum</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final IconsEnum[] VALUES_ARRAY =
		new IconsEnum[] {
			ICON_PLUS,
			ICON_MINUS,
			ICON_INFO,
			ICON_LEFT,
			ICON_UP,
			ICON_RIGHT,
			ICON_DOWN,
			ICON_EXCHANGE,
			ICON_HOME,
			ICON_HOME_1,
			ICON_UP_DIR,
			ICON_RIGHT_DIR,
			ICON_DOWN_DIR,
			ICON_LEFT_DIR,
			ICON_STAR,
			ICON_STAR_EMPTY,
			ICON_TH_LIST,
			ICON_HEART_EMPTY,
			ICON_HEART,
			ICON_MUSIC,
			ICON_TH,
			ICON_FLAG,
			ICON_COG,
			ICON_ATTENTION,
			ICON_MAIL,
			ICON_EDIT,
			ICON_PENCIL,
			ICON_OK,
			ICON_CANCEL_1,
			ICON_CANCEL,
			ICON_CANCEL_CIRCLE,
			ICON_HELP,
			ICON_PLUS_CIRCLE,
			ICON_MINUS_CIRCLE,
			ICON_RIGHT_THIN,
			ICON_FORWARD,
			ICON_CW,
			ICON_LEFT_THIN,
			ICON_UP_THIN,
			ICON_DOWN_THIN,
			ICON_LEFT_BOLD,
			ICON_RIGHT_BOLD,
			ICON_UP_BOLD,
			ICON_DOWN_BOLD,
			ICON_USER_ADD,
			ICON_HELP_CIRCLE,
			ICON_INFO_CIRCLE,
			ICON_BACK,
			ICON_EYE,
			ICON_TAG,
			ICON_UPLOAD_CLOUD,
			ICON_REPLY,
			ICON_EXPORT,
			ICON_PRINT,
			ICON_RETWEET,
			ICON_COMMENT,
			ICON_VCARD,
			ICON_LOCATION,
			ICON_TRASH,
			ICON_RESIZE_FULL,
			ICON_RESIZE_SMALL,
			ICON_DOWN_OPEN,
			ICON_LEFT_OPEN,
			ICON_RIGHT_OPEN,
			ICON_UP_OPEN,
			ICON_ARROWS_CW,
			ICON_CHART_PIE,
			ICON_SEARCH_1,
			ICON_USER,
			ICON_USERS,
			ICON_MONITOR,
			ICON_FOLDER,
			ICON_DOC,
			ICON_CALENDAR,
			ICON_CHART,
			ICON_ATTACH,
			ICON_UPLOAD,
			ICON_DOWNLOAD,
			ICON_MOBILE,
			ICON_CAMERA,
			ICON_LOCK,
			ICON_LOCK_OPEN,
			ICON_BELL,
			ICON_LINK,
			ICON_CLOCK,
			ICON_BLOCK,
		};

	/**
	 * A public read-only list of all the '<em><b>Icons Enum</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<IconsEnum> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Icons Enum</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static IconsEnum get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			IconsEnum result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Icons Enum</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static IconsEnum getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			IconsEnum result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Icons Enum</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static IconsEnum get(int value) {
		switch (value) {
			case ICON_PLUS_VALUE: return ICON_PLUS;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private IconsEnum(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //IconsEnum
